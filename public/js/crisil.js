$(document).ready(function(){ 
	var dur = 3;
	var dataName = '';
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var chart_id = 'graph-view-div';
	var date_id = '#from_date';
	var value_id = '#to_date';
	var diff_id = '#val';
	var graph_val = '';
	var data = '';
	var start_nav = '';
	var start_date;
	var range_stat = 0;

	$(document).on('click','.subcat-li',function(){
		dataName = $(this).data('value');
		console.log($(this).text());
		$('#sub-category').text($(this).text());
		graphData(dur, dataName);
	});

	$('.date-li').on('click',function(){
		dur = $(this).data('value');
		$('#pick-date-id').text($(this).text());
		if (dataName != '') {
			graphData(dur, dataName);
		}
	});

	function graphData(dur, dataname) {
		// console.log(dur+dataName);
		$('.overlay').css('display','block');
		var formData = 'duration='+dur+'&data='+dataname;
		$.ajax({
			type: "POST",
			url: "/getCrisil",
			data:formData,
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
			  $('.overlay').css('display','none');
				if (data.msg == 1) {
					graph_val = data.data;
					var change = data.return;
	                $('#rr-span').removeClass('positive');
	                $('#rr-span').removeClass('negetive');
	                if(change < 0) {
	                  $('#rr-span').addClass('negetive');
	                } else {
	                  $('#rr-span').addClass('positive');
	                }
	                $('#rr-span').text('('+change+'%)');
					populate_data(chart_id,date_id,value_id,diff_id,graph_val);
				}else{
					alert('error!!!!!');
				}
			},
			error: function(xhr, status, error) {
			  $('.overlay').css('display','none');
				alert(error);
			}
		});
	}

	function populate_data(chart_id, date_id, value_id, diff_id, data) 
	{
		console.log(data);
	  Highcharts.stockChart(chart_id, {
		 xAxis: {
	            gapGridLineWidth: 0,
	            lineColor: '#232323',
	        },
	     yAxis: {
	            gridLineColor: '#232323'
	        },
		    credits: {
		        text: 'Righttracker',
		        href: 'http://www.righttracker.in'
		    },
	        rangeSelector: {
	            selected: 5,
	            enabled:false
	            
	        },
	        chart: {
			      backgroundColor: chart_bg_color,
			      selectionMarkerFill: markerFillColor,
			                events: {
			              selection: function(event) {
			                  if (event.xAxis) {
			                      return false;
			                  }
			              }
			          },
			          zoomType: 'x'
			    
			  },
			chart: {
			      backgroundColor: chart_bg_color,
			      selectionMarkerFill: markerFillColor,
			                events: {
			              selection: function(event) {
			                  if (event.xAxis) {
			                      return false;
			                  }
			              }
			          },
			          zoomType: 'x'
			    
			  },
	        navigator: {
	            enabled: false
	        },
	        scrollbar: {
	            enabled: false
	        },
	        plotOptions: {
		      series: {
		        allowPointSelect: true,
		          marker: {
		              states: {
		                  select: {
		                      fillColor: 'red',
		                      lineWidth: 0
		                  }
		              }
		          },
		        point: {
		          events: {
		            mouseOver: function () {
		              if(range_stat == 1 && start_nav == '') {
		                var row_index = parseInt(this.index) - 1;
		                selected_nav = data[row_index];
		                start_date = new Date(data[row_index][0]);
		                start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
		              }
		              var current_index = parseFloat(this.y);
		              if(range_stat == 1 && start_nav != '' ) {
		                var diff = current_index - start_nav;
		                var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
		                myDate = new Date(this.x);
		                $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		                $(value_id).text(' '+current_index.toFixed(2)+' ');
		                $(diff_id).html(' ('+change+'%)');
		                $(diff_id).removeClass('positive');
		                $(diff_id).removeClass('negetive');
		                if(change < 0) {
		                  $(diff_id).addClass('negetive');
		                } else {
		                  $(diff_id).addClass('positive');
		                }
		              } else {
		                myDate = new Date(this.x);
		                $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		                $(value_id).text(' '+current_index.toFixed(2)+' ');
		                $(diff_id).html('');


		              }
		            }
		          },    
		        },
		      }
		    },
	        series: [{
	            name: 'EOD',
	            type: 'area',
	            data: data,
	            gapSize: 0,
	            color : graphColor,
	            tooltip: {
	                valueDecimals: 2,
	                enabled: false,
	            },
	            fillColor: {
	                linearGradient: {
	                    x1: 0,
	                    y1: 0,
	                    x2: 0,
	                    y2: 1
	                },
	                stops: [
	                    [0, 'rgba(248, 132, 7, 0.5)'],
	                    [1, 'rgba(248, 132, 7, 0.0)'],

	                ]
	            },
	            threshold: null
	        }]
	    });

		$('#graph-view-div').on('mousedown', function() {
		  range_stat = 1;
		});

		$('#graph-view-div').on('mouseup', function() {
		  range_stat = 0;
		  start_nav = '';
			$(diff_id).text('');
			$(value_id).text('');
			$(date_id).text('');
		});
		$('#graph-view-div').on('mouseleave', function(){
			$(diff_id).text('');
			$(value_id).text('');
			$(date_id).text('');
			range_stat = 0;
			start_nav = '';
		});
	   
	}
});