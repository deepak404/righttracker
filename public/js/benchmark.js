$(document).ready(function(){
	// $( document ).ajaxStart(function() {
	//   $('.overlay').css('display','block');
	// });
	// $( document ).ajaxStop(function() {
	//   $('.overlay').css('display','none');
	// });
	var dt = new Date();
	var time = dt.getHours()+"."+dt.getMinutes();
	var today = dt.getDay();
	var sensex_old = 0;
	var nifty_old = 0;
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var graph_val = '';
	var data = '';
	var start_nav = '';
	var start_date;
	var range_stat = 0;
	console.log(time);
	getIndex('sensex','sensex-chart', '#sensex-date', '#sensex-rate', '#sensex-diff', 1);
	getIndex('nifty','nifty-chart', '#nifty-date', '#nifty-rate', '#nifty-diff', 1);
	getIndex('midcap','midcap-chart', '#midcap-date', '#midcap-rate', '#midcap-diff', 1);
	getIndex('smallcap','smallcap-chart', '#smallcap-date', '#smallcap-rate', '#smallcap-diff', 1);
	checkTime()
	// liveIndex()

	function checkTime() {
		if (today != 0 && today != 6) {
			indexUpdate('in%3BNSX','nifty')
			indexUpdate('in%3BSEN','sensex')
			indexUpdate('in%3Bbmx','midcap') //BSE Midcap
			indexUpdate('in%3BBCX','smallcap') // BSE Smallcap
			setTimeout(checkTime, 5000);
		}
	}


	function indexUpdate(index,name) {
		$.ajax({
			type: "GET",
			url: "https://priceapi.moneycontrol.com/pricefeed/notapplicable/inidicesindia/"+index,
			success: function(data) {
				if (data != "") {
					var val = data.data;
					var change = Math.round(val.pricechange * 100) / 100;
					var per = Math.round(val.pricepercentchange * 100) / 100;

					$("#"+name+"-span").text(val.pricecurrent);
					$("#"+name+"-current").text(change);
					$("#"+name+"-per").text("("+per+"%)");

					if (change > 0) {
						$('#'+name+'-current').removeClass('negetive').addClass('positive');
						$('#'+name+'-per').removeClass('negetive').addClass('positive');
						$('#'+name+'-icon').removeClass('negetive').addClass('positive');
						$('#'+name+'-icon').text('arrow_drop_up');
					}else{
						$('#'+name+'-current').removeClass('positive').addClass('negetive');
						$('#'+name+'-per').removeClass('positive').addClass('negetive');
						$('#'+name+'-icon').removeClass('positive').addClass('negetive');
						$('#'+name+'-icon').text('arrow_drop_down');
					}
						
				}
			},
			error: function(xhr, error, status) {
				console.log(error);
			}
		});
	}

	// function liveIndex() {
	// 	$.ajax({
	// 		type: "GET",
	// 		url: "/liveindex",
	// 		success: function(data) {
	// 			setTimeout(checkTime, 60000);
	// 			var sensex = data.data.sensex;
	// 			var nifty = data.data.nifty;
	// 			var smallcap = data.data.smallcap;
	// 			var midcap = data.data.midcap;
	// 			console.log(smallcap);
	// 			$("#sensex-span").text(sensex.close);
	// 			$("#sensex-current").text(sensex.change);
	// 			$("#sensex-per").text("("+sensex.per+"%)");
	// 			$("#titletext").text("RightTracker | Sensex - "+sensex.close+" "+sensex.change+" ("+sensex.per+"%)");
	// 			if (sensex.change > 0) {
	// 				$("#sensex-current").removeClass('negetive').addClass('positive');
	// 				$("#sensex-per").removeClass('negetive').addClass('positive');
	// 				$("#sensex-icon").removeClass('negetive').addClass('positive');
	// 				$("#sensex-icon").text('arrow_drop_up');
	// 			}else{
	// 				$("#sensex-current").removeClass('positive').addClass('negetive');
	// 				$("#sensex-per").removeClass('positive').addClass('negetive');
	// 				$("#sensex-icon").removeClass('positive').addClass('negetive');
	// 				$("#sensex-icon").text('arrow_drop_down');
	// 			}

	// 			$("#nifty-span").text(nifty.close);
	// 			$("#nifty-current").text(nifty.change);
	// 			$("#nifty-per").text("("+nifty.per+"%)");
	// 			if (nifty.change > 0) {
	// 				$("#nifty-current").removeClass('negetive').addClass('positive');
	// 				$("#nifty-per").removeClass('negetive').addClass('positive');
	// 				$("#nifty-icon").removeClass('negetive').addClass('positive');
	// 				$("#nifty-icon").text('arrow_drop_up');
	// 			}else{
	// 				$("#nifty-current").removeClass('positive').addClass('negetive');
	// 				$("#nifty-per").removeClass('positive').addClass('negetive');
	// 				$("#nifty-icon").removeClass('positive').addClass('negetive');
	// 				$("#nifty-icon").text('arrow_drop_down');
	// 			}

	// 			$("#smallcap-span").text(smallcap.close);
	// 			$("#smallcap-current").text(smallcap.change);
	// 			$("#smallcap-per").text("("+smallcap.per+"%)");
	// 			if (smallcap.change > 0) {
	// 				$("#smallcap-current").removeClass('negetive').addClass('positive');
	// 				$("#smallcap-per").removeClass('negetive').addClass('positive');
	// 				$("#smallcap-icon").removeClass('negetive').addClass('positive');
	// 				$("#smallcap-icon").text('arrow_drop_up');
	// 			}else{
	// 				$("#smallcap-current").removeClass('positive').addClass('negetive');
	// 				$("#smallcap-per").removeClass('positive').addClass('negetive');
	// 				$("#smallcap-icon").removeClass('positive').addClass('negetive');
	// 				$("#smallcap-icon").text('arrow_drop_down');
	// 			}

	// 			$("#midcap-span").text(midcap.close);
	// 			$("#midcap-current").text(midcap.change);
	// 			$("#midcap-per").text("("+midcap.per+"%)");
	// 			if (midcap.change > 0) {
	// 				$("#midcap-current").removeClass('negetive').addClass('positive');
	// 				$("#midcap-per").removeClass('negetive').addClass('positive');
	// 				$("#midcap-icon").removeClass('negetive').addClass('positive');
	// 				$("#midcap-icon").text('arrow_drop_up');
	// 			}else{
	// 				$("#midcap-current").removeClass('positive').addClass('negetive');
	// 				$("#midcap-per").removeClass('positive').addClass('negetive');
	// 				$("#midcap-icon").removeClass('positive').addClass('negetive');
	// 				$("#midcap-icon").text('arrow_drop_down');
	// 			}

	// 		},
	// 		error: function(xhr, status, error) {
	// 			alert(error);
	// 		}
	// 	});
	// }

	function getIndex(index, chart_id, date_id, value_id, diff_id, timespace){
	  $('.overlay').css('display','block');
		var formData = 'duration='+timespace+'&index='+index;
		$.ajax({
			type: "POST",
			url: "/getIndex",
			data:formData,
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
			  $('.overlay').css('display','none');
				if (data.msg == 1) {
					graph_val = data.data;
					populate_data(chart_id,date_id,value_id,diff_id,graph_val);
				}else{
					alert('error!!!!!');
				}
			},
			error: function(xhr, status, error) {
			  $('.overlay').css('display','none');
				alert(error);
			}
		});
	}

	// function sensex() {
	// 	$.ajax({
	// 		type: "GET",
	// 		url: "http://www.moneycontrol.com/terminal/upd_terminal.php?sect_val=347c425345",
	// 		success: function(data) {
	// 			setTimeout(sensex, 5000);
	// 			if (data != "") {
	// 				var array = data.split('~');
	// 				$("#sensex-span").text(array[0]);
	// 				$("#sensex-current").text(array[1]);
	// 				$("#sensex-per").text("("+array[2]+"%)");
	// 				$("#titletext").text("RightTracker | Sensex - "+array[0]+" "+array[1]+" ("+array[2]+"%)");
	// 				if (array[1] > 0) {
	// 					$("#sensex-current").removeClass('negetive').addClass('positive');
	// 					$("#sensex-per").removeClass('negetive').addClass('positive');
	// 					$("#sensex-icon").removeClass('negetive').addClass('positive');
	// 					$("#sensex-icon").text('arrow_drop_up');
	// 				}else{
	// 					$("#sensex-current").removeClass('positive').addClass('negetive');
	// 					$("#sensex-per").removeClass('positive').addClass('negetive');
	// 					$("#sensex-icon").removeClass('positive').addClass('negetive');
	// 					$("#sensex-icon").text('arrow_drop_down');
	// 				}
	// 				// if (sensex_old < array[0]) {
	// 				// 	$('#down').hide();
	// 				// 	$('#up').show();
	// 				// }else{
	// 				// 	$('#up').hide();
	// 				// 	$('#down').show();
	// 				// }
	// 				// sensex_old = array[0];
	// 			}
	// 		},
	// 		error: function(xhr, status, error) {
	// 			alert(error);
	// 		}
	// 	});
	// }

	// function nifty() {
	// 	$.ajax({
	// 		type: "GET",
	// 		url: "http://www.moneycontrol.com/terminal/upd_terminal.php?sect_val=397c4e5345",
	// 		success: function(data) {
	// 			setTimeout(nifty, 5000);
	// 			if (data != "") {
	// 				var array = data.split('~');
	// 				$("#nifty-span").text(array[0]);
	// 				$("#nifty-current").text(array[1]);
	// 				$("#nifty-per").text("("+array[2]+"%)");
	// 				if (array[1] > 0) {
	// 					$("#nifty-current").removeClass('negetive').addClass('positive');
	// 					$("#nifty-per").removeClass('negetive').addClass('positive');
	// 					$("#nifty-icon").removeClass('negetive').addClass('positive');
	// 					$("#nifty-icon").text('arrow_drop_up');
	// 				}else{
	// 					$("#nifty-current").removeClass('positive').addClass('negetive');
	// 					$("#nifty-per").removeClass('positive').addClass('negetive');
	// 					$("#nifty-icon").removeClass('positive').addClass('negetive');
	// 					$("#nifty-icon").text('arrow_drop_down');
	// 				}
	// 				// if (nifty_old < array[0]) {
	// 				// 	$('#Ndown').hide();
	// 				// 	$('#Nup').show();
	// 				// }else{
	// 				// 	$('#Nup').hide();
	// 				// 	$('#Ndown').show();
	// 				// }
	// 				// nifty_old = array[0];
	// 			}
	// 		},
	// 		error: function(xhr, status, error) {
	// 			alert(error);
	// 		}
	// 	});
	// }
function populate_data(chart_id, date_id, value_id, diff_id, data) 
{
	console.log(data);
  Highcharts.stockChart(chart_id, {
	 xAxis: {
            gapGridLineWidth: 0,
            lineColor: '#232323',
        },
     yAxis: {
            gridLineColor: '#232323'
        },
	    credits: {
	        text: 'Righttracker',
	        href: 'http://www.righttracker.in'
	    },
        rangeSelector: {
            selected: 5,
            enabled:false
            
        },
        chart: {
		      backgroundColor: chart_bg_color,
		      selectionMarkerFill: markerFillColor,
		                events: {
		              selection: function(event) {
		                  if (event.xAxis) {
		                      return false;
		                  }
		              }
		          },
		          zoomType: 'x'
		    
		  },
		chart: {
		      backgroundColor: chart_bg_color,
		      selectionMarkerFill: markerFillColor,
		                events: {
		              selection: function(event) {
		                  if (event.xAxis) {
		                      return false;
		                  }
		              }
		          },
		          zoomType: 'x'
		    
		  },
        navigator: {
            enabled: false
        },
        scrollbar: {
            enabled: false
        },
        plotOptions: {
	      series: {
	        allowPointSelect: true,
	          marker: {
	              states: {
	                  select: {
	                      fillColor: 'red',
	                      lineWidth: 0
	                  }
	              }
	          },
	        point: {
	          events: {
	            mouseOver: function () {
	              if(range_stat == 1 && start_nav == '') {
	                var row_index = parseInt(this.index) - 1;
	                selected_nav = data[row_index];
	                start_date = new Date(data[row_index][0]);
	                start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
	              }
	              var current_index = parseFloat(this.y);
	              if(range_stat == 1 && start_nav != '' ) {
	                var diff = current_index - start_nav;
	                var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
	                myDate = new Date(this.x);
	                $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
	                $(value_id).text(' '+current_index.toFixed(2)+' ');
	                $(diff_id).html(' ('+change+'%)');
	                $(diff_id).removeClass('positive');
	                $(diff_id).removeClass('negetive');
	                if(change < 0) {
	                  $(diff_id).addClass('negetive');
	                } else {
	                  $(diff_id).addClass('positive');
	                }
	              } else {
	                myDate = new Date(this.x);
	                $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
	                $(value_id).text(' '+current_index.toFixed(2)+' ');
	                $(diff_id).html('');


	              }
	            }
	          },    
	        },
	      }
	    },
        series: [{
            name: 'EOD',
            type: 'area',
            data: data,
            gapSize: 0,
            color : graphColor,
            tooltip: {
                valueDecimals: 2,
                enabled: false,
            },
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, 'rgba(248, 132, 7, 0.5)'],
                    [1, 'rgba(248, 132, 7, 0.0)'],

                ]
            },
            threshold: null
        }]
    });
   
}

$('#sensex-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#sensex-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
	$('#sensex-diff').text('');
	$('#sensex-rate').text('');
	$('#sensex-date').text('');
});

$('#nifty-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#nifty-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
	$('#nifty-diff').text('');
	$('#nifty-rate').text('');
	$('#nifty-date').text('');
});
$('#smallcap-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#smallcap-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
	$('#smallcap-diff').text('');
	$('#smallcap-rate').text('');
	$('#smallcap-date').text('');
});

$('#midcap-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#midcap-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#midcap-diff').text('');
		$('#midcap-rate').text('');
		$('#midcap-date').text('');
});

$('.sensex-li').on('click',function(){
	getIndex('sensex','sensex-chart', '#sensex-date', '#sensex-rate', '#sensex-diff', $(this).data('value'));
	$('#sensex-btn').text($(this).text());
	$('#sensex-btn').append('<span class="caret"></span>');
});
$('.nifty-li').on('click',function(){
	$('#nifty-btn').text($(this).text());
	$('#nifty-btn').append('<span class="caret"></span>');
	getIndex('nifty','nifty-chart', '#nifty-date', '#nifty-rate', '#nifty-diff', $(this).data('value'));
});
$('.smallcap-li').on('click',function(){
	$('#smallcap-btn').text($(this).text());
	$('#smallcap-btn').append('<span class="caret"></span>');
	getIndex('smallcap','smallcap-chart', '#smallcap-date', '#smallcap-rate', '#smallcap-diff', $(this).data('value'));
});
$('.midcap-li').on('click',function(){
	$('#midcap-btn').text($(this).text());
	$('#midcap-btn').append('<span class="caret"></span>');
	getIndex('midcap','midcap-chart', '#midcap-date', '#midcap-rate', '#midcap-diff', $(this).data('value'));
});
	$('#sensex-chart').on('mouseleave', function(){
		$('#sensex-diff').text('');
		$('#sensex-rate').text('');
		$('#sensex-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#nifty-chart').on('mouseleave', function(){
		$('#nifty-diff').text('');
		$('#nifty-rate').text('');
		$('#nifty-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#midcap-chart').on('mouseleave', function(){
		$('#midcap-diff').text('');
		$('#midcap-rate').text('');
		$('#midcap-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#smallcap-chart').on('mouseleave', function(){
		$('#smallcap-diff').text('');
		$('#smallcap-rate').text('');
		$('#smallcap-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});


});
