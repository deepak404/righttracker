$(document).ready(function(){
	var chart_id = 'graph-div';
	var date_id = '#date-span';
	var value_id = '#value-span';
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var graph_val = '';
	var data = '';
	var start_nav = '';
	var start_date;
	var range_stat = 0;
	var timespace = 3;
	var selected_policy = 'wpi';
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#selected-title').text($('#wpi-div').data('title'));
    $('#selected-date').text($('#wpi-div').data('date'));
    $('#selected-val').text($('#wpi-div').data('price')+'%');

	selectedPolicy(selected_policy);

	$('.card-custom').on('click', function(){
		$( ".active-card" ).removeClass( "active-card" );
		$(this).addClass("active-card");
	    $('#selected-title').text($(this).data('title'));
	    $('#selected-date').text($(this).data('date'));
	    $('#selected-val').text($(this).data('price')+'%');
	    selected_policy = $(this).data('policy');
	    switch($(this).data('policy')){
	    	case 'cpi':
	    		timespace = 2;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="2" selected>3 month</option>'+
                        '<option value="3">1 Year</option>'+
                        '<option value="4">5 Years</option>'+
                        '<option value="5">max</option>')
	    	break;
	    	case 'wpi':
	    		timespace = 2;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="2" selected>3 month</option>'+
                        '<option value="3">1 Year</option>'+
                        '<option value="4">5 Years</option>'+
                        '<option value="5">max</option>')
	    	break;
	    	case 'gdp':
	    		timespace = 2;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="2" selected>3 month</option>'+
                        '<option value="3">1 Year</option>'+
                        '<option value="4">5 Years</option>'+
                        '<option value="5">max</option>')
	    	break;
	    	case 'wpi':
	    		timespace = 2;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="2" selected>3 month</option>'+
                        '<option value="3">1 Year</option>'+
                        '<option value="4">5 Years</option>'+
                        '<option value="5">max</option>')
	    	break;
	    	case 'rr':
	    		timespace = 3;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="3" selected>1 Year</option>'+
                        '<option value="4">5 Years</option>'+
                        '<option value="5">max</option>')
	    	break;
	    	case 'ten':
	    		timespace = 1;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="1" selected>1 month</option>'+
		    			'<option value="2">3 month</option>'+
                        '<option value="3">1 Year</option>'+
                        '<option value="4">5 Years</option>'+
                        '<option value="5">max</option>')
	    	break;
	    	case 'slr':
	    		timespace = 0;
	    		$('.globalgraph-years').empty();
	    		$('.globalgraph-years').append('<option value="5" selected>max</option>')
	    	break;
	    }
	    selectedPolicy(selected_policy);
	});

	$('.globalgraph-years').on('change', function(){
		timespace = $('.globalgraph-years').val();
		selectedPolicy(selected_policy);
	});
	
	$('#wpi').on('change',function(){
		var formData = new FormData();
		var wpi = $('#wpi').get(0).files[0];
		formData.append('file',wpi);
		formData.append('policy','wpi');
		$('.overlay').css('display','block')
		addPolicy(formData);
	});	

	$('#cpi').on('change',function(){
		var formData = new FormData();
		var cpi = $('#cpi').get(0).files[0];
		formData.append('file',cpi);
		formData.append('policy','cpi');
		$('.overlay').css('display','block')
		addPolicy(formData);
	});	

	$('#slr').on('change',function(){
		var formData = new FormData();
		var slr = $('#slr').get(0).files[0];
		formData.append('file',slr);
		formData.append('policy','slr');
		$('.overlay').css('display','block')
		addPolicy(formData);
	});	
	
	$('#gdp').on('change',function(){
		var formData = new FormData();
		var gdp = $('#gdp').get(0).files[0];
		formData.append('file',gdp);
		formData.append('policy','gdp');
		$('.overlay').css('display','block')
		addPolicy(formData);
	});

	$('#repo-rate').on('change',function(){
		var formData = new FormData();
		var repoRate = $('#repo-rate').get(0).files[0];
		formData.append('file',repoRate);
		formData.append('policy','rr');
		$('.overlay').css('display','block')
		addPolicy(formData);
	});

	$('#ten-bond').on('change',function(){
		var formData = new FormData();
		var tenYearBond = $('#ten-bond').get(0).files[0];
		formData.append('file',tenYearBond);
		formData.append('policy','ten');
		$('.overlay').css('display','block')
		addPolicy(formData);
	});

	$('.add-policy').on('click', function(){
		$('#ptmodal-title').text($(this).parent().parent().data('title'));
		$('#ptmodal-policy').val($(this).parent().parent().data('policy'));
		$('#ptmodal').modal('show');

	});

	$('#policy-update-form').on('submit',function(e){
		e.preventDefault();
		$('.overlay').css('display','block');
		var formData = $('#policy-update-form').serialize();
		$.ajax({
	        type: 'POST',
	        url: '/insert_policy',
	        data: formData,
	        // contentType: false,
	        // processData: false,
	        success:function(data){
	        	$('.overlay').css('display','none')
	        	if (data.msg == 1) {
	        		alert('Success !!!!');
	        		location.reload();
	        	}else{
	        		alert('Error Try Again!!!');
	        	}
	        },
	        error:function(){ 
	        	$('.overlay').css('display','none')
	            
	        }
	    });
	});

	function addPolicy(formData){
	    $.ajax({
	        type: 'POST',
	        url: '/add_policy',
	        data: formData,
	        contentType: false,
	        processData: false,
	        success:function(data){
	        	$('.overlay').css('display','none')
	        	if (data.msg == 1) {
	        		alert('Success !!!!');
	        		location.reload();
	        	}else{
	        		alert('Error Try Again!!!');
	        	}
	        },
	        error:function(){ 
	        	$('.overlay').css('display','none')
	            
	        }
	    });
		
	}

	function populate_data(data) 
	{
	  Highcharts.stockChart(chart_id, {
		 xAxis: {
	            gapGridLineWidth: 0,
	            lineColor: '#232323',
	        },
	     yAxis: {
	            gridLineColor: '#232323'
	        },
	        rangeSelector: {
	            selected: 5,
	            enabled:false
	        },
			chart: {
			      backgroundColor: chart_bg_color,
			  },
		    credits: {
		        text: 'Righttracker',
		        href: 'http://www.righttracker.in'
		    },
	        navigator: {
	            enabled: false
	        },
	        scrollbar: {
	            enabled: false
	        },
	        plotOptions: {
		      series: {
		        allowPointSelect: true,
		          marker: {
		              states: {
		                  select: {
		                      fillColor: 'red',
		                      lineWidth: 0
		                  }
		              }
		          },
		        point: {
		          events: {
		            mouseOver: function () {
		              if(range_stat == 1 && start_nav == '') {
		                var row_index = parseInt(this.index) - 1;
		                selected_nav = data[row_index];
		                start_date = new Date(data[row_index][0]);
		                start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
		              }
		              var current_index = parseFloat(this.y);
		              if(range_stat == 1 && start_nav != '' ) {
		                var diff = current_index - start_nav;
		                var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
		                myDate = new Date(this.x);
		                $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		                $(value_id).text(' '+current_index.toFixed(2)+' ');
		                // $(diff_id).html(' ('+change+'%)');
		                // $(diff_id).removeClass('positive');
		                // $(diff_id).removeClass('negetive');
		                // if(change < 0) {
		                //   $(diff_id).addClass('negetive');
		                // } else {
		                //   $(diff_id).addClass('positive');
		                // }
		              } else {
		                myDate = new Date(this.x);
		                $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		                $(value_id).text(' '+current_index.toFixed(2)+' ');
		                // $(diff_id).html('');
		              }
		            }
		          },    
		        },
		      }
		    },
	        series: [{
	            name: '',
	            type: 'area',
	            data: data,
	            gapSize: 0,
	            color : graphColor,
	            tooltip: {
	                valueDecimals: 2,
	                enabled: false,
	            },
	            fillColor: {
	                linearGradient: {
	                    x1: 0,
	                    y1: 0,
	                    x2: 0,
	                    y2: 1
	                },
	                stops: [
	                    [0, 'rgba(248, 132, 7, 0.5)'],
	                    [1, 'rgba(248, 132, 7, 0.0)'],
	                ]
	            },
	            threshold: null
	        }]
	    });
	}

	function selectedPolicy(policy){
		var formData = 'duration='+timespace+'&policy='+policy;
		$.ajax({
			type: "POST",
			url: "/selectedPolicy",
			data:formData,
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
				if (data.msg == 1) {
					graph_val = data.data;
					populate_data(graph_val);
				}else{
					alert('error!!!!!');
				}
			},
			error: function(xhr, status, error) {
				alert(error);
			}
		});
	}

	$('#'+chart_id).on('mouseleave', function(){
		$(date_id).text('');
		$(value_id).text('');
	});


});