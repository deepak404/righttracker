$(document).ready(function(){ 
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	$("#scheme-name").text($(".active-scheme").data('schemename'));
	$("#scheme_code").val($(".active-scheme").data('schemecode'));
	$("#asset").val($(".active-scheme").data('asset'));
	if ($(".active-scheme").data('exit') == 0) {
		$("#no_year").val(0);
		$("#percentage").val(0);
	}else{
		var data = $(".active-scheme").data('exit');
		var mydata = data.split('/');
		$("#no_year").val(mydata[0]);
		$("#percentage").val(mydata[1]);
	}
	var avg = $(".active-scheme").data('avg');
	var mod = $(".active-scheme").data('mod');
	var gov = $(".active-scheme").data('gov');
	var aa = $(".active-scheme").data('aa');
	var brk = $(".active-scheme").data('brk');
	$("#avg_mat").val(avg);
	$("#mod_dur").val(mod);
	$("#gov_sec").val(gov);
	$("#aa_rate").val(aa);
	$("#brk").val(brk);
	var ot = (aa+gov);
	$(".total-val").text(ot+'%');

	$(".scheme_list").on('click',function(){
			$(".active-scheme").removeClass('active-scheme');
			$(this).addClass('active-scheme');
			$("#scheme-name").text($(".active-scheme").data('schemename'));
			$("#scheme_code").val($(".active-scheme").data('schemecode'));
			$("#asset").val($(".active-scheme").data('asset'));
			if ($(".active-scheme").data('exit') == 0) {
				$("#no_year").val(0);
				$("#percentage").val(0);
			}else{
				var data = $(".active-scheme").data('exit');
				var mydata = data.split('/');
				$("#no_year").val(mydata[0]);
				$("#percentage").val(mydata[1]);
			}
			var avg = $(".active-scheme").data('avg');
			var mod = $(".active-scheme").data('mod');
			var gov = $(".active-scheme").data('gov');
			var aa = $(".active-scheme").data('aa');
			var brk = $(".active-scheme").data('brk');
			$("#avg_mat").val(avg);
			$("#mod_dur").val(mod);
			$("#gov_sec").val(gov);
			$("#aa_rate").val(aa);
			$("#brk").val(brk);
			var ot = (aa+gov);
			$(".total-val").text(ot+'%');
		});

		$('.class-split').on('keyup',function(){
			var sum = 0;
			if ($('#gov_sec').val() != '') {
				sum += parseFloat($('#gov_sec').val())
			}
			if ($('#aa_rate').val() != '') {
				sum += parseFloat($('#aa_rate').val())
			}
			$(".total-val").text(sum+'%');
		});	

		$('#debt-form').on('submit',function(e){
			e.preventDefault();
			var formData = $('#debt-form').serialize();
	        $.ajax({
	            type: 'POST',
	            url: '/add_debt',
	            data: formData,
	            success:function(data){
	            	if (data.msg == 1) {
		            	location.reload();
	            	}else{
	            		console.log(data.info);
	            	}
	            },
	            error:function(){ 
	                
	            }
	        });
		});
});