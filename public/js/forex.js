$(document).ready(function(){
	$( document ).ajaxStart(function() {
	  $('.overlay').css('display','block');
	});
	$( document ).ajaxStop(function() {
	  $('.overlay').css('display','none');
	});
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var graph_val = '';
	var data = '';
	var start_nav = '';
	var start_date;
	var range_stat = 0;

	getForex('usd','usd-chart', '#usd-date', '#usd-rate', '#usd-diff', 4);
	getForex('gbp','gbp-chart', '#gbp-date', '#gbp-rate', '#gbp-diff', 4);
	getForex('sgd','sgd-chart', '#sgd-date', '#sgd-rate', '#sgd-diff', 4);
	getForex('yen','yen-chart', '#yen-date', '#yen-rate', '#yen-diff', 4);
	getForex('chf','chf-chart', '#chf-date', '#chf-rate', '#chf-diff', 4);
	getForex('eur','eur-chart', '#eur-date', '#eur-rate', '#eur-diff', 4);

	function getForex(index, chart_id, date_id, value_id, diff_id, timespace){
		var formData = 'duration='+timespace+'&index='+index;
		$.ajax({
			type: "POST",
			url: "/getForex",
			data:formData,
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
				if (data.msg == 1) {
					graph_val = data.data;
					populate_data(chart_id,date_id,value_id,diff_id,graph_val);
				}else{
					alert('error!!!!!');
				}
			},
			error: function(xhr, status, error) {
				alert(error);
			}
		});
	}

	function populate_data(chart_id, date_id, value_id, diff_id, data) 
		{
		  Highcharts.stockChart(chart_id, {
			 xAxis: {
		            gapGridLineWidth: 0,
		            lineColor: '#232323',
		        },
		     yAxis: {
		            gridLineColor: '#232323'
		        },
		        rangeSelector: {
		            selected: 5,
		            enabled:false
		        },
			    credits: {
			        text: 'Righttracker',
			        href: 'http://www.righttracker.in'
			    },
		        chart: {
				      backgroundColor: chart_bg_color,
				      selectionMarkerFill: markerFillColor,
				                events: {
				              selection: function(event) {
				                  if (event.xAxis) {
				                      return false;
				                  }
				              }
				          },
				          zoomType: 'x'
				  },
				chart: {
				      backgroundColor: chart_bg_color,
				      selectionMarkerFill: markerFillColor,
				                events: {
				              selection: function(event) {
				                  if (event.xAxis) {
				                      return false;
				                  }
				              }
				          },
				          zoomType: 'x'
				  },
		        navigator: {
		            enabled: false
		        },
		        scrollbar: {
		            enabled: false
		        },
		        plotOptions: {
			      series: {
			        allowPointSelect: true,
			          marker: {
			              states: {
			                  select: {
			                      fillColor: 'red',
			                      lineWidth: 0
			                  }
			              }
			          },
			        point: {
			          events: {
			            mouseOver: function () {
			              if(range_stat == 1 && start_nav == '') {
			                var row_index = parseInt(this.index) - 1;
			                selected_nav = data[row_index];
			                start_date = new Date(data[row_index][0]);
			                start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
			              }
			              var current_index = parseFloat(this.y);
			              if(range_stat == 1 && start_nav != '' ) {
			                var diff = current_index - start_nav;
			                var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
			                myDate = new Date(this.x);
			                $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
			                $(value_id).text(' '+current_index.toFixed(2)+' ');
			                $(diff_id).html(' ('+change+'%)');
			                $(diff_id).removeClass('positive');
			                $(diff_id).removeClass('negetive');
			                if(change < 0) {
			                  $(diff_id).addClass('negetive');
			                } else {
			                  $(diff_id).addClass('positive');
			                }
			              } else {
			                myDate = new Date(this.x);
			                $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
			                $(value_id).text(' '+current_index.toFixed(2)+' ');
			                $(diff_id).html('');


			              }
			            }
			          },    
			        },
			      }
			    },
		        series: [{
		            name: 'EOD',
		            type: 'area',
		            data: data,
		            gapSize: 0,
		            color : graphColor,
		            tooltip: {
		                valueDecimals: 2,
		                enabled: false,
		            },
		            fillColor: {
		                linearGradient: {
		                    x1: 0,
		                    y1: 0,
		                    x2: 0,
		                    y2: 1
		                },
		                stops: [
		                    [0, 'rgba(248, 132, 7, 0.5)'],
		                    [1, 'rgba(248, 132, 7, 0.0)'],
		                ]
		            },
		            threshold: null
		        }]
		    });
		}


$('#usd-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#usd-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#usd-diff').text('');
		$('#usd-rate').text('');
		$('#usd-date').text('');
});

$('#chf-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#chf-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#chf-diff').text('');
		$('#chf-rate').text('');
		$('#chf-date').text('');
});

$('#eur-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#eur-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#eur-diff').text('');
		$('#eur-rate').text('');
		$('#eur-date').text('');
});

$('#gbp-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#gbp-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#gbp-diff').text('');
		$('#gbp-rate').text('');
		$('#gbp-date').text('');
});
$('#sgd-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#sgd-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#sgd-diff').text('');
		$('#sgd-rate').text('');
		$('#sgd-date').text('');
});

$('#yen-chart').on('mousedown', function() {
  range_stat = 1;
});

$('#yen-chart').on('mouseup', function() {
  range_stat = 0;
  start_nav = '';
  		$('#yen-diff').text('');
		$('#yen-rate').text('');
		$('#yen-date').text('');
});

$('.usd-li').on('click',function(){
	getForex('usd','usd-chart', '#usd-date', '#usd-rate', '#usd-diff', $(this).data('value'));
	$('#usd-btn').text($(this).text());
	$('#usd-btn').append('<span class="caret"></span>');
});
$('.gbp-li').on('click',function(){
	$('#gbp-btn').text($(this).text());
	$('#gbp-btn').append('<span class="caret"></span>');
	getForex('gbp','gbp-chart', '#gbp-date', '#gbp-rate', '#gbp-diff', $(this).data('value'));
});
$('.sgd-li').on('click',function(){
	$('#sgd-btn').text($(this).text());
	$('#sgd-btn').append('<span class="caret"></span>');
	getForex('sgd','sgd-chart', '#sgd-date', '#sgd-rate', '#sgd-diff', $(this).data('value'));
});
$('.yen-li').on('click',function(){
	$('#yen-btn').text($(this).text());
	$('#yen-btn').append('<span class="caret"></span>');
	getForex('yen','yen-chart', '#yen-date', '#yen-rate', '#yen-diff', $(this).data('value'));
});
$('.chf-li').on('click',function(){
	$('#chf-btn').text($(this).text());
	$('#chf-btn').append('<span class="caret"></span>');
	getForex('chf','chf-chart', '#chf-date', '#chf-rate', '#chf-diff', $(this).data('value'));
});
$('.eur-li').on('click',function(){
	$('#eur-btn').text($(this).text());
	$('#eur-btn').append('<span class="caret"></span>');
	getForex('eur','eur-chart', '#eur-date', '#eur-rate', '#eur-diff', $(this).data('value'));
});
	$('#usd-chart').on('mouseleave', function(){
		$('#usd-diff').text('');
		$('#usd-rate').text('');
		$('#usd-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#gbp-chart').on('mouseleave', function(){
		$('#gbp-diff').text('');
		$('#gbp-rate').text('');
		$('#gbp-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#sgd-chart').on('mouseleave', function(){
		$('#sgd-diff').text('');
		$('#sgd-rate').text('');
		$('#sgd-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#yen-chart').on('mouseleave', function(){
		$('#yen-diff').text('');
		$('#yen-rate').text('');
		$('#yen-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#chf-chart').on('mouseleave', function(){
		$('#chf-diff').text('');
		$('#chf-rate').text('');
		$('#chf-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
	$('#eur-chart').on('mouseleave', function(){
		$('#eur-diff').text('');
		$('#eur-rate').text('');
		$('#eur-date').text('');
		  range_stat = 0;
		  start_nav = '';
	});
});