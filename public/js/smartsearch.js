$(document).ready(function(){
	// var Sensex = '<a data-schemecode="bse" data-fundtype = "0" class="list-group-item suggested-user"><span class="suggested-name">Sensex</span></a>';
	// var Nifty = '<a data-schemecode="nse" data-fundtype = "0" class="list-group-item suggested-user"><span class="suggested-name">Nifty</span></a>';
	// var smallCap = '<a data-schemecode="small" data-fundtype = "0" class="list-group-item suggested-user"><span class="suggested-name">Small Cap</span></a>';
	// var midCap = '<a data-schemecode="mid" data-fundtype = "0" class="list-group-item suggested-user"><span class="suggested-name">Mid Cap</span></a>';
	var valdata = new Bloodhound({
		datumTokenizer:Bloodhound.tokenizers.whitespace('scheme_name'),
		queryTokenizer:Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/scheme_search?search-input=%QUERY%',
			wildcard: '%QUERY%',
		},
	});
	valdata.initialize();
	$('#fundSearch').typeahead(
	{
		hint: !1,
		highlight: !1,
		minLength: 0,

	},
	{
		source:valdata.ttAdapter(),
		name:'fundList',
		source:valdata,
		displayKey:'scheme_name',
		limit: 20,
		templates: {
			// empty:[Sensex+Nifty+smallCap+midCap],
			empty:['<div class = "suggestion no-user">No Schemes Found!</div>'],
			header:['<div class="list-group search-results-dropdown">'],
			suggestion:function(data){
				// data=data;
				console.log(data);
				var schemes = [];
				schemes = get_active_scheme_codes();
				if ($.inArray(data.scheme_code.toString(), schemes) == -1) {
					return '<a data-schemecode="'+data.scheme_code+'" data-fundtype = '+data.fund_type+' class="list-group-item suggested-user"> <span class="suggested-name">'+data.scheme_name+'</span></a>'
				}else{
					return '<div class = "suggestion no-user"></div>';
				}
			}
		}
	})

    function get_active_scheme_codes()
    {
        var scheme_codes=[];
        $('.fund_compare_card').each(function() {
                scheme_codes.push($(this).data('id').toString());
        });
        return scheme_codes;
    }



});