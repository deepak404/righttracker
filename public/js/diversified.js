$(document).ready(function(){
	var $divs = $("div.data_border");
	$('.d_icon').on('click',function(){
		if ($(this).data('sort')=='asc') {
			$('div').removeClass('active-sort');
			$(this).data('sort', 'desc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(a).find("div.d_val").text() - $(b).find("div.d_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
			$('.d_head').addClass('active-sort');
		    });
		}else{
			$('div').removeClass('active-sort');
			$(this).data('sort', 'asc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(b).find("div.d_val").text() - $(a).find("div.d_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.d_head').addClass('active-sort');
		}
	});
	$('.w_icon').on('click',function(){
		if ($(this).data('sort')=='asc') {
			$('div').removeClass('active-sort');
			$(this).data('sort', 'desc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(a).find("div.w_val").text() - $(b).find("div.w_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
			$('.w_head').addClass('active-sort');
		    });
		}else{
			$('div').removeClass('active-sort');
			$(this).data('sort', 'asc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(b).find("div.w_val").text() - $(a).find("div.w_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.w_head').addClass('active-sort');
		}
	});
	$('.m_icon').on('click',function(){
		if ($(this).data('sort')=='asc') {
			$('div').removeClass('active-sort');
			$(this).data('sort', 'desc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(a).find("div.m_val").text() - $(b).find("div.m_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.m_head').addClass('active-sort');
		}else{
			$('div').removeClass('active-sort');
			$(this).data('sort', 'asc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(b).find("div.m_val").text() - $(a).find("div.m_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.m_head').addClass('active-sort');
		}
	});
	$('.y_icon').on('click',function(){
		if ($(this).data('sort')=='asc') {
			$('div').removeClass('active-sort');
			$(this).data('sort', 'desc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(a).find("div.y_val").text() - $(b).find("div.y_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.y_head').addClass('active-sort');
		}else{
			$('div').removeClass('active-sort');
			$(this).data('sort', 'asc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(b).find("div.y_val").text() - $(a).find("div.y_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.y_head').addClass('active-sort');
		}
	});
	$('.ys_icon').on('click',function(){
		if ($(this).data('sort')=='asc') {
			$('div').removeClass('active-sort');
			$(this).data('sort', 'desc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(a).find("div.ys_val").text() - $(b).find("div.ys_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.ys_head').addClass('active-sort');
		}else{
			$('div').removeClass('active-sort');
			$(this).data('sort', 'asc');
		    var numericallyOrderedDivs = $divs.sort(function (a, b) {
		        return $(b).find("div.ys_val").text() - $(a).find("div.ys_val").text();
		    });
			$(".data-container").empty();
			var masterCount = 0;
		    $.each(numericallyOrderedDivs, function(index, value){
		    	if (masterCount != 5) {
					$(".data-container").append(value);
		    	}else{
		    		$(".data-container").append('<h4 class="padding_tb allscheme-title">All Schemes</h4>'+
									            '<div class="container table_padding">'+
									              '<div class="head_border  table_mod" >'+  
									                '<div class=" Schemes_data ">'+
									                  '<span class="col-lg-1 padding-lr-zero "></span>'+
									                  '<span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>'+
									                '</div>'+
										                '<div class="NAV_head " >Todays NAV</div>'+
										                '<div class="d_head">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
												        '<div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>'+
										                '<div class="asset_size_head ">Asset size  Cr.</div>'+
										              '</div>'+    
										            '</div>');
					$(".data-container").append(value);
		    		
		    	}
		    	masterCount += 1;
		    });
			$('.ys_head').addClass('active-sort');
		}
	});
});