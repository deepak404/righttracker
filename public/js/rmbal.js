$(document).ready(function(){ 
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	$("#scheme-name").text($(".active-scheme").data('schemename'));
	$("#scheme_code").val($(".active-scheme").data('schemecode'));
	$("#asset").val($(".active-scheme").data('asset'));
	if ($(".active-scheme").data('exit') == 0) {
		$("#no_year").val(0);
		$("#percentage").val(0);
	}else{
		var data = $(".active-scheme").data('exit');
		var mydata = data.split('/');
		$("#no_year").val(mydata[0]);
		$("#percentage").val(mydata[1]);
	}
	var smallcap = $(".active-scheme").data('smallcap');
	var midcap = $(".active-scheme").data('midcap');
	var largecap = $(".active-scheme").data('largecap');
	var debt = $(".active-scheme").data('debt');
	var brk = $(".active-scheme").data('brk');
	$("#small_cap").val(smallcap);
	$("#mid_cap").val(midcap);
	$("#larg_cap").val(largecap);
	$("#debt").val(debt);
	$("#brk").val(brk);
	var ot = (smallcap+largecap+midcap+debt);
	$(".total-val").text(ot+'%');

	$(".scheme_list").on('click',function(){
		$(".active-scheme").removeClass('active-scheme');
		$(this).addClass('active-scheme');
		$("#scheme-name").text($(".active-scheme").data('schemename'));
		$("#scheme_code").val($(".active-scheme").data('schemecode'));
		$("#asset").val($(".active-scheme").data('asset'));
		if ($(".active-scheme").data('exit') == 0) {
			$("#no_year").val(0);
			$("#percentage").val(0);
		}else{
			var data = $(".active-scheme").data('exit');
			var mydata = data.split('/');
			$("#no_year").val(mydata[0]);
			$("#percentage").val(mydata[1]);
		}
		var smallcap = $(".active-scheme").data('smallcap');
		var midcap = $(".active-scheme").data('midcap');
		var largecap = $(".active-scheme").data('largecap');
		var brk = $(".active-scheme").data('brk');
		$("#small_cap").val(smallcap);
		$("#mid_cap").val(midcap);
		$("#larg_cap").val(largecap);
		$("#debt").val(debt);
		$("#brk").val(brk);
		var ot = (smallcap+largecap+midcap+debt);
		$(".total-val").text(ot+'%');
	});

	$('.class-split').on('keyup',function(){
		var sum = 0;
		if ($('#small_cap').val() != '') {
			sum += parseFloat($('#small_cap').val())
		}
		if ($('#mid_cap').val() != '') {
			sum += parseFloat($('#mid_cap').val())
		}
		if ($('#larg_cap').val() != '') {
			sum += parseFloat($('#larg_cap').val());
		}
		if ($('#debt').val() != '') {
			sum += parseFloat($('#debt').val());
		}
		$(".total-val").text(sum+'%');
	});	

	$('#bal-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#bal-form').serialize();
        $.ajax({
            type: 'POST',
            url: '/add_hybrid',
            data: formData,
            success:function(data){
            	if (data.msg == 1) {
	            	location.reload();
            	}else{
            		console.log(data.info);
            	}
            },
            error:function(){ 
                
            }
        });
	});



});