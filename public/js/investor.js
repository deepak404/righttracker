$(document).ready(function(){ 
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var range_stat = 0;
	var timespace = 4;
	var data = '';
	var start_nav = '';
	var start_date;
	var start_index;
	var range_stat = 0;
	var key = '';
	var scheme_details_info = '';
	var master_scheme_code = '';
	var master_fund_type = '';

	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var username = '';
	$(document).on('click', '.add-scheme', function() {
    	$('#search-scheme').val('');
        var code = $(this).data('schemecode');
        var type = $(this).data('fundtype');
        var scheme_name = $(this).data('schemename');
		$('#tag-input').tagsinput('add', code);

    });
	$(document).on('click', '.add-name', function() {
		username = $(this).data('name');
		$('#inv_id').val($(this).data('invid'));
		console.log()
    	$('#name-input').val(username);
    });
    $('#portfolio-form').on('submit',function(e){
    	$('.overlay').css('display','block');
    	e.preventDefault();
    	var formData = $('#portfolio-form').serialize();
    	$.ajax({
    		type:'POST',
    		url:'/track_user',
    		data:formData,
    		success:function(data){
		    	$('.overlay').css('display','none');
    			var bal = data.balanced_val;
    			var debt = data.debt_val;
    			var equity = data.equity_val;
    			var eq_info = data.equity_detail;
    			var db_info = data.debt_detail;
    			var bal_info = data.balanced_detail;
    			create_graph('sensex-chart','#date',equity.sensex);
    			create_options(eq_info.sensex,'#sensex-info');
    			create_graph('small-chart','#date',equity.small_cap);
    			create_options(eq_info.small_cap,'#small-info');
    			create_graph('mid-chart','#date',equity.mid_cap);
    			create_options(eq_info.mid_cap,'#mid-info');
    			create_graph('gov-chart','#date',debt.gov);
    			create_options(db_info.gov,'#gov-info');
    			create_graph('fd-chart','#date',debt.fd);
    			create_options(db_info.fd,'#fd-info');
    			create_graph('bal-chart','#date',bal);
    			create_options(bal_info,'#bal-info');
    		},
    		error:function(error){
    			console.log(error);
    		}
    	})

    });
	$('.data-input').on('focus',function(){
    	$('#name-input').val(username);
	});
	$today = new Date();
	$yesterday = new Date($today);
	$yesterday.setDate($today.getDate() - 1); 
	$('.date-input').datepicker({  maxDate: $yesterday });
	$('#add-investor').on('click',function(){
		$('#inv_name').val('');
		$('#investorModal').modal('show');
	});
	$('#investor-form').on('submit', function(e){
		e.preventDefault()
		var formData = $('#investor-form').serialize();
		$.ajax({
			type:'POST',
			url:'/add_investor',
			data:formData,
			success:function(data){
				$('#investorModal').modal('hide');
			},
			error:function(error){
				console.log(error);
			}
		});
	});
	$('#add-investments').on('click',function(){
		$.ajax({
			type:'GET',
			url:'/get_group',
			success:function(data){
				$('#inv_name_select').empty();
				$.each(data.user,function(key,value){
					$('#inv_name_select').append('<option value="'+value['id']+'">'+value['name']+'</option>');
				});
				$('#tag-input').tagsinput('removeAll');
				$('#investmentModal').modal('show');
			},
			error:function(error){
				
			}
		})
	});
	$('#investment-form').on('submit', function(e){
		e.preventDefault();
		var inv_name = $('#investment-form').serialize();
		var schemecode = $('#tag-input').val();
		var formData = inv_name+'&scheme_code='+schemecode;
		$.ajax({
			type:'POST',
			url:'/add_invetments',
			data:formData,
			success:function(data){
				$('#investmentModal').modal('hide');
			},
			error:function(error){
				console.log(error);
			}

		})
	});

	var mydata = new Bloodhound({
		datumTokenizer:Bloodhound.tokenizers.whitespace('user_name'),
		queryTokenizer:Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/user_search?search-input=%QUERY%',
			wildcard: '%QUERY%',
		},
	});
	mydata.initialize();
	$('#name-input').typeahead(
	{
		hint: !1,
		highlight: !1,
		minLength: 0,

	},
	{
		source:mydata.ttAdapter(),
		name:'namelist',
		source:mydata,
		displayKey:'user_name',
		limit: 20,
		templates: {
			empty:['<div class = "suggestion no-user">No Schemes Found!</div>'],
			header:['<div class="list-group search-results-dropdown">'],
			suggestion:function(data){
				return '<a data-name="'+data.name+'" data-invid="'+data.id+'" class="list-group-item suggested-user add-name"> <span class="suggested-name">'+data.name+'</span></a>'
			}
		}
	});


	var valdata = new Bloodhound({
		datumTokenizer:Bloodhound.tokenizers.whitespace('scheme_name'),
		queryTokenizer:Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/scheme_search?search-input=%QUERY%',
			wildcard: '%QUERY%',
		},
	});
	valdata.initialize();
	$('#search-scheme').typeahead(
	{
		hint: !1,
		highlight: !1,
		minLength: 0,

	},
	{
		source:valdata.ttAdapter(),
		name:'fundList',
		source:valdata,
		displayKey:'scheme_name',
		limit: 20,
		templates: {
			empty:['<div class = "suggestion no-user">No Schemes Found!</div>'],
			header:['<div class="list-group search-results-dropdown">'],
			suggestion:function(data){
				return '<a data-schemecode="'+data.scheme_code+'" data-fundtype = '+data.fund_type+' data-schemename="'+data.scheme_name+'" class="list-group-item suggested-user add-scheme"> <span class="suggested-name">'+data.scheme_name+'</span></a>'
			}
		}
	});

    function create_graph(chart_id, diff_id, data) 
  	{
	    // Create the chart
	    var data1 = [];
	    var data2 = [];
	    var data3 = [];
	    var data4 = [];
	    var data5 = [];
	    var data6 = [];
	    var data7 = [];
	    var data8 = [];
	    var key1 = key2 = key3 = key4 = key5 = key6 = key7 = key8 = '';
	    var count =1;
	    $.each(data,function(key,value) {//console.log(value);
			switch(count) {
				case 1:
				data1 = value;
				key1 = key;
				break;
				case 2:
				data2 = value;
				key2 = key;
				break;
				case 3:
				data3 = value;
				key3 = key;
				break;
				case 4:
				data4 = value;
				key4 = key;
				break;
				case 5:
				data5 = value;
				key5 = key;
				break;
				case 6:
				data6 = value;
				key6 = key;
				break;
				case 7:
				data7 = value;
				key7 = key;
				break;
				case 8:
				data8 = value;
				key8 = key;
				break;
			}
			count++;//console.log(data1);console.log(key1);
	    });
	    Highcharts.stockChart(chart_id, {
			xAxis: {
				gapGridLineWidth: 0,
				lineColor: '#232323',
			},
			yAxis: {
			    gridLineColor: '#232323'
			},
		    credits: {
		        text: 'Righttracker',
		        href: 'http://www.righttracker.in'
		    },
	      chart: {
	        backgroundColor: chart_bg_color,
	        selectionMarkerFill: markerFillColor,
	                  events: {
	                selection: function(event) {
	                    if (event.xAxis) {
	                        // $report.html('min: '+ event.xAxis[0].min +', max: '+ event.xAxis[0].max);
	                        return false;
	                    }
	                }
	            },
	            zoomType: 'x'
	      
	    },
	    rangeSelector: {
	        selected: 5,
	        enabled:false
	      },
	      exporting: {
	      	enabled:false,
	        chartOptions: { 
	          rangeSelector: { 
	            enabled: false 
	          } 
	        } 
	      },
	      scrollbar : {
	      	enabled:false
	      },

	      navigator:{
	      	enabled : false
	      },

	      title: {
	        //text: 'USD - INR Exchange Rate'
	      },

	      tooltip: {
	          valueDecimals: 2,
	          enabled: false
	      },

	      plotOptions: {
	        series: {
	          allowPointSelect: true,
	            marker: {
	                states: {
	                    select: {
	                        fillColor: 'red',
	                        lineWidth: 0
	                    }
	                }
	            },
	          point: {
	            events: {
	              mouseOver: function () {
	              	$('#fundSearch').blur();
	              	if((key == 0 || key == '' || range_stat !=1)) {//console.log('key may be empty or zero');
	              		key = this.series.userOptions.name;
	              	}
	              	console.log(key);
	              	var scheme_data = [];
	              	switch (key) {
	              		case 1:{
	              			scheme_data = data1;
	              			break;
	              		}
	              		case 2:{
	              			scheme_data = data2;
	              			break;
	              		}
	              		case 3:{
	              			scheme_data = data3;
	              			break;
	              		}
	              		case 4:{
	              			scheme_data = data4;
	              			break;
	              		}
	              		case 5:{
	              			scheme_data = data5;
	              			break;
	              		}
	              		case 6:{
	              			scheme_data = data6;
	              			break;
	              		}
	              		case 7:{
	              			scheme_data = data7;
	              			break;
	              		}
	              		case 8:{
	              			scheme_data = data8;
	              			break;
	              		}
	              	}//console.log(scheme_data);
	              	//var data = 
	              	if(parseInt(this.index) > 0) {
	              		var row_index = parseInt(this.index);console.log(row_index);
	              	} else {
	              		var row_index = parseInt(this.index);console.log(row_index);
	              	}console.log(row_index);
	                if(range_stat == 1 && start_nav == '') {
	                  //var row_index = parseInt(this.index) - 1;
	                  selected_nav = scheme_data[row_index];//console.log(selected_nav);
	                  start_nav = parseFloat(scheme_data[row_index][2]);//console.log(start_nav);console.log(selected_nav);
	                  start_date = new Date(scheme_data[row_index][0]);
	                  start_index = row_index;
	                }

	                if(range_stat == 1 && start_nav != '' ) {
	                  $('#to_date').text('');
	                  var current_nav = scheme_data[row_index][2];
	                  var diff = current_nav - start_nav;
	                  var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
	                  myDate = new Date(this.x);
	                  // $('#from_date').text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear());
	                  // $('#to_date').text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
	                  $('#from_date').text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' To '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear()+ ' ');
	                  $('#index-value').text('');
	                  $('.compare_nav').css('display','inline');
					  $('.period_change').css('display','none');
	                  if(data1 != ''){
	                  	var start_value = data1[start_index][2];
	                  	var current_value = data1[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data0-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data0-change").removeClass('negetive');
						$("#data0-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data0-change").addClass('negetive');
	                  	} else {
	                  		$("#data0-change").addClass('positive');
	                  	}
	                  }
	                  if(data2 != ''){
	                  	var start_value = data2[start_index][2];
	                  	var current_value = data2[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data1-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data1-change").removeClass('negetive');
						$("#data1-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data1-change").addClass('negetive');
	                  	} else {
	                  		$("#data1-change").addClass('positive');
	                  	}
	                  }
	                  if(data3 != ''){
	                  	var start_value = data3[start_index][2];
	                  	var current_value = data3[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data2-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data2-change").removeClass('negetive');
						$("#data2-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data2-change").addClass('negetive');
	                  	} else {
	                  		$("#data2-change").addClass('positive');
	                  	}
					  }
	                  if(data4 != ''){
	                  	var start_value = data4[start_index][2];
	                  	var current_value = data4[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data3-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data3-change").removeClass('negetive');
						$("#data3-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data3-change").addClass('negetive');
	                  	} else {
	                  		$("#data3-change").addClass('positive');
	                  	}
	                  }
	                  if(data5 != ''){
	                  	var start_value = data5[start_index][2];
	                  	var current_value = data5[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data4-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data4-change").removeClass('negetive');
						$("#data4-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data4-change").addClass('negetive');
	                  	} else {
	                  		$("#data4-change").addClass('positive');
	                  	}
	                  }
	                  if(data6 != ''){
	                  	var start_value = data6[start_index][2];
	                  	var current_value = data6[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data5-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data5-change").removeClass('negetive');
						$("#data5-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data5-change").addClass('negetive');
	                  	} else {
	                  		$("#data5-change").addClass('positive');
	                  	}
	                  }
	                  if(data6 != ''){
	                  	var start_value = data6[start_index][2];
	                  	var current_value = data6[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data5-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data5-change").removeClass('negetive');
						$("#data5-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data5-change").addClass('negetive');
	                  	} else {
	                  		$("#data5-change").addClass('positive');
	                  	}
	                  }
	                  if(data7 != ''){
	                  	var start_value = data7[start_index][2];
	                  	var current_value = data7[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data6-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data6-change").removeClass('negetive');
						$("#data6-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data6-change").addClass('negetive');
	                  	} else {
	                  		$("#data6-change").addClass('positive');
	                  	}
	                  }
	                  if(data8 != ''){
	                  	var start_value = data8[start_index][2];
	                  	var current_value = data8[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data7-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data7-change").removeClass('negetive');
						$("#data7-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data7-change").addClass('negetive');
	                  	} else {
	                  		$("#data7-change").addClass('positive');
	                  	}
	                  }
	                } else {
	                  myDate = new Date(this.x);//console.log(this.series);console.log(this.series.userOptions);console.log(this.series.userOptions.name);
	                  $(diff_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear()+ ' ');
	                  $('#to_date').text(scheme_data[row_index][2]+' ');
	                }
	              },
	              mouseDown: function(){
	              	//console.log('into the click event function');
	              }
	            },    
	          },
	        }
	      },
	      series: [
			{
				name: 1,
				color:'#0091ea',
				data: data1
			}, {
				name: 2,
				color:'#BF71FF',
				data: data2
			},{
				name: 3,
				color: '#3EC963',
				data: data3
			}, {
				name: 4,
				color: '#FF697A',
				data: data4
			}, {
				name: 5,
				color: '#F88407',
				data: data5
			}, {
				name: 6,
				color: '#F80786',
				data: data6
			}, {
				name: 7,
				color: '#E3F807',
				data: data7
			}, {
				name: 8,
				color: '#FD2B2B',
				data: data8
			},

			],
	    });
	}

	function create_options(scheme_details,div_id) 
	{
		console.log(scheme_details);
		var new_options = '';
		var colors = ['#0091ea','#BF71FF','#3EC963','#FF697A','#F88407','#F80786','#E3F807','#FD2B2B'];
		var count = 0;
		$(div_id).empty();
		// var length = scheme_details.length;
		$.each(scheme_details, function(key, value){
			console.log(value);
			console.log(div_id);
			if (value.per > 0) {
				$(div_id).append('<div class="card fund_compare_card" style="border-left: 4px solid '+colors[count]+';">'+ 
		        '<h4 class="fund_compare_name">'+value.scheme_name+'</h4>'+
		        '<h3 class="nav_value">'+value.nav+'<span class="positive nav-spacing" id="data'+count+'-change">('+value.per+'%)</span></h3>'+
				'</div>');
			}else{
				$(div_id).append('<div class="card fund_compare_card" style="border-left: 4px solid '+colors[count]+';">'+ 
		        '<h4 class="fund_compare_name">'+value.scheme_name+'</h4>'+
		        '<h3 class="nav_value">'+value.nav+'<span class="negetive nav-spacing" id="data'+count+'-change">('+value.per+'%)</span></h3>'+
				'</div>');
			}
		count += 1;
		});
	}

	$('#delete-data').on('click', function(){
		$.ajax({
			type:'GET',
			url:'/get_inv',
			success:function(data){
				$('.schemes-tbody').empty();
					$('.schemes-tbody').append('<tr>'+
					                  '<td>No data Found.</td>'+
						              '</tr>');
				$('#delete-user').attr('data-user','0');
				$('#inv_name_del').empty();
				$('#inv_name_del').append('<option value="0" selected disabled>Pick Your Investor</option>');
				$.each(data.inv_name, function(key,value){
					$('#inv_name_del').append('<option value="'+value.id+'">'+value.name+'</option>');
				});
				$('#deleteModal').modal('show');
			},
			error:function(error){
				console.log(error);
			}
		});
	});

	$('#inv_name_del').on('change', function(){
		var data = 'user_id='+$(this).val();
		$('#delete-user').attr('data-user',$(this).val());
		$.ajax({
			type:'POST',
			url:'/get_investments',
			data:data,
			success:function(data){
				$('.schemes-tbody').empty();
				$.each(data.val,function(key,value){
					$('.schemes-tbody').append('<tr>'+
					                  '<td>'+value.name+'</td>'+
					                  '<td><i class="material-icons delete-scheme" data-invid="'+value.id+'">delete</i></td>'+
						              '</tr>');
				});
			},
			error:function(error){

			}
		});
	});
	$(document).on('click','.delete-scheme',function(){
		var data = 'inv_id='+$(this).data('invid');
		$(this).parent().parent().remove();
		$.ajax({
			type:'POST',
			url:'/remove_scheme',
			data:data,
			success:function(data){
				console.log(data);
			},
			error:function(error){
				console.log(error);
			}
		})

	});

	$(document).on('click','#delete-user',function(){
		var data = 'inv_id='+$(this).attr('data-user');
		$.ajax({
			type:'POST',
			url:'/remove_user',
			data:data,
			success:function(data){
				$('#deleteModal').modal('hide');
			},
			error:function(error){
				console.log(error);
			}
		})

	});

});