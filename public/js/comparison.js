$(document).ready(function(){ 
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var range_stat = 0;
	var timespace = 4;
	var data = '';
	var start_nav = '';
	var start_date;
	var start_index;
	var range_stat = 0;
	var key = '';
	var scheme_details_info = '';
	var master_scheme_code = '';
	var master_fund_type = '';
    $(document).on('click', '.suggested-user', function() {
    	$('#fundSearch').val('');
        var code = $(this).data('schemecode');
        var scheme_codes = code+'|';
        var type = $(this).data('fundtype');
    	var fund_type = type+'|';
        if($('.fund_compare_card').length < 12) {
        	$('.fund_compare_card').each(function() {
	        		scheme_codes += $(this).data('id')+'|';
	        		fund_type += $(this).data('fundtype')+'|';
	        });
        }
        console.log(scheme_codes);
        console.log(fund_type);
        get_scheme_info(scheme_codes, fund_type, timespace);

    });

    function get_scheme_info(scheme_code, fundtype, dur){
	  $('.overlay').css('display','block')
	    	var formData = 'scheme_code='+scheme_code+'&fund_type='+fundtype+'&dur='+dur;
			$.ajax({
				type: "POST",
				url: "/get_data",
				data:formData,
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data) {
				  $('.overlay').css('display','none')
					if (data.msg == 1) {
						scheme_details_info = '';
						scheme_details_info = data.scheme_details;
						create_options(data.scheme_details);
						create_graph('graph-view-div','#from_date',data.data);
					}else{
						// alert('error!!!!!');
					}
				},
				error: function(xhr, status, error) {
				  $('.overlay').css('display','none')
					alert(error);
				}
			});
    }

    $(document).on('click', '.close', function() {
    	// alert( $(this).parent().data('id'));
    	var id = $(this).parent().data('id');
    	$('div').remove('#'+id);
    	var scheme_codes = '';
		var fund_type = '';
        if($('.fund_compare_card').length < 12) {
        	$('.fund_compare_card').each(function() {
	        		scheme_codes += $(this).data('id')+'|';
	        		fund_type += $(this).data('fundtype')+'|';
	        });
        }
        if (scheme_codes != '') {
	        get_scheme_info(scheme_codes, fund_type, timespace);
        }else{
			var chart = $('#graph-view-div').highcharts();
	        var seriesLength = chart.series.length;
	        console.log('length'+seriesLength);
	        for(var i = seriesLength -1; i > -1; i--) {
	            chart.series[i].remove();
				master_scheme_code = '';
				master_fund_type = '';
				$('.fund-beater').css('display','none');
	        }
        }

    });

	$('#graph-view-div').on('mouseleave', function(){
		$('#from_date').text('DD-MM-YYYY');
		$('#to_date').text('');
	    range_stat = 0;
	    key = '';
	    start_nav = '';
	    $count = 0;
	    $.each(scheme_details_info, function(key,value){
			$('#data'+$count+'-change').text(value['current_per'].toFixed(2)+'%');
			$('#data'+$count+'-change').removeClass('negetive');
			$('#data'+$count+'-change').removeClass('positive');
			if(value['current_per'] < 0) {
				$('#data'+$count+'-change').addClass('negetive');
			} else {
				$('#data'+$count+'-change').addClass('positive');
			}
			$count += 1;
	    });
	});

  //   $(document).on('change', '.graph-years', function() {
  //   	timespace = $('.graph-years').val();
  //   	var scheme_codes = '';
		// var fund_type = '';
  //       if($('.fund_compare_card').length < 9) {
  //       	$('.fund_compare_card').each(function() {
	 //        		scheme_codes += $(this).data('id')+'|';
	 //        		fund_type += $(this).data('fundtype')+'|';
	 //        });
  //       }
  //       if (scheme_codes != '') {
	 //        get_scheme_info(scheme_codes, fund_type, timespace);
  //       }
  //   });

    function create_graph(chart_id, diff_id, data) 
  	{
	    // Create the chart
	    var data1 = [];
	    var data2 = [];
	    var data3 = [];
	    var data4 = [];
	    var data5 = [];
	    var data6 = [];
	    var data7 = [];
	    var data8 = [];
	    var data9 = [];
	    var data10 = [];
	    var data11 = [];
	    var data12 = [];
	    var key1 = key2 = key3 = key4 = key5 = key6 = key7 = key8 = key9 = key10 = key11 = key12 = '';
	    var count =1;
	    $.each(data,function(key,value) {//console.log(value);
			switch(count) {
				case 1:
				data1 = value;
				key1 = key;
				break;
				case 2:
				data2 = value;
				key2 = key;
				break;
				case 3:
				data3 = value;
				key3 = key;
				break;
				case 4:
				data4 = value;
				key4 = key;
				break;
				case 5:
				data5 = value;
				key5 = key;
				break;
				case 6:
				data6 = value;
				key6 = key;
				break;
				case 7:
				data7 = value;
				key7 = key;
				break;
				case 8:
				data8 = value;
				key8 = key;
				break;
				case 9:
				data9 = value;
				key9 = key;
				break;
				case 10:
				data10 = value;
				key10 = key;
				break;
				case 11:
				data11 = value;
				key11 = key;
				break;
				case 12:
				data12 = value;
				key12 = key;
				break;
			}
			count++;//console.log(data1);console.log(key1);
	    });
	    Highcharts.stockChart(chart_id, {
			xAxis: {
				gapGridLineWidth: 0,
				lineColor: '#232323',
				labels:{
					style:{
						fontSize:'15px'
					}
				}
			},
			yAxis: {
				gridLineColor: '#232323',
				labels:{
					style:{
						fontSize:'18px'
					}
				}
			},
		    credits: {
		        text: 'Righttracker',
		        href: 'http://www.righttracker.in'
		    },
	      chart: {
	        backgroundColor: chart_bg_color,
	        selectionMarkerFill: markerFillColor,
	                  events: {
	                selection: function(event) {
	                    if (event.xAxis) {
	                        // $report.html('min: '+ event.xAxis[0].min +', max: '+ event.xAxis[0].max);
	                        return false;
	                    }
	                }
	            },
	            zoomType: 'x'
	      
	    },
	    rangeSelector: {
	        selected: 12,
	        enabled:false
	      },
	      exporting: {
	      	enabled:false,
	        chartOptions: { 
	          rangeSelector: { 
	            enabled: false 
	          } 
	        } 
	      },
	      scrollbar : {
	      	enabled:false
	      },

	      navigator:{
	      	enabled : false
	      },

	      title: {
	        //text: 'USD - INR Exchange Rate'
	      },

	      tooltip: {
	          valueDecimals: 2,
	          enabled: false
	      },

	      plotOptions: {
	        series: {
	          allowPointSelect: true,
	            marker: {
	                states: {
	                    select: {
	                        fillColor: 'red',
	                        lineWidth: 0
	                    }
	                }
	            },
	          point: {
	            events: {
	              mouseOver: function () {
	              	$('#fundSearch').blur();
	              	if((key == 0 || key == '' || range_stat !=1)) {//console.log('key may be empty or zero');
	              		key = this.series.userOptions.name;
	              	}
	              	console.log(key);
	              	var scheme_data = [];
	              	switch (key) {
	              		case 1:{
	              			scheme_data = data1;
	              			break;
	              		}
	              		case 2:{
	              			scheme_data = data2;
	              			break;
	              		}
	              		case 3:{
	              			scheme_data = data3;
	              			break;
	              		}
	              		case 4:{
	              			scheme_data = data4;
	              			break;
	              		}
	              		case 5:{
	              			scheme_data = data5;
	              			break;
	              		}
	              		case 6:{
	              			scheme_data = data6;
	              			break;
	              		}
	              		case 7:{
	              			scheme_data = data7;
	              			break;
	              		}
	              		case 8:{
	              			scheme_data = data8;
	              			break;
						}
						case 9:{
							scheme_data = data9;
							break;
						}
						case 10:{
							scheme_data = data10;
							break;
						}
						case 11:{
							scheme_data = data11;
							break;
						}
						case 12:{
							scheme_data = data12;
							break;
						}
	              	}//console.log(scheme_data);
	              	//var data = 
	              	if(parseInt(this.index) > 0) {
	              		var row_index = parseInt(this.index);console.log(row_index);
	              	} else {
	              		var row_index = parseInt(this.index);console.log(row_index);
	              	}console.log(row_index);
	                if(range_stat == 1 && start_nav == '') {
	                  //var row_index = parseInt(this.index) - 1;
	                  selected_nav = scheme_data[row_index];//console.log(selected_nav);
	                  start_nav = parseFloat(scheme_data[row_index][2]);//console.log(start_nav);console.log(selected_nav);
	                  start_date = new Date(scheme_data[row_index][0]);
	                  start_index = row_index;
	                }

	                if(range_stat == 1 && start_nav != '' ) {
	                  $('#to_date').text('');
	                  var current_nav = scheme_data[row_index][2];
	                  var diff = current_nav - start_nav;
	                  var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
	                  myDate = new Date(this.x);
	                  // $('#from_date').text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear());
	                  // $('#to_date').text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
	                  $('#from_date').text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' To '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear()+ ' ');
	                  $('#index-value').text('');
	                  $('.compare_nav').css('display','inline');
					  $('.period_change').css('display','none');
	                  if(data1 != ''){
	                  	var start_value = data1[start_index][2];
	                  	var current_value = data1[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data0-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data0-change").removeClass('negetive');
						$("#data0-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data0-change").addClass('negetive');
	                  	} else {
	                  		$("#data0-change").addClass('positive');
	                  	}
	                  }
	                  if(data2 != ''){
	                  	var start_value = data2[start_index][2];
	                  	var current_value = data2[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data1-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data1-change").removeClass('negetive');
						$("#data1-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data1-change").addClass('negetive');
	                  	} else {
	                  		$("#data1-change").addClass('positive');
	                  	}
	                  }
	                  if(data3 != ''){
	                  	var start_value = data3[start_index][2];
	                  	var current_value = data3[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data2-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data2-change").removeClass('negetive');
						$("#data2-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data2-change").addClass('negetive');
	                  	} else {
	                  		$("#data2-change").addClass('positive');
	                  	}
					  }
	                  if(data4 != ''){
	                  	var start_value = data4[start_index][2];
	                  	var current_value = data4[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data3-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data3-change").removeClass('negetive');
						$("#data3-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data3-change").addClass('negetive');
	                  	} else {
	                  		$("#data3-change").addClass('positive');
	                  	}
	                  }
	                  if(data5 != ''){
	                  	var start_value = data5[start_index][2];
	                  	var current_value = data5[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data4-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data4-change").removeClass('negetive');
						$("#data4-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data4-change").addClass('negetive');
	                  	} else {
	                  		$("#data4-change").addClass('positive');
	                  	}
	                  }
	                  if(data6 != ''){
	                  	var start_value = data6[start_index][2];
	                  	var current_value = data6[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data5-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data5-change").removeClass('negetive');
						$("#data5-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data5-change").addClass('negetive');
	                  	} else {
	                  		$("#data5-change").addClass('positive');
	                  	}
	                  }
	                  if(data6 != ''){
	                  	var start_value = data6[start_index][2];
	                  	var current_value = data6[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data5-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data5-change").removeClass('negetive');
						$("#data5-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data5-change").addClass('negetive');
	                  	} else {
	                  		$("#data5-change").addClass('positive');
	                  	}
	                  }
	                  if(data7 != ''){
	                  	var start_value = data7[start_index][2];
	                  	var current_value = data7[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data6-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data6-change").removeClass('negetive');
						$("#data6-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data6-change").addClass('negetive');
	                  	} else {
	                  		$("#data6-change").addClass('positive');
	                  	}
	                  }
	                  if(data8 != ''){
	                  	var start_value = data8[start_index][2];
	                  	var current_value = data8[row_index][2];
	                  	var change_percent = ((current_value - start_value)/start_value)*100;
	                  	$('#data7-change').text(change_percent.toFixed(2)+'%');
	                  	$("#data7-change").removeClass('negetive');
						$("#data7-change").removeClass('positive');
	                  	if(change_percent < 0) {
	                  		$("#data7-change").addClass('negetive');
	                  	} else {
	                  		$("#data7-change").addClass('positive');
	                  	}
					  }
					  if(data9 != ''){
							var start_value = data9[start_index][2];
							var current_value = data9[row_index][2];
							var change_percent = ((current_value - start_value)/start_value)*100;
							$('#data8-change').text(change_percent.toFixed(2)+'%');
							$("#data8-change").removeClass('negetive');
						$("#data8-change").removeClass('positive');
							if(change_percent < 0) {
								$("#data8-change").addClass('negetive');
							} else {
								$("#data8-change").addClass('positive');
							}
						}
						if(data10 != ''){
							var start_value = data10[start_index][2];
							var current_value = data10[row_index][2];
							var change_percent = ((current_value - start_value)/start_value)*100;
							$('#data9-change').text(change_percent.toFixed(2)+'%');
							$("#data9-change").removeClass('negetive');
						  $("#data9-change").removeClass('positive');
							if(change_percent < 0) {
								$("#data9-change").addClass('negetive');
							} else {
								$("#data9-change").addClass('positive');
							}
						}
						if(data11 != ''){
							var start_value = data11[start_index][2];
							var current_value = data11[row_index][2];
							var change_percent = ((current_value - start_value)/start_value)*100;
							$('#data10-change').text(change_percent.toFixed(2)+'%');
							$("#data10-change").removeClass('negetive');
						  $("#data10-change").removeClass('positive');
							if(change_percent < 0) {
								$("#data10-change").addClass('negetive');
							} else {
								$("#data10-change").addClass('positive');
							}
						}
						if(data12 != ''){
							var start_value = data12[start_index][2];
							var current_value = data12[row_index][2];
							var change_percent = ((current_value - start_value)/start_value)*100;
							$('#data11-change').text(change_percent.toFixed(2)+'%');
							$("#data11-change").removeClass('negetive');
						  $("#data11-change").removeClass('positive');
							if(change_percent < 0) {
								$("#data11-change").addClass('negetive');
							} else {
								$("#data11-change").addClass('positive');
							}
						}
	                } else {
	                  myDate = new Date(this.x);//console.log(this.series);console.log(this.series.userOptions);console.log(this.series.userOptions.name);
	                  $(diff_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear()+ ' ');
	                  $('#to_date').text(scheme_data[row_index][2]+' ');
	                }
	              },
	              mouseDown: function(){
	              	//console.log('into the click event function');
	              }
	            },    
	          },
	        }
	      },
	      series: [
			{
				name: 1,
				color:'#0091ea',
				data: data1
			}, {
				name: 2,
				color:'#BF71FF',
				data: data2
			},{
				name: 3,
				color: '#3EC963',
				data: data3
			}, {
				name: 4,
				color: '#FF697A',
				data: data4
			}, {
				name: 5,
				color: '#F88407',
				data: data5
			}, {
				name: 6,
				color: '#F80786',
				data: data6
			}, {
				name: 7,
				color: '#E3F807',
				data: data7
			}, {
				name: 8,
				color: '#FD2B2B',
				data: data8
			}, {
				name: 9,
				color: '#A3E4D7',
				data: data9
			}, {
				name: 10,
				color: '#8ea832',
				data: data10
			}, {
				name: 11,
				color: '#0B5345',
				data: data11
			}, {
				name: 12,
				color: '#512E5F',
				data: data12
			},

			],
	    });
	}

	function create_options(scheme_details) 
	{
		//var options_selected = $('#compare_data').html();
		var new_options = '';
		var colors = ['#0091ea','#BF71FF','#3EC963','#FF697A','#F88407','#F80786','#E3F807','#FD2B2B','#A3E4D7','#8ea832','#0B5345','#512E5F'];
		var count = 0;
		$('#compare_data').empty();
		var length = scheme_details.length;
		$.each(scheme_details, function(key, value){
			if (length == 1) {
				$('.fund-beater').css('display','inline');
				master_scheme_code = value.scheme_code;
				master_fund_type = value.fund_type;
			}else{
				$('.fund-beater').css('display','none');
				master_scheme_code = '';
				master_fund_type = '';
			}
			console.log('master_length'+length);
			if (value.current_per > 0) {
				$('#compare_data').append('<div class="card fund_compare_card" style="border-left: 4px solid '+colors[count]+';" data-id='+value.scheme_code+' id="'+value.scheme_code+'" data-fundType='+value.fund_type+'>'+ 
		        '<i class="close material-icons pull-right close_icon">close</i>'+
		        '<h4 class="fund_compare_name">'+value.scheme_name+'</h4>'+
		        '<h3 class="nav_value">'+value.current_value+'<span class="positive nav-spacing" id="data'+count+'-change">('+value.current_per+'%)</span></h3>'+
				'</div>');
			}else{
				$('#compare_data').append('<div class="card fund_compare_card" style="border-left: 4px solid '+colors[count]+';" data-id='+value.scheme_code+' id="'+value.scheme_code+'" data-fundType='+value.fund_type+'>'+ 
		        '<i class="close material-icons pull-right close_icon" data-id='+value.scheme_code+'>close</i>'+
		        '<h4 class="fund_compare_name">'+value.scheme_name+'</h4>'+
		        '<h3 class="nav_value">'+value.current_value+'<span class="negetive nav-spacing" id="data'+count+'-change">('+value.current_per+'%)</span></h3>'+
				'</div>');
			}
		count += 1;
		});
	}

	$('#graph-view-div').on('mousedown', function() {
	    // $('#compare-diff').html('');
	    range_stat = 1;
	    key = 0;
  	});

    $('#graph-view-div').on('mouseup', function() {
	    range_stat = 0;
	    key = '';
	    start_nav = '';
	    $count = 0;
	    $.each(scheme_details_info, function(key,value){
	    	console.log(value);
			$('#data'+$count+'-change').text(value['current_per'].toFixed(2)+'%');
			$('#data'+$count+'-change').removeClass('negetive');
			$('#data'+$count+'-change').removeClass('positive');
			if(value['current_per'] < 0) {
				$('#data'+$count+'-change').addClass('negetive');
			} else {
				$('#data'+$count+'-change').addClass('positive');
			}
			$count += 1;
	    });
	    // $('.compare_nav span').text('');
	    // $('.compare_nav').css('display','none');
	    // $('.period_change').css('display','inline');
	    // $('#compare-diff').html('');
   	});

   	EXPORT_WIDTH = 1000;

	function save_chart(chart) {
	    var render_width = EXPORT_WIDTH;
	    var render_height = render_width * chart.chartHeight / chart.chartWidth

	    // Get the cart's SVG code
	    var svg = chart.getSVG({
	        exporting: {
	            sourceWidth: chart.chartWidth,
	            sourceHeight: chart.chartHeight
	        }
	    });
	    svg = svg.replace('Highcharts.com','');
	    return svg.replace('fill="#111111"','fill="#FFFFFF"');
	}

	$('.edit_icon').on('click', function(){
		  $('.overlay').css('display','block')
		var chartSvg = save_chart($('#graph-view-div').highcharts());
		var jObject={};
		jObject[0] = JSON.stringify(scheme_details_info);
		jObject[1] = JSON.stringify(chartSvg);
		jObject[2] = JSON.stringify(timespace);
			$.ajax({
				type: "POST",
				url: "/exportPdf",
				data:{data: jObject[0],
					svg: jObject[1],
					timespace: jObject[2]},
				async:false,
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data) {
					  $('.overlay').css('display','none');
					  var win = window.open('/'+data.file, '_blank');
					  win.focus();
				},
				error: function(xhr, status, error) {
					$('.overlay').css('display','none');
					alert(error);
				}
			});

	});

	// $('.comp-catogory').on('change', function(){
	// 	$('#pick-date-id').text('5 Year');
	// 	timespace = '6';
	// 	var formData = 'cat='+$('.comp-catogory').val();
	// 	  $('.overlay').css('display','block');
	// 	$.ajax({
	// 		type:"POST",
	// 		url:"/topFunds",
	// 		data:formData,
	// 		headers: {
	// 			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 		},
	// 		success: function(data) {
	// 		  $('.overlay').css('display','none');
	// 		  	scheme_details_info = '';
	// 			scheme_details_info = data.scheme_details;
	// 			create_options(data.scheme_details);
	// 			create_graph('graph-view-div','#from_date',data.data);
	// 		},
	// 		error: function(xhr, status, error) {
	// 			  $('.overlay').css('display','none');
	// 			alert(error);
	// 		}
	// 	})
	// });

	$('.date-li').on('click',function(){
		$('#pick-date-id').text($(this).text());
		$('#pick-date-id').append('<span class="caret"></span>');
    	timespace = $(this).data('value');
    	var scheme_codes = '';
		var fund_type = '';
        if($('.fund_compare_card').length < 16) {
        	$('.fund_compare_card').each(function() {
	        		scheme_codes += $(this).data('id')+'|';
	        		fund_type += $(this).data('fundtype')+'|';
	        });
        }
        if (scheme_codes != '') {
	        get_scheme_info(scheme_codes, fund_type, timespace);
        }
	});

	$('.cat-li').on('click',function(){
		$('#classification-id').text($(this).text());
		$('#classification-id').append('<span class="caret"></span>');
		// $('#pick-date-id').text('5 Year');
		// $('#pick-date-id').append('<span class="caret"></span>');
		// timespace = '6';
		var formData = 'cat='+$(this).data('value')+'&dur='+timespace;
		  $('.overlay').css('display','block');
		$.ajax({
			type:"POST",
			url:"/topFunds",
			data:formData,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
			  $('.overlay').css('display','none');
			  	scheme_details_info = '';
				scheme_details_info = data.scheme_details;
				create_options(data.scheme_details);
				create_graph('graph-view-div','#from_date',data.data);
			},
			error: function(xhr, status, error) {
				  $('.overlay').css('display','none');
				alert(error);
			}
		})
	});

	$('.fund-beater').on('click',function(){
			var formData = 'scheme_code='+master_scheme_code+'&fund_type='+master_fund_type+'&timespace='+timespace;
			$.ajax({
				type:'POST',
				url:'/beatme',
				data:formData,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(data) {
				  	scheme_details_info = '';
					scheme_details_info = data.scheme_details;
					create_options(data.scheme_details);
					create_graph('graph-view-div','#from_date',data.data);
				},
				error: function(xhr,status, error) {

				}
			});

	});

});