$(document).ready(function(){
	$( document ).ajaxStart(function() {
	  $('.overlay').css('display','block');
	});
	$( document ).ajaxStop(function() {
	  $('.overlay').css('display','none');
	});
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	var graph_val = '';
	var data = '';
	var start_nav = '';
	var start_date;
	var range_stat = 0;
	var chart_id = 'global-chart';
	var date_id = '#change-date';
	var value_id = '#change-points';
	var diff_id = '#change-per';
	var indexData = 'DowJones';
	
	$('#detail-title').text($('#DowJones').data('title'));
	$('#detail-point').text($('#DowJones').data('points'));
	if ($('#DowJones').data('diff') > 0) {
		$('#detail-diff').text($('#DowJones').data('diff')).removeClass('negetive').addClass('positive');
		$('#detail-per').text("("+$('#DowJones').data('per')+"%)").removeClass('negetive').addClass('positive');
		$('#state-icon').text('arrow_drop_up').removeClass('negetive').addClass('positive');
	}else{
		$('#detail-diff').text($('#DowJones').data('diff')).removeClass('positive').addClass('negetive');
		$('#detail-per').text("("+$('#DowJones').data('per')+"%)").removeClass('positive').addClass('negetive');			
		$('#state-icon').text('arrow_drop_down').removeClass('positive').addClass('negetive');
	}

	selectedIndex(indexData,4);

	$('#distenc-state').on('change',function(){
		selectedIndex(indexData,$('#distenc-state').val());
	});

	$('#global-chart').on('mousedown', function() {
	  range_stat = 1;
	});

	$('#global-chart').on('mouseup', function() {
	  range_stat = 0;
	  start_nav = '';
	});

	$('#global-chart').on('mouseleave', function(){
		$('#change-points').text('');
		$('#change-date').text('');
		$('#change-per').text('');
	});

	$('.index-card').on('click',function(){
		indexData = $(this).data('value');
		$( ".active-card" ).removeClass( "active-card" );
		$(this).addClass("active-card");
			$('#detail-title').text($(this).data('title'));
			$('#detail-point').text($(this).data('points'));
		if ($(this).data('diff') > 0) {
			$('#detail-diff').text($(this).data('diff')).removeClass('negetive').addClass('positive');
			$('#detail-per').text("("+$(this).data('per')+"%)").removeClass('negetive').addClass('positive');
			$('#state-icon').text('arrow_drop_up').removeClass('negetive').addClass('positive');
		}else{
			$('#detail-diff').text($(this).data('diff')).removeClass('positive').addClass('negetive');
			$('#detail-per').text("("+$(this).data('per')+"%)").removeClass('positive').addClass('negetive');			
			$('#state-icon').text('arrow_drop_down').removeClass('positive').addClass('negetive');
		}
		selectedIndex(indexData,$('#distenc-state').val());
	});

	function selectedIndex(index, timespace){
		var formData = 'duration='+timespace+'&index='+index;
		$.ajax({
			type: "POST",
			url: "/selectedIndex",
			data:formData,
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function(data) {
				if (data.msg == 1) {
					graph_val = data.data;
					populate_data(graph_val);
				}else{
					alert('error!!!!!');
				}
			},
			error: function(xhr, status, error) {
				alert(error);
			}
		});
	}

	function populate_data(data) 
	{
	  Highcharts.stockChart(chart_id, {
		 xAxis: {
	            gapGridLineWidth: 0,
	            lineColor: '#232323',
	        },
	     yAxis: {
	            gridLineColor: '#232323'
	        },
	        rangeSelector: {
	            selected: 5,
	            enabled:false
	        },
		    credits: {
		        text: 'Righttracker',
		        href: 'http://www.righttracker.in'
		    },
	        chart: {
			      backgroundColor: chart_bg_color,
			      selectionMarkerFill: markerFillColor,
			                events: {
			              selection: function(event) {
			                  if (event.xAxis) {
			                      return false;
			                  }
			              }
			          },
			          zoomType: 'x'
			  },
			chart: {
			      backgroundColor: chart_bg_color,
			      selectionMarkerFill: markerFillColor,
			                events: {
			              selection: function(event) {
			                  if (event.xAxis) {
			                      return false;
			                  }
			              }
			          },
			          zoomType: 'x'
			  },
	        navigator: {
	            enabled: false
	        },
	        scrollbar: {
	            enabled: false
	        },
	        plotOptions: {
		      series: {
		        allowPointSelect: true,
		          marker: {
		              states: {
		                  select: {
		                      fillColor: 'red',
		                      lineWidth: 0
		                  }
		              }
		          },
		        point: {
		          events: {
		            mouseOver: function () {
		              if(range_stat == 1 && start_nav == '') {
		                var row_index = parseInt(this.index) - 1;
		                selected_nav = data[row_index];
		                start_date = new Date(data[row_index][0]);
		                start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
		              }
		              var current_index = parseFloat(this.y);
		              if(range_stat == 1 && start_nav != '' ) {
		                var diff = current_index - start_nav;
		                var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
		                myDate = new Date(this.x);
		                $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		                $(value_id).text(' '+current_index.toFixed(2)+' ');
		                $(diff_id).html(' ('+change+'%)');
		                $(diff_id).removeClass('positive');
		                $(diff_id).removeClass('negetive');
		                if(change < 0) {
		                  $(diff_id).addClass('negetive');
		                } else {
		                  $(diff_id).addClass('positive');
		                }
		              } else {
		                myDate = new Date(this.x);
		                $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		                $(value_id).text(' '+current_index.toFixed(2)+' ');
		                $(diff_id).html('');


		              }
		            }
		          },    
		        },
		      }
		    },
	        series: [{
	            name: 'EOD',
	            type: 'area',
	            data: data,
	            gapSize: 0,
	            color : graphColor,
	            tooltip: {
	                valueDecimals: 2,
	                enabled: false,
	            },
	            fillColor: {
	                linearGradient: {
	                    x1: 0,
	                    y1: 0,
	                    x2: 0,
	                    y2: 1
	                },
	                stops: [
	                    [0, 'rgba(248, 132, 7, 0.5)'],
	                    [1, 'rgba(248, 132, 7, 0.0)'],
	                ]
	            },
	            threshold: null
	        }]
	    });
	}
});