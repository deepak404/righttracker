
$(document).ready(function(){
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
	$( document ).ajaxStart(function() {
	  $('.overlay').css('display','block')
	});
		$( document ).ajaxStop(function() {
	  $('.overlay').css('display','none')
	});
	var scheme_code_val = 0;
	var fund_type_val = 0;

	$("#myBtn").click(function(){
		$('#myModal').modal('show');
	});

	$('#add-amc').on('click', function(){
		$('#amc-div').empty();
		$('#amc-div').append('<input type="text" name="amc_name" required id="amc_name">'+
              '<span class="bar"></span>'+
              '<label>AMC Name</label>');
	});

	$('#fund_type').on('change',function(){
		if ($('#fund_type option:selected').val() == 1) {
			$('#field_one').empty();
			$('#field_two').empty();
			$('#field_three').empty();
			$('#submit_btn').empty();
			$('#field_four').empty();
			$('#classification').empty();
			$('#classification').append('<option value="" disabled selected>Classification</option>'+
              '<option value="large cap">Large Cap</option>'+
              '<option value="mid cap">Mid Cap</option>'+
              '<option value="ELSS">ELSS</option>'+
              '<option value="sectoral">Sectoral</option>'+
              '<option value="diversified">Diversified Eq.</option>'+
              '<option value="arbitrage">Arbitrage</option>'+
              '<option value="small cap">Small Cap</option>');
			$('#field_one').append('<input type="text" name="small_cap" required id="small_cap">'+
              '<span class="bar"></span>'+
              '<label>% Small Cap</label>');
			$('#field_two').append('<input type="text" name="mid_cap" required id="mid_cap">'+
              '<span class="bar"></span>'+
              '<label>% Mid Cap</label>');
			$('#field_three').append('<input type="text" name="larg_cap" required id="larg_cap">'+
              '<span class="bar"></span>'+
              '<label>% Large Cap</label>');
			$('#submit_btn').append('<button type="submit" class="btn button_popup btn-primary" data-val="equity" id="add_scheme">Add Scheme</button>')
			$('#submit_btn').append('<span class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 composition">Total Composition:<span id="total">0%</span></span>')
		}else if ($('#fund_type option:selected').val() == 2) {
			$('#field_one').empty();
			$('#field_two').empty();
			$('#field_three').empty();
			$('#submit_btn').empty();
			$('#field_four').empty();
			$('#classification').empty();
			$('#classification').append('<option value="" disabled selected>Classification</option>'+
              '<option value="liquid">Liquid</option>'+
              '<option value="lp">LP</option>'+
              '<option value="fmp">FMP</option>'+
              '<option value="long term">Long term</option>'+
              '<option value="short term">Short term</option>'+
              '<option value="ultra short term">Ultra Short term</option>'+
              '<option value="floating rate">Floating rate</option>'+
              '<option value="credit opps">Credit Opps.</option>'+
              '<option value="mip">MIP</option>'+
							'<option value="gilt short term">Gilt Short term</option>'+
              '<option value="overnight">Over Night Fund</option>'+
              '<option value="gilt long term">Gilt Long term</option>');
			$('#field_one').append('<input type="text" name="avg_mat" required id="avg_mat">'+
              '<span class="bar"></span>'+
              '<label>Average Maturity</label>');
			$('#field_two').append('<input type="text" name="mod_due" required id="mod_due">'+
              '<span class="bar"></span>'+
              '<label>Modification Duration</label>');
			$('#field_three').append('<input type="text" name="gov_sec" required id="gov_sec">'+
              '<span class="bar"></span>'+
              '<label>% of Gov.Securities</label>');
			$('#field_four').append('<input type="text" name="aa_return" required id="aa_return">'+
              '<span class="bar"></span>'+
              '<label>% > AA Rate</label>');
			$('#submit_btn').append('<button type="submit" class="btn button_popup btn-primary" data-val="debt" id="add_scheme">Add Scheme</button>')
			$('#submit_btn').append('<span class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 composition">Total Composition:<span id="total">0%</span></span>')
		}else if ($('#fund_type option:selected').val() == 3) {
			$('#field_one').empty();
			$('#field_two').empty();
			$('#field_three').empty();
			$('#field_four').empty();
			$('#submit_btn').empty();
			$('#classification').empty();
			$('#classification').append('<option value="" disabled selected>Classification</option>'+
              '<option value="balanced">Balanced</option>'+
              '<option value="ess">ESS</option>');
			$('#field_one').append('<input type="text" name="small_cap" required id="small_cap">'+
              '<span class="bar"></span>'+
              '<label>% Small Cap</label>');
			$('#field_two').append('<input type="text" name="mid_cap" required id="mid_cap">'+
              '<span class="bar"></span>'+
              '<label>% Mid Cap</label>');
			$('#field_three').append('<input type="text" name="larg_cap" required id="larg_cap">'+
              '<span class="bar"></span>'+
              '<label>% Large Cap</label>');
			$('#field_four').append('<input type="text" name="debt" required id="debt">'+
              '<span class="bar"></span>'+
              '<label>% Debt</label>');
			$('#submit_btn').append('<button type="submit" class="btn button_popup btn-primary" data-val="bal" id="add_scheme">Add Scheme</button>')
			$('#submit_btn').append('<span class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 composition">Total Composition:<span id="total">0%</span></span>')
		}
	});

	$('#file_upload').on('change', function(){
		var filename = $('#file_upload').val().replace(/C:\\fakepath\\/i, '');
		$('.label-upload').text(filename);
	});

	$('#add-scheme-form').on('submit',function(e){
		e.preventDefault();

		var formData = new FormData();
		if ($('#amc_name').attr('type') == 'text') {
			formData.append('amc_name',$('#amc_name').val());
		}else{
			formData.append('amc_name',$('#amc_name option:selected').val());
		}
		var csv_file = $('#file_upload').get(0).files[0];
		formData.append('csv_file',csv_file);
		formData.append('fund_type',$('#fund_type option:selected').val());
		formData.append('classification',$('#classification option:selected').val());
		formData.append('asset_size',$('#asset_size').val());
		formData.append('exit_load',$('#exit_load').val());
		formData.append('scheme_name',$('#scheme_name').val());
		formData.append('scheme_code',$('#scheme_code').val());

		if ($('#add_scheme').data('val') == 'equity') {
			formData.append('small_cap',$('#small_cap').val());
			formData.append('mid_cap',$('#mid_cap').val());
			formData.append('larg_cap',$('#larg_cap').val());
		}else if ($('#add_scheme').data('val') == 'debt') {
			formData.append('aa_return',$('#aa_return').val());
			formData.append('avg_mat',$('#avg_mat').val());
			formData.append('mod_due',$('#mod_due').val());
			formData.append('gov_sec',$('#gov_sec').val());
		}else if ($('#add_scheme').data('val') == 'bal') {
			formData.append('small_cap',$('#small_cap').val());
			formData.append('mid_cap',$('#mid_cap').val());
			formData.append('larg_cap',$('#larg_cap').val());
			formData.append('debt',$('#debt').val());
		}

		$.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
            type: 'POST',
            url: '/add_scheme',
            data: formData,
            contentType: false,
            processData: false,
            success:function(data){
            	if (data.msg == 1) {
            		alert('Success !!!!');
            		location.reload();
            	}else if (data.msg == 2){
            		alert(data.info);
            	}else{
            		alert('Error Try Again!!!');
            	}
            },
            error:function(){

            }
        });
	});

	$('.select_state').on('click',function(){
		$('.list_radio_checked').removeClass('list_radio_checked list_radio_active').addClass('list_radio');
		$(this).removeClass('list_radio').addClass('list_radio_checked list_radio_active');
		fund_type_val = $(this).data('funds');
		scheme_code_val = $(this).data('schemes');
	});

	$('.delete_icon').on('click', function(){
		if (scheme_code_val != 0) {
			var data = "fund_type="+fund_type_val+"&scheme_code="+scheme_code_val;

	        $.ajax({
	            type: 'POST',
	            url: '/delete_scheme',
	            data: data,
	            success:function(data){
            	if (data.msg == 1) {
            		alert('Success !!!!');
            		location.reload();
            	}else{
            		alert('Error Try Again!!!');
            	}
	            },
	            error:function(){

	            }
	        });
		}else{
			alert('Select Scheme to make action!!!!!');
		}
	});

	$('.edit_icon').on('click',function(){
		var data = 'scheme_code='+scheme_code_val+'&fund_type='+fund_type_val;
        $.ajax({
            type: 'POST',
            url: '/get_scheme_info',
            data: data,
            success:function(data){
        	if (data.msg == 1) {
        		var schemeInfo = data.schemeInfo[0];
	        		$('#classification-edit').val(schemeInfo['classification']);
	        		$('#schemeName-edit').val(schemeInfo['scheme_name']);
	        		$('#asset-edit').val(schemeInfo['asset_size']);
	        		$('#exitLoad-edit').val(schemeInfo['exit_load']);
					$('#schemeCode-edit').val(schemeInfo['scheme_code']);
					$('#fundType-edit').val(schemeInfo['fund_type']);
        		if (schemeInfo['fund_type'] == 1) {
	        		$('#smallCap-edit').val(schemeInfo['small_cap']);
	        		$('#midCap-edit').val(schemeInfo['mid_cap']);
	        		$('#largeCap-edit').val(schemeInfo['large_cap']);
					$('#equityedit-modal').modal('show');
        		}else if (schemeInfo['fund_type'] == 2) {
        			$('#avgMat-edit').val(schemeInfo['avg_mat']);
        			$('#modDuration-edit').val(schemeInfo['mod_duration']);
        			$('#govSec-edit').val(schemeInfo['gov_sec']);
        			$('#AA-edit').val(schemeInfo['AA_return']);
        			$('#debtedit-modal').modal('show');
        		}else if (schemeInfo['fund_type'] == 3) {
	        		$('#smallCap-edit').val(schemeInfo['small_cap']);
	        		$('#midCap-edit').val(schemeInfo['mid_cap']);
	        		$('#largeCap-edit').val(schemeInfo['large_cap']);
	        		$('#debt-edit').val(schemeInfo['debt']);
        			$('#balancededit-modal').modal('show');
        		}
        		// location.reload();
        	}else{
        		alert('Error Try Again!!!');
        	}
            },
            error:function(){
                alert('error');
            }
        });
	});

	$('#scheme-edit').on('submit', function(e){
		e.preventDefault();
		var formData = $('#scheme-edit').serialize();
        $.ajax({
            type: 'POST',
            url: '/edit_scheme_info',
            data: formData,
            success:function(data){
	        	if (data.msg == 1) {
					$('#equityedit-modal').modal('hide');
        			$('#debtedit-modal').modal('hide');
        			$('#balancededit-modal').modal('hide');
					$('.sucess-msg').text('Scheme edited Sucessfully')
	        		$('#sucess-modal').modal('show');
	        	}else{
					$('.sucess-msg').text('Scheme edit Failed')
	        		$('#fail-modal').modal('show');
	        	}
            },
            error:function(){
                alert('error');
            }
        });
	});

	$('.alert-btn').on('click', function(){
		location.reload();
	});

	$(document).on('keyup','#small_cap,#mid_cap,#larg_cap,#gov_sec,#aa_return,#debt', function(){
		var sum = 0;
		if ($('#fund_type option:selected').val() == 1) {
			if ($('#small_cap').val() != '') {
				sum += parseFloat($('#small_cap').val())
			}
			if ($('#mid_cap').val() != '') {
				sum += parseFloat($('#mid_cap').val())
			}
			if ($('#larg_cap').val() != '') {
				sum += parseFloat($('#larg_cap').val());
			}
		}else if ($('#fund_type option:selected').val() == 2) {
			if ($('#aa_return').val() != '') {
				sum += parseFloat($('#aa_return').val());
			}
			if ($('#gov_sec').val() != '') {
				sum += parseFloat($('#gov_sec').val());
			}
		}else if ($('#fund_type option:selected').val() == 3) {
			if ($('#small_cap').val() != '') {
				sum += parseFloat($('#small_cap').val())
			}
			if ($('#mid_cap').val() != '') {
				sum += parseFloat($('#mid_cap').val())
			}
			if ($('#larg_cap').val() != '') {
				sum += parseFloat($('#larg_cap').val());
			}
			if ($('#debt').val() != '') {
				sum += parseFloat($('#debt').val());
			}
		}
		$('#total').text(sum+'%');
	});

});
