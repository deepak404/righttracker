$(document).ready(function(){ 
	var chart_bg_color = '#111111';
	var markerFillColor = 'rgba(247, 154, 53, 0.2)';
	var graphColor = '#F88407';
	$today = new Date();
	$yesterday = new Date($today);
	$yesterday.setDate($today.getDate() - 1); 
	$('#pe-date').datepicker({  maxDate: $yesterday });

	getGraph(3)
	checkTime()

	function checkTime() {
		var dt = new Date();
		var len = dt.getMinutes().toString().length;
		var min = 0;
		if (len == 1) {
			min = "0"+dt.getMinutes();
		}else{
			min = dt.getMinutes();
		}
		var time = dt.getHours()+"."+min;
		var today = dt.getDay();
		if (today != 0 && today != 6) {
			if (time > 9 && time < 15.30) {
				// in%3Bccx
				indexUpdate('in%3BNSX','nifty')
				indexUpdate('in%3BSEN','sensex')
				indexUpdate('in%3Bccx','midcap') // NSE Midcap
				indexUpdate('in%3Bcnxs','smallcap') //NSE Smallcap
				// indexUpdate('in%3Bbmx','midcap') //BSE Midcap
				// indexUpdate('in%3BBCX','smallcap') // BSE Smallcap
				setTimeout(checkTime, 5000);
			}else if (time < 16){
				indexUpdate('in%3BNSX','nifty')
				indexUpdate('in%3BSEN','sensex')
				indexUpdate('in%3Bcnxs','smallcap') //NSE Smallcap
				indexUpdate('in%3Bccx','midcap') // NSE Midcap
				// indexUpdate('in%3BBCX','smallcap')
				// indexUpdate('in%3Bbmx','midcap')
				console.log('timeout'+time);
			}
		}
	}


	function indexUpdate(index,name) {
		$.ajax({
			type: "GET",
			url: "https://priceapi.moneycontrol.com/pricefeed/notapplicable/inidicesindia/"+index,
			success: function(data) {
				// setTimeout(sensex, 5000);
				if (data != "") {
					var val = data.data;
					var change = Math.round(val.pricechange * 100) / 100;
					var per = Math.round(val.pricepercentchange * 100) / 100;
					console.log();
					$('#'+name+'-val').text(val.pricecurrent);
					$('#'+name+'-chg').text(' '+change+' '+'('+per+'%)');
					if (change > 0) {
						$('#'+name+'-chg').removeClass('negetive').addClass('positive');
					}else{
						$('#'+name+'-chg').removeClass('positive').addClass('negetive');
					}
				}
			},
			error: function(xhr, error, status) {
				console.log(error);
			}
		});
	}


	function populate_data(chart_id, data) 
	{
		Highcharts.stockChart(chart_id, {
		 xAxis: {
	            gapGridLineWidth: 0,
	            lineColor: '#232323',
	        },
	     yAxis: {
	            gridLineColor: '#232323'
	        },
		    credits: {
		        text: 'Righttracker',
		        href: 'http://www.righttracker.in',
		        enabled:false
		    },
	        rangeSelector: {
	            selected: 5,
	            enabled:false
	            
	        },
			chart: {
			      backgroundColor: chart_bg_color,
			      selectionMarkerFill: markerFillColor,
			                events: {
			              selection: function(event) {
			                  if (event.xAxis) {
			                      return false;
			                  }
			              }
			          },
		              enabled:false,
			          zoomType: 'x'
			    
			  },
	        navigator: {
	            enabled: false
	        },
	        scrollbar: {
	            enabled: false
	        },
	        plotOptions: {
		      series: {
		        allowPointSelect: true,
		          marker: {
		              states: {
		                  select: {
		                      fillColor: 'red',
		                      lineWidth: 0
		                  }
		              }
		          },
		        point: {
		          events: {
		            mouseOver: function () {
		            //   if(range_stat == 1 && start_nav == '') {
		            //     var row_index = parseInt(this.index) - 1;
		            //     selected_nav = data[row_index];
		            //     start_date = new Date(data[row_index][0]);
		            //     start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
		            //   }
		            //   var current_index = parseFloat(this.y);
		            //   if(range_stat == 1 && start_nav != '' ) {
		            //     var diff = current_index - start_nav;
		            //     var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
		            //     myDate = new Date(this.x);
		            //     $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		            //     $(value_id).text(' '+current_index.toFixed(2)+' ');
		            //     $(diff_id).html(' ('+change+'%)');
		            //     $(diff_id).removeClass('positive');
		            //     $(diff_id).removeClass('negetive');
		            //     if(change < 0) {
		            //       $(diff_id).addClass('negetive');
		            //     } else {
		            //       $(diff_id).addClass('positive');
		            //     }
		            //   } else {
		            //     myDate = new Date(this.x);
		            //     $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
		            //     $(value_id).text(' '+current_index.toFixed(2)+' ');
		            //     $(diff_id).html('');


		            //   }
		            }
		          },    
		        },
		      }
		    },
	        series: [{
	            name: 'EOD',
	            type: 'line',
	            data: data,
	            gapSize: 0,
	            color : graphColor,
	            tooltip: {
	                valueDecimals: 2,
	                enabled: false,
	            },
	            fillColor: {
	                linearGradient: {
	                    x1: 0,
	                    y1: 0,
	                    x2: 0,
	                    y2: 1
	                },
	                stops: [
	                    [0, 'rgba(248, 132, 7, 0.5)'],
	                    [1, 'rgba(248, 132, 7, 0.0)'],

	                ]
	            },
	            threshold: null
	        }]
	    });
	}

	function getGraph(duration) {
		$.ajax({
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			url: "/tvgraph",
			data: 'duration='+duration,
			success:function(data){
				if (data.msg == 1) {
					var per = data.per;
					var chartData = data.data;
					var chartPe = data.pedata;
					$.each(chartData,function(key,value){
						populate_data(key,value)
					});
					$.each(chartPe,function(key,value){
						if (value.length != 0) {
							populate_data('pe-'+key,value)
							$('#'+key+'-pe').text(value[value.length - 1][1]);
						}else{

						}
					});
				}
			},
			error:function(error){
				console.log(error);
			} 
		});
	}

	$('.date-li').on('click',function(){
		$('#pick-date-id').text($(this).text());
		$('#pick-date-id').append('<span class="caret"></span>');
    	timespace = $(this).data('value');
    	getGraph(timespace)
	});

	$('#update-pe').on('click', function(){
		console.log('hello');
		$('#pemodal').modal('show');
	})

	$('#policy-update-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#policy-update-form').serialize();
		$.ajax({
			headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type:'POST',
			url:'/addindexpe',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					alert(data.response);
					location.reload(true);
				}
			},
			error:function(error){
				console.log(error);
			}
		})
	});

	$("#myiframe").contents().find(".header").remove()

});