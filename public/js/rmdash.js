$(document).ready(function(){ 
	
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	$('#addAmc').on('click',function(){
		$.ajax({
            type: 'GET',
            url: '/get_rm_amc',
            success:function(data){
            	$('#amc_name_id').empty();
        		$('#amc_name_id').append('<option value="" selected disabled>AMC Name</option>');
            	$.each(data.amc, function(key,value){
            		$('#amc_name_id').append('<option value="'+value+'">'+value+'</option>');
            	});
				$('#addAmcModal').modal('show');
            },
            error:function(){ 
                
            }
        });

	});

	$('#addAmcForm').on('submit', function(e){
		e.preventDefault();
		var formData = $('#addAmcForm').serialize();
		$.ajax({
            type: 'POST',
            url: '/add_rmuser',
            data: formData,
            success:function(data){
            	if (data.msg == 1) {
            		location.reload();
            	}
            },
            error:function(){ 
                
            }
        });
	});

	$('.change-pass-li').on('click',function(){
		$('#pass_id').val($(this).data('id'));
		$('#pass-state').text('');
		$('#changePassModal').modal('show');
	});

	$('.delete-li').on('click',function(){
		var user_id = 'user_id='+$(this).data('id');
		$.ajax({
			type: 'POST',
			url: '/delete_rmuser',
			data: user_id,
			success:function(data){
				location.reload();
			},
			error:function(error){
				console.log(error);
			}
		});
	});

	$('#c_password').on('keyup',function(){
		if ($('#c_password').val() == '') {
			$('#pass-state').text('');
		}else{
			if ($('#password').val() == $('#c_password').val()) {
				$('#pass-state').text('pass mached');
			}else{
				$('#pass-state').text('pass not mached');
			}
		}
	});

	$('#changePassForm').on('submit',function(e){
		e.preventDefault();
		var data = $('#changePassForm').serialize();
		$.ajax({
			type:'POST',
			url:'/change_rmuser_pass',
			data: data,
			success:function(data){
				if (data.msg == 1) {
					location.reload();
				}
			},
			error:function(error){
				console.log(error);
			}
		});

	});

	$('.edit-user-li').on('click',function(){
		console.log($(this).data('id'));
		var formData = 'user_id='+$(this).data('id');
		$.ajax({
			type:'POST',
			url:'/get_rm_info',
			data:formData,
			success:function(data){
				// location.reload();
				var myData = data.info;
				$('#edit_id').val(myData.user_id);
				$('#edit_name').val(myData.rm_name);
				$('#edit_mail').val(myData.mail_id);
				$('#edit_mobile').val(myData.mobile);
				$('#editAmc').modal('show');
			},
			error:function(error){
				console.log(error);
			}
		})
	});

	$('#editAmcForm').on('submit',function(e){
		e.preventDefault();
		var formData = $('#editAmcForm').serialize();
		$.ajax({
			type:'POST',
			url:'/update_user',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					location.reload();
				}
			},
			error:function(error){
				console.log(error);
			}
		});

	});
});