<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title id="titletext">RightTracker</title>

	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{url('css/index.css')}}" rel="stylesheet">
		<link href="{{url('css/schemes.css')}}" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/benchmark.css')}}" rel="stylesheet">
	</head>

<body class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>

                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>

              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li>
        </ul>
          <li><a href="/benchmark" class="active_navbar_header">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container schemes-padding ">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 benchmar_padding ">
    <div>
      <h2 class="headtxt"> Sensex  &nbsp
        <span id="sensex-span" class="nav_value">{{$sensex['points']}}&nbsp</span>
        <?php if ($sensex['Diff'] > 0): ?>
          <span class="positive" id="sensex-current">{{$sensex['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="sensex-current">{{$sensex['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($sensex['Per'] > 0): ?>
          <span id="sensex-per" class="positive">({{$sensex['Per']}}%)</span>
          <i id="sensex-icon" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="sensex-per" class="negetive">({{$sensex['Per']}}%)</span>
          <i id="sensex-icon" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>

      </h2>
      <h4 >
        <span id="sensex-date" class="benchmark_date"></span>
        <span>| &nbsp</span>
          <span id="sensex-rate" class="benchmark_rate"></span>
          <span id="sensex-diff" class="positive"></span>
          <button class=" dropdown-toggle pull-right graph_period " type="button" id="sensex-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            1 Day
            <span class="caret"></span>
          </button>
          <ul class="dropdown_period  dropdown-menu pull-right " aria-labelledby="dropdownMenu1">
            <li class="sensex-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="sensex-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="sensex-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="sensex-li" data-value="4"><a href="#">3 Months</a></li>
						<li class="sensex-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="sensex-li" data-value="8"><a href="#">2 Year</a></li>
            <li class="sensex-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="sensex-li" data-value="7"><a href="#">Max</a></li>
          </ul>
        </span>
      </h4>
    </div>
    <div class="card benchmark_card" id="sensex-chart">
      <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
    </div>
  </div>

  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 benchmar_padding ">
    <div>
      <h2 class="headtxt">Nifty   &nbsp
        <span id="nifty-span" class="nav_value">{{$nifty['points']}}&nbsp</span>
        <?php if ($nifty['Diff'] > 0): ?>
          <span class="positive" id="nifty-current">{{$nifty['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="nifty-current">{{$nifty['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($nifty['Per'] > 0): ?>
          <span id="nifty-per" class="positive">({{$nifty['Per']}}%)</span>
          <i id="nifty-icon" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="nifty-per" class="negetive">({{$nifty['Per']}}%)</span>
          <i id="nifty-icon" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
      </h2>
      <h4 >
        <span id="nifty-date" class="benchmark_date"></span>
        <span>| &nbsp</span>
          <span id="nifty-rate" class="benchmark_rate"></span>
          <span id="nifty-diff" class="positive"></span>
        <span>
          <button class=" dropdown-toggle pull-right graph_period " type="button" id="nifty-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          1 Day
          <span class="caret"></span>
          </button>
          <ul class=" dropdown_period  dropdown-menu pull-right dropdown_period  " aria-labelledby="dropdownMenu1">
            <li class="nifty-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="nifty-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="nifty-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="nifty-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="nifty-li" data-value="5"><a href="#">1 Year</a></li>
						<li class="nifty-li" data-value="8"><a href="#">2 Year</a></li>
            <li class="nifty-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="nifty-li" data-value="7"><a href="#">Max</a></li>
          </ul>
        </span>
      </h4>
    </div>
    <div class="card benchmark_card" id="nifty-chart">

    </div>
  </div>


  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 benchmar_padding">
    <div>
      <h2 class="headtxt">BSE Midcap  &nbsp
        <span id="midcap-span" class="nav_value">{{$midcap['points']}}&nbsp</span>
        <?php if ($midcap['Diff'] > 0): ?>
          <span class="positive" id="midcap-current">{{$midcap['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="midcap-current">{{$midcap['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($midcap['Per'] > 0): ?>
          <span id="midcap-per" class="positive">({{$midcap['Per']}}%)</span>
          <i id="midcap-icon" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="midcap-per" class="negetive">({{$midcap['Per']}}%)</span>
          <i id="midcap-icon" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>


      </span>
    </h2>
      <h4 >
        <span id="midcap-date" class="benchmark_date"></span>
        <span>| &nbsp</span>
          <span id="midcap-rate" class="benchmark_rate"></span>
          <span id="midcap-diff" class="positive"></span>
          <button class=" dropdown-toggle pull-right graph_period " type="button" id="midcap-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            1 Day
            <span class="caret"></span>
          </button>
          <ul class="dropdown_period  dropdown-menu pull-right dropdown_period " aria-labelledby="dropdownMenu1">
            <li class="midcap-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="midcap-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="midcap-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="midcap-li" data-value="4"><a href="#">3 Months</a></li>
						<li class="midcap-li" data-value="8"><a href="#">2 Year</a></li>
            <li class="midcap-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="midcap-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="midcap-li" data-value="7"><a href="#">Max</a></li>
          </ul>
        </span>
      </h4>
    </div>
    <div class="card benchmark_card" id="midcap-chart">

    </div>
  </div>


  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 benchmar_padding  ">
    <div>
      <h2 class="headtxt">BSE SmallCap  &nbsp
        <span id="smallcap-span" class="nav_value">{{$smallcap['points']}}&nbsp</span>
        <?php if ($smallcap['Diff'] > 0): ?>
          <span class="positive" id="smallcap-current">{{$smallcap['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="smallcap-current">{{$smallcap['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($smallcap['Per'] > 0): ?>
          <span id="smallcap-per" class="positive">({{$smallcap['Per']}}%)</span>
          <i id="smallcap-icon" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="smallcap-per" class="negetive">({{$smallcap['Per']}}%)</span>
          <i id="smallcap-icon" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>

        </span>
      </h2>
      <h4 >
        <span id="smallcap-date" class="benchmark_date"></span>
        <span>| &nbsp</span>
          <span id="smallcap-rate" class="benchmark_rate"></span>
          <span id="smallcap-diff" class="positive"></span>
          <button class=" dropdown-toggle pull-right graph_period " type="button" id="smallcap-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                1 Day
            <span class="caret"></span>
          </button>
          <ul class=" dropdown_period  dropdown-menu pull-right dropdown_period " aria-labelledby="dropdownMenu1">
            <li class="smallcap-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="smallcap-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="smallcap-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="smallcap-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="smallcap-li" data-value="5"><a href="#">1 Year</a></li>
						<li class="smallcap-li" data-value="8"><a href="#">2 Year</a></li>
            <li class="smallcap-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="smallcap-li" data-value="7"><a href="#">Max</a></li>
          </ul>
        </span>
      </h4>
    </div>
    <div class="card benchmark_card" id="smallcap-chart">

    </div>
  </div>
</div>
<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
<script type="text/javascript" src="{{url('js/benchmark.js')}}"></script>
<script type="text/javascript" src = "{{url('js/highstock.js')}}"></script>

</body>
</html>
