<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>RightTracker</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script   src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/globalindex.css">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" type="text/css" href="css/policytracking.css">
    <link href="css/schemes-popup.css" rel="stylesheet">
    <link href="css/schemes-debt.css" rel="stylesheet">
    <link href="css/schemes.css" rel="stylesheet">
</head>

<body class="body">
      <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" class="active_navbar_header">Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
    <div class=" container  schemes-padding padding-lr-zero">
        <div class="row">
            <h2 class=" headtxt ">Policy Tracking</h2>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 index-spacing padding-lr-zero">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 side-bar  padding-l-zero" id="scroll-customize">
                    <div class="index-card card  card-custom active-card" data-title="Wholesale Price Index" data-date="{{$wpiDate}}" data-price="{{$wpiPrice}}" data-policy="wpi" id='wpi-div'>
                        <div class="points-div">
                            <h4 class="policy-head">Wholesale Price Index</h4>
                            <i class="material-icons fileupload-icon add-policy">control_point</i>
                            <form class="file-upload">
                                <input type="file" id="wpi" />
                                <label for="wpi" class="upload-label">
                                    <i class="material-icons fileupload-icon">file_upload</i>
                                </label>
                            </form>
                            <span class="policy-date" id="">{{$wpiDate}}</span>
                            <span class="policy-percent  positive" id="">{{$wpiPrice}}%</span>
                            <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                        </div>
                    </div>
                    <div class="index-card card  card-custom" data-title="Consumer price index" data-date="{{$cpiDate}}" data-price="{{$cpiPrice}}" data-policy="cpi">
                        <div class="points-div">
                            <h4 class="policy-head">Consumer price index</h4>
                            <i class="material-icons fileupload-icon add-policy">control_point</i>
                            <form class="file-upload">
                                <input type="file" id="cpi" />
                                <label for="cpi" class="upload-label">
                                    <i class="material-icons fileupload-icon">file_upload</i>
                                </label>
                            </form>
                            <span class="policy-date" id="">{{$cpiDate}}</span>
                            <span class="policy-percent  positive" id="">{{$cpiPrice}}%</span>
                            <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                        </div>
                    </div>
                    <div class="index-card card  card-custom" data-title="Statutory liquidity ratio" data-date="{{$slrDate}}" data-price="{{$slrPrice}}" data-policy="slr">
                        <div class="points-div">
                            <h4 class="policy-head">Statutory liquidity ratio </h4>
                            <i class="material-icons fileupload-icon add-policy">control_point</i>
                            <form class="file-upload">
                                <input type="file" id="slr" />
                                <label for="slr" class="upload-label">
                                    <i class="material-icons fileupload-icon">file_upload</i>
                                </label>
                            </form>
                            <span class="policy-date" id="">{{$slrDate}}</span>
                            <span class="policy-percent  positive" id="">{{$slrPrice}}%</span>
                            <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                        </div>
                    </div>
                    <div class="index-card card  card-custom" data-title="Repo Rate" data-date="{{$rrDate}}" data-price="{{$rrPrice}}" data-policy="rr">
                        <div class="points-div">
                            <h4 class="policy-head">Repo Rate</h4>
                            <i class="material-icons fileupload-icon add-policy">control_point</i>
                            <form class="file-upload">
                                <input type="file" id="repo-rate" />
                                <label for="repo-rate" class="upload-label">
                                    <i class="material-icons fileupload-icon">file_upload</i>
                                </label>
                            </form>
                            <span class="policy-date" id="">{{$rrDate}}</span>
                            <span class="policy-percent  positive" id="">{{$rrPrice}}%</span>
                            <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                        </div>
                    </div>
                    <div class="index-card card  card-custom" data-title="10 Years Bond rate" data-date="{{$tenDate}}" data-price="{{$tenPrice}}" data-policy="ten">
                        <div class="points-div">
                            <h4 class="policy-head">10 Years Bond rate</h4>
                            <i class="material-icons fileupload-icon add-policy">control_point</i>
                            <form class="file-upload">
                                <input type="file" id="ten-bond" />
                                <label for="ten-bond" class="upload-label">
                                    <i class="material-icons fileupload-icon">file_upload</i>
                                </label>
                            </form>
                            <span class="policy-date" id="">{{$tenDate}}</span>
                            <span class="policy-percent  positive" id="">{{$tenPrice}}%</span>
                            <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                        </div>
                    </div>
                    <div class="index-card card  card-custom" data-title="Gross domestic product" data-date="{{$gdpDate}}" data-price="{{$gdpPrice}}" data-policy="gdp">
                        <div class="points-div">
                            <h4 class="policy-head">Gross domestic product</h4>
                            <i class="material-icons fileupload-icon add-policy">control_point</i>
                            <form class="file-upload">
                                <input type="file" id="gdp" />
                                <label for="gdp" class="upload-label">
                                    <i class="material-icons fileupload-icon">file_upload</i>
                                </label>
                            </form>
                            <span class="policy-date" id="">{{$gdpDate}}</span>
                            <span class="policy-percent  positive" id="">{{$gdpPrice}}%</span>
                            <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                        </div>
                    </div>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 ">
                    <div class="points-div ">
                        <h4 class="policyhead-graph" id="selected-title">-</h4>
                        <span class="policyindex-date" id="selected-date">-</span>
                        <br>
                        <span class="policyindex-percentage positive" id="selected-val">-%</span>
                        <!-- <i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i> -->
                    </div>
                    <div class="pointssel-div">
                        <span id="date-span"></span> |
                        <span class="positive" id="value-span"></span>
                    </div>
                    <select class="globalgraph-years pull-right">
                        <!-- <option value="1">1 month</option> -->
                        <option value="2" selected>3 month</option>
                        <option value="3">1 Year</option>
                        <option value="4">5 Years</option>
                        <option value="5">max</option>
                    </select>
                    <div class="card graph_card" id="graph-div">
                        <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
<!-- policy add model -->
    <div class="modal fade" id="ptmodal" role="dialog">
      <div class="modal-dialog" id="modal-dialog">
        <div class="modal-content col-lg-12">
          <div class="modal-header col-lg-12">
            <h4 class="modal-title" id="ptmodal-title">Title for policy tracking</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
          </div>
          <div class="modal-body col-lg-12">
            <form  id="policy-update-form">
                <input type="hidden" name="policy" value="" id="ptmodal-policy">
              <div class="group  col-md-12  col-lg-12 padding-lr-zero">      
                <input type="text" name="date" required>
                <span class="highlight"></span> 
                <span class="bar"></span>
                <label class="modal-label">Date (YYYY/MM/DD)</label>
              </div>
              <div class="group  col-md-12  col-lg-12 padding-lr-zero">      
                <input type="text" name="price" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="modal-label">Price</label>
              </div>
              <div class="col-lg-12 padding-lr-zero">
                <button type="submit" class="btn button_popup btn-primary ">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript" src="js/policy.js"></script>
<script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>
</body>

</html>