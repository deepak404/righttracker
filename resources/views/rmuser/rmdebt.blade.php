<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>RightTracker</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>
    
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/rmglobal.css">
	<link rel="stylesheet" href="css/rmequity.css">
</head>
<body>
	<!-- Nav Bar For RM User -->
	<nav class="nav navbar-default navebar-fixed-top">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i class="material-icons">menu</i></button>
			<a href="#">
				<img src="img/logo.svg" alt="RightTracker">
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a  href="/rm">Equity</a></li>
				<li><a class="active-menu" href="/rm_debt">Debt</a></li>
				<li><a href="/rm_hybrid">Hybrid</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="info-li">
					<span class="user-name">Mr.{{$rminfo['rm_name']}}</span>
					<span>{{$rminfo['amc_name']}}</span>
				</li>
				<li class="chart-li">
					<!--Pie Chart -->
					<div class="progress-pie-chart" data-percent="{{$rminfo['rm_progress']}}">
					    <div class="ppc-progress">
					        <div class="ppc-progress-fill">
					        	
					        </div>
					    </div>
					    <div class="ppc-percents">
					    </div>
					</div>
				</li>
				<li class="info-li">
					<span class="user-name">Completed {{$rminfo['rm_progress']}}%</span>
					<span>Last Update: {{date('d-m-Y',strtotime($rminfo['updated_at']))}}</span>
				</li>
				<li>
					<a href="/logout">
						Logout<i class="material-icons logout-icon">exit_to_app</i>
					</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- Main Container -->
	<div class="container plr-0">
		<div class="col-lg-12 plr-0">
			<div class="row">
				<div class="col-lg-4">
					<ul class="scheme-ul plr-0">
						@foreach($debt as $data)
							<?php if ($debt[0] == $data): ?>
								<li class="active-scheme scheme_list" data-schemecode="{{$data['scheme_code']}}" data-avg="{{$data['avg_mat']}}" data-aa="{{$data['AA_return']}}" data-mod="{{$data['mod_duration']}}" data-gov="{{$data['gov_sec']}}" data-exit="{{$data['exit_load']}}" data-asset="{{$data['asset_size']}}" data-brk="{{$data['brk']}}" data-schemename="{{$data['scheme_name']}}">{{$data['scheme_name']}}
								<?php if ($data['state'] == 1): ?>
									<i class="material-icons pull-right check-icon">check_circle</i>
								<?php endif ?>		
								</li>
							<?php else: ?>
								<li class="scheme_list" data-schemecode="{{$data['scheme_code']}}" data-avg="{{$data['avg_mat']}}" data-aa="{{$data['AA_return']}}" data-mod="{{$data['mod_duration']}}" data-gov="{{$data['gov_sec']}}" data-exit="{{$data['exit_load']}}" data-asset="{{$data['asset_size']}}" data-brk="{{$data['brk']}}" data-schemename="{{$data['scheme_name']}}">{{$data['scheme_name']}}
								<?php if ($data['state'] == 1): ?>
									<i class="material-icons pull-right check-icon">check_circle</i>
								<?php endif ?>	
								</li>
							<?php endif ?>
						@endforeach
					</ul>
				</div>
				<div class="col-lg-8">
					<h3 id="scheme-name">Name of scheme</h3>
					<form class="col-lg-12 group" id="debt-form">
						<input type="hidden" value="" name="scheme_code" id="scheme_code">
						<div class="col-lg-12 mb-20">
							<div class="col-lg-6">
							</div>
							<div class="col-lg-6">
								<span class="form-span">Exit Load</span>
							</div>
						</div>
						<div class="col-lg-6 mb-20">
							<input type="number" step="any" name="asset" id="asset" required>
							<span class="bar"></span>
							<label>Asset Size</label>
						</div>
						<div class="col-lg-6 mb-20">
							<div class="col-lg-6 pl-0">
								<input type="number" step="any" name="no_year" id="no_year" class="w-100" required>
								<span class="bar w-100"></span>
								<label>No of Year</label>
							</div>
							<div class="col-lg-6 pl-0">
	   							<input type="text" name="percentage" id="percentage" class="w-100" required>
								<span class="bar w-100"></span>
								<label>Percentage</label>
							</div>
						</div>
						<div class="col-lg-6 mb-20">
							<input type="number" step="any" name="avg_mat" id="avg_mat" required>
							<span class="bar"></span>
							<label>Average Maturity</label>
						</div>
						<div class="col-lg-6 mb-20">
							<input type="number" step="any" name="mod_dur" id="mod_dur" required>
							<span class="bar"></span>
							<label>Modification Duration</label>
						</div>
						<div class="col-lg-6 mb-20">
							<input class="class-split" type="number" step="any" name="gov_sec" id="gov_sec" required>
							<span class="bar"></span>
							<label>% of Gov.Securities</label>
						</div>
						<div class="col-lg-6 mb-20">
							<input class="class-split" type="number" step="any" name="aa_rate" id="aa_rate" required>
							<span class="bar"></span>
							<label>% > AA Rate</label>
						</div>
						<div class="col-lg-6 mb-20">
							<input class="class-split" type="text" name="brk" id="brk" required>
							<span class="bar"></span>
							<label>Brokerage</label>
						</div>
						<div class="col-lg-11 mb-20 total-div">
							<p class="total-p">Total: <span class="total-val">100%</span></p>
						</div>
						<div class="col-lg-12">
							<button type="submit" class="btn button_popup btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
	<script src="js/chart.js"></script>
	<script src="js/rmdebt.js?V1.0"></script>
	</body>
</html>