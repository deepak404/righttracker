<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title id="titletext">RightTracker</title>
		
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{url('css/index.css')}}" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/schemes.css')}}" rel="stylesheet">
    <link href="{{url('css/benchmark.css')}}" rel="stylesheet">
    <link href="{{url('css/comparison.css')}}" rel="stylesheet">

	</head>

<body class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil" class="active_navbar_header">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container schemes-padding">
    <div class="col-lg-12">
      <div class="col-xs-12 col-lg-3">
        <button class=" dropdown-toggle  pull-right" type="button" id="category" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          Select Category
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="pick-date-id">
            <li class="cat-li" data-value="1"><a href="#">Credit Indices</a></li>
            <li class="cat-li" data-value="2"><a href="#">Composite Indices</a></li>
            <li class="cat-li" data-value="3"><a href="#">Hybrid Indices</a></li>
            <li class="cat-li" data-value="4"><a href="#">GILT Indices</a></li>
            <li class="cat-li" data-value="5"><a href="#">Money Market Indices</a></li>
            <li class="cat-li" data-value="6"><a href="#">Commodity Indices</a></li>
            <li class="cat-li" data-value="7"><a href="#">Dollar Indices</a></li>
            <li class="cat-li" data-value="8"><a href="#">SDL Indices</a></li>
            <li class="cat-li" data-value="9"><a href="#">FPI Indices</a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-lg-3">
        <button class=" dropdown-toggle  pull-right" type="button" id="sub-category" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Select SubCategory
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" id="sub-ul" aria-labelledby="pick-date-id">
            <!-- <li class="subcat-li" data-value="1"><a href="#">5 Days</a></li> -->
        </ul>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 benchmar_padding padding-lr-zero">
      <h2 class="headtxt">Crisil Graph  </h2>
      <h4 class="seperator "></h4> <span id="from_date" class="compare_from ">DD-MM-YYYY</span>
      <span id="to_date" class="compare_to "></span> <span class="compare_to" id="val"></span>  
        
        <button class=" dropdown-toggle  pull-right" type="button" id="pick-date-id" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          3 Months
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="pick-date-id">
            <li class="date-li" data-value="1"><a href="#">5 Days</a></li>
            <li class="date-li" data-value="2"><a href="#">1 Month</a></li>
            <li class="date-li" data-value="3"><a href="#">3 Months</a></li>
            <li class="date-li" data-value="4"><a href="#">1 Year</a></li>
            <li class="date-li" data-value="5"><a href="#">3 Year</a></li>
            <li class="date-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="date-li" data-value="7"><a href="#">Max</a></li>
        </ul>
        <h4>Returns: <span id="rr-span" class="positive">%</span></h4>
      <div class="card graph_card" id="graph-view-div">

      </div>
    </div>
</div>
<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
<script>
  $(document).ready(function(){ 
    var data = {!! json_encode($value) !!};
    $('.cat-li').on('click',function(){
        $('#category').text($(this).text());
        if ($(this).data('value') == '1') {
          var value = data['Credit Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });
        }else if ($(this).data('value') == '2') {
          var value = data['Composite Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });
        }else if ($(this).data('value') == '3') {
          var value = data['Hybrid Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }else if ($(this).data('value') == '4') {
          var value = data['GILT Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }else if ($(this).data('value') == '5') {
          var value = data['Money Market Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }else if ($(this).data('value') == '6') {
          var value = data['Commodity Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }else if ($(this).data('value') == '7') {
          var value = data['Dollar Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }else if ($(this).data('value') == '8') {
          var value = data['SDL Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }else if ($(this).data('value') == '9') {
          var value = data['FPI Indices'];
          $('#sub-ul').empty();
          $.each( value, function( key, data ) {
            $('#sub-ul').append('<li class="subcat-li" data-value="'+data.tablename+'"><a href="#">'+data.indexname+'</a></li>');
          });

          
        }
    }); 
  });
</script>
<script type="text/javascript" src = "{{url('js/highstock.js')}}"></script>
<script type="text/javascript" src = "{{url('js/crisil.js')}}"></script>

</body>
</html>