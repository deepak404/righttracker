<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Tracker</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	    <!-- Optional theme -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	    <link rel="stylesheet" type="text/css" href="css/export.css">

	</head>

	<body class="body">
		<div class="container">
		  	<div class="row">
		  		<div class="col-lg-12">
			   		<h3 class=" title">Mutual Fund Scheme Comparison as on <span>27-Nov-17</span></h3>
			   		<hr>
			   	</div>
		  	</div>
		</div>
		<!-- Scheme Data Comparison -->
		<div class="container">
		  	<div class="row">
				<h4 class="title-spacing">Scheme Data Comparison </h4>

				<!-- scheme div -->

				<div class=" scheme-dataspacing">
					<div class=" scheme-spacing  ">
						<h5 class="scheme-name scheme-border">Franklin India Smaller Companies Fund - Growth</h5>
					</div>
					<div class=" current-nav ">
						<p class="nav">Current NAV </p>
						<span class="value">60.5</span>
					</div>
					<div class="returns ">
						<p class="nav "> Returns (<span> 5years </span> )</p>
						<span class="value positive">272.01%</span>
					</div>
				</div>
				
				<!-- div ends -->

				<div class="col-md-12 col-lg-12 scheme-dataspacing">
					<div class=" scheme-spacing  ">
						<h5 class="scheme-name scheme-border">Franklin India Smaller Companies Fund - Growth  </h5>
					</div>
					<div class=" current-nav ">
						<p class="nav">Current NAV </p>
						<span class="value">60.5</span>
					</div>
					<div class="  returns">
						<p class="nav "> Returns (<span> 5years </span> )</p>
						<span class="value positive"><strong>272.01%</strong></span>
					</div>
				</div>

				<div class=" scheme-dataspacing">
					<div class=" scheme-spacing  ">
						<h5 class="scheme-name scheme-border">Franklin India Smaller Companies Fund - Growth</h5>
					</div>
					<div class=" current-nav ">
						<p class="nav">Current NAV </p>
						<span class="value">60.5</span>
					</div>
					<div class=" returns">
						<p class="nav "> Returns (<span> 5years </span> )</p>
						<span class="value positive">272.01%</span>
					</div>
				</div>
				
				<div class=" scheme-dataspacing">
					<div class=" scheme-spacing  ">
						<h5 class="scheme-name scheme-border">Franklin India Smaller Companies Fund - Growth</h5>
					</div>
					<div class="current-nav ">
						<p class="nav">Current NAV </p>
						<span class="value">60.5</span>
					</div>

					<div class="  returns">
						<p class="nav "> Returns (<span> 5years </span> )</p>
						<span class="value positive">272.01%</span>
					</div>
				</div>

				<div class=" scheme-dataspacing">
					<div class=" scheme-spacing  ">
						<h5 class="scheme-name scheme-border">Franklin India Smaller Companies Fund - Growth</h5>
					</div>
					<div class="current-nav ">
						<p class="nav">Current NAV </p>
						<span class="value">60.5</span>
					</div>
					<div class="  returns">
						<p class="nav "> Returns (<span> 5years </span> )</p>
						<span class="value positive">272.01%</span>
					</div>
				</div>


			</div>
		</div>
		<!-- Graphical Comparison -->
		<div class="container">
		  	<div class="row">
				<h4 class="title-spacing sub-head ">Graphical Comparison</h4>
				<div class="graph-spacing">
					<img class="nism" src="img/nism.png " id="myCanvas">
				</div>
			</div>
		</div>
		<!-- footer -->
		<div class="container">
		  	<div class="row">
				<div class="footer-spacing">
					<ul class="list-inline">
						<li class="  "> <img src="img/rf.png"> </li>
						<li class="nism-no">NISM Certified <span class="arn">ARN 116221</span></li>
						<li class=" "><img src="img/sebi.png"></li>
						<li class=" "><img class="nism" src="img/nism.png "></li>
						<li class=" "><img src="img/amfi.png"></li>
					</ul>
				</div>
			</div>
		</div>

<!-- 
		<script>
		var c = document.getElementById("myCanvas");
		var ctx = c.getContext("2d");
		// Create gradient
		var grd = ctx.createLinearGradient(0,0,500,0);
		grd.addColorStop(0,"red");
		grd.addColorStop(1,"white");
		// Fill with gradient
		ctx.fillStyle = grd;
		ctx.fillRect(0,0,1000,1400);
		</script> -->


	</body>
</html>