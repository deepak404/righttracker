<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>RightTracker</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	    <!-- Optional theme -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	    <link rel="stylesheet" type="text/css" href="css/export.css">

	</head>

	<body class="body">

		<!-- Scheme Data Comparison -->
		<section class="compare-sec">
			<div class="container">
					<h3 class=" title">Mutual Fund Scheme Comparison as on {{$date}} for {{$dur}}</h3>
					<hr>
			  	<div class="row">
					<h4 class="title-spacing"><b>Scheme Data Comparison</b> </h4>
			
					<?php $colors = ['#0091ea','#BF71FF','#3EC963','#FF697A','#F88407','#F80786','#E3F807','#FD2B2B','#A3E4D7','#8ea832','#0B5345','#512E5F'];
					$count = 0; ?>
					<!-- scheme div -->
					<?php foreach ($data as $value): ?>
						<div class=" scheme-dataspacing">
							<div class=" scheme-spacing  ">
								<h5 class="scheme-name" style="border-left: 4px solid {{$colors[$count]}};">{{$value->scheme_name}}</h5>
							</div>
							<div class=" current-nav ">
								<p class="nav">Current NAV </p>
								<span class="value">{{$value->current_value}}</span>
							</div>
							<div class="returns ">
								<p class="nav "> Returns <!-- (<span> {{$dur}} </span> ) --></p>
								<span class="value positive">{{$value->current_per}}%</span>
							</div>
						</div>
						<?php $count += 1; ?>
					<?php endforeach ?>
				</div>
			</div>
		</section>
		<!-- Graphical Comparison -->
		<section style="margin-top:30px;">
			<div class="container">
					<h4 class="title-spacing ">Graphical Comparison</h4>
					<img src="{{$name}}.svg" id="myCanvas" height="300px" width="100%">
				</div>
		</section>
		<!-- footer -->
		<footer>
			<div class="container">
			  	<div class="row">
					<ul class="list-inline footer-spacing">
						<li > <img src="img/rf.png"> </li>
						<li ><img src="img/sebi.png"></li>
						<li ><img src="img/nis.png"></li>
						<li ><img src="img/amfi.png"></li>
					</ul>
				</div>
			</div>
		</footer>





	</body>
</html>