<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>RightTracker</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/rmglobal.css">
	<link rel="stylesheet" href="css/rmdash.css">
</head>
<body>
	<!-- Nav Bar For RM User -->
	<nav class="nav navbar-default navbar-fixed-top">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i class="material-icons">menu</i></button>
			<a href="/">
				<img src="img/logo.svg" alt="RightTracker">
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a class="active-menu" href="#">RM Dashboard</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="/logout">
						Logout<i class="material-icons logout-icon">exit_to_app</i>
					</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- Main Container -->
	<div class="container plr-0">
		<h3>RM Details</h3>
		<button id="addAmc" class="btn button_popup btn-primary"><i class="material-icons add-amc">add</i>Add AMC</button>
		<table class="table">
			<thead>
				<tr>
					<th class="left-align">AMC Name</th>
					<th>RM Name</th>
					<th>RM Progress</th>
					<th>Password</th>
					<th>Mail ID</th>
					<th class="left-align">Mobile No</th>
				</tr>
			</thead>
			<tbody>
				@foreach($rminfo as $info)
				<tr>
					<td class="left-align">{{$info['amc_name']}}</td>
					<td>{{$info['rm_name']}}</td>
					<td>{{$info['rm_progress']}}%</td>
					<td>{{$info['password']}}</td>
					<td>{{$info['mail_id']}}</td>
					<td class="left-align">
						{{$info['mobile']}}
						<div class="dropdown pull-right">
						    <button class="more-btn" type="button" data-toggle="dropdown"><i class="material-icons">more_vert</i></button>
						    <ul class="dropdown-menu">
						      <li class="boder-bottom change-pass-li" data-id="{{$info['user_id']}}" ><a href="#">Change Password</a></li>
						      <li class="boder-bottom edit-user-li" data-id="{{$info['user_id']}}" ><a href="#">Edit</a></li>
						      <li class="delete-li" data-id="{{$info['user_id']}}"><a href="#">Delete</a></li>
						    </ul>
						 </div>
					</td>
				</tr>
				@endforeach		
 			</tbody>
		</table>
	</div>	
	<!-- Modal -->
	<div id="addAmcModal" class="modal fade" role="dialog">
	  <div class="modal-dialog ">

	    <!-- Modal content-->
	    <div class="modal-content col-lg-12">
	      <div class="modal-header col-lg-12">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add User</h4>
	      </div>
	      <div class="modal-body col-lg-12 plr-0">
		        <form id="addAmcForm" class="col-lg-12 plr-0">
		        	<div class="col-lg-6 mb-20">
						<select name="amc_name" id="amc_name_id">
							<option value="" selected disabled>AMC Name</option>
						</select>
		        	</div>
		        	<div class="col-lg-6 mb-20">
						<input class="input-css" type="text" name="rm_name" required>
						<span class="bar  input-css"></span>
						<label>RM Name</label>
		        	</div>
		        	<div class="col-lg-6 mb-20">
						<input class="input-css" type="text" name="password" required>
						<span class="bar  input-css"></span>
						<label>Password</label>
		        	</div>
		        	<div class="col-lg-6 mb-20">
						<input class="input-css" type="email" name="mail_id" required>
						<span class="bar input-css"></span>
						<label>Mail ID</label>
		        	</div>
		        	<div class="col-lg-6 mb-20">
						<input class="input-css" type="number" name="mobile" required>
						<span class="bar input-css"></span>
						<label>Mobile No.</label>
		        	</div>
					<div class="col-lg-12">
						<button type="submit" class="btn btn-primary button-mod">Add User</button>
					</div>
		        </form>
	      </div>
	    </div>

	  </div>
	</div>
	<div id="changePassModal" class="modal fade" role="dialog">
		<div class="modal-dialog" id="changePassDiv">
			<div class="modal-content col-lg-12">
				<div class="modal-header col-lg-12">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Change Password</h4>
				</div>
				<div class="modal-body col-lg-12">
					<form id="changePassForm" class="col-lg-12">
						<input type="hidden" name="user_id" value="" id="pass_id">
						<div class="col-lg-12 mb-20">
							<input class="input-css" type="password" name="password" id="password" required>
							<span class="bar  input-css"></span>
							<label>New Password</label>
						</div>
						<div class="col-lg-12">
							<input class="input-css" type="password" name="c_password" id="c_password" required>
							<span class="bar  input-css"></span>
							<label>Confirm Password</label>
							<span id="pass-state"></span>
						</div>
						<div class="col-lg-12 mb-20">
							<button type="submit" class="btn  btn-primary change-btn">Change</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<div id="editAmc" class="modal fade" role="dialog">
	  <div class="modal-dialog ">

	    <!-- Modal content-->
	    <div class="modal-content col-lg-12">
	      <div class="modal-header col-lg-12">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Edit User</h4>
	      </div>
	      <div class="modal-body col-lg-12 plr-0">
		        <form id="editAmcForm" class="col-lg-12 plr-0">
		        	<input type="hidden" name="user_id" id="edit_id">

		        	<div class="col-lg-12 mb-20">
						<input class="input-css" type="text" name="rm_name" id="edit_name" required>
						<span class="bar  input-css"></span>
						<label>RM Name</label>
		        	</div>
		        	<div class="col-lg-12 mb-20">
						<input class="input-css" type="email" name="mail_id" id="edit_mail" required>
						<span class="bar input-css"></span>
						<label>Mail ID</label>
		        	</div>
		        	<div class="col-lg-12 mb-20">
						<input class="input-css" type="number" name="mobile" id="edit_mobile" required>
						<span class="bar input-css"></span>
						<label>Mobile No.</label>
		        	</div>
					<div class="col-lg-12">
						<button type="submit" class="btn btn-primary button-mod">Update</button>
					</div>
		        </form>
	      </div>
	    </div>

	  </div>
	</div>
	<script src="js/chart.js"></script>
	<script src="js/rmdash.js"></script>
	</body>
</html>