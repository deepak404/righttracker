<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>RightTracker</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <!-- CSS Links -->
    <link rel="stylesheet" href="css/rmglobal.css">

</head>

<body>
	<nav class="navbar navbar-expand-md fixed-top">
	      <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav"><span class="navbar-toggler-icon"></span></button>
	      <a class="navbar-brand" href="/"><img src="img/nav-logo.svg" alt="RightTracker" class="nav-logo"></a>
	        <div class="collapse navbar-collapse" id="navbarNav">
	          <ul class="navbar-nav mr-auto">
	              <li class="nav-item">
	                  <a class="nav-link mx-2 active-menu" href="#">RM Progress</a>
	              </li>
	              <li class="nav-item">
	                  <a class="nav-link mx-2" href="#">RM Details</a>
	              </li>
	          </ul>
	          <a href="logout" class="nav-link" >Logout<i class="material-icons ml-1 mb-1 logout-icon">exit_to_app</i></a>
	        </div>
	</nav>
	<div class="container mb-5 ">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
			<h3>RM Progress</h3>
		</div>
		<div class="d-flex flex-column-reverse row-hl">
	      <div class="item-hl mt-5 master-flex">
	      	@foreach($names as $name)
			<div class="d-flex flex-row align-items-stretch row-hl">
		      <div class="p-4 item-hl amc-name">{{$name}}</div>
		      <div class="p-4 item-hl amc-state">
		      	<div class="col-lg-11 progress-div">
			      	<div class="progress-bar-data" style="width: 100%"></div>
		      	</div><span class="pull-right progress-sapn">100%</span>
		      </div>
		    </div>
		    @endforeach
	      </div>
	    </div>
	</div>
</body>
</html>