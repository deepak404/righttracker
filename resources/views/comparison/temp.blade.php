<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
		  <title>RightTracker</title>
  	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

      <script   src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>


      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

      <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

  		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">
  		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
  		<link href="css/schemes.css" rel="stylesheet">
      <link href="css/global.css" rel="stylesheet">
      <link href="css/benchmark.css" rel="stylesheet">
      <link href="css/comparison.css" rel="stylesheet">
  	</head>

<body class="body">
	<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" class="active_navbar_header">Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class=" container  schemes-padding padding-lr-zero">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fund_compare padding-lr-zero">
      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 padding-lr-zero">
          <h2 class=" headtxt " >Fund Comparison</h2>
          <button class="fund-beater" style="display: none;">FB<i style="padding-left:  10px;color: #F57C00;" class="material-icons fund-beatericon">trending_up</i></button>
<!--           <select class="pull-right comp-catogory">
              <option value="" disabled selected>Classification</option>
              <option value="large cap">Large Cap</option>
              <option value="mid cap">Mid Cap</option>
              <option value="ELSS">ELSS</option>
              <option value="sectoral">Sectoral</option>
              <option value="diversified">Diversified Eq.</option>
              <option value="small cap">Small Cap</option>
              <option value="liquid">Liquid</option>
              <option value="long term">Long term</option>
              <option value="short term">Short term</option>
              <option value="ultra short term">Ultra Short term</option>
              <option value="floating rate">Floating rate</option>
              <option value="credit opps">Credit Opps.</option>
              <option value="mip">MIP</option>
              <option value="gilt short term">Gilt Short term</option>
              <option value="gilt long term">Gilt Long term</option>
              <option value="balanced">Balanced</option>
              <option value="ess">ESS</option>
          </select> -->
          <button class=" dropdown-toggle  catagory-dd  pull-right" type="button" id="classification-id" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          Classification
        <span class="caret"></span>
        </button>
        <a class="pt" href="/portfolio_track">PT<i class="material-icons" id="pt-icon">multiline_chart</i></a>
        <ul class="dropdown-menu pull-right dropdown_period catagory-dl" aria-labelledby="classification-id" id="scroll-customize">
           <li class="dropdown-header">Equity</li>
           <li class="cat-li" data-value="diversified"><a href="#">Diversified Eq.</a></li>
           <li class="cat-li" data-value="large cap"><a href="#">Large Cap</a></li>
           <li class="cat-li" data-value="small cap"><a href="#">Small Cap</a></li>
           <li class="cat-li" data-value="mid cap"><a href="#">Mid Cap</a></li>
           <li class="cat-li" data-value="ELSS"><a href="#">ELSS</a></li>
           <li class="cat-li" data-value="arbitrage"><a href="#">Arbitrage</a></li>
           <li class="cat-li" data-value="sectoral"><a href="#">Sectoral</a></li>
           <li class="dropdown-header">Debt</li>
           <li class="cat-li" data-value="liquid"><a href="#">Liquid</a></li>
           <li class="cat-li" data-value="lp"><a href="#">LP</a></li>
           <li class="cat-li" data-value="fmp"><a href="#">FMP</a></li>
           <li class="cat-li" data-value="long term"><a href="#">Long term</a></li>
           <li class="cat-li" data-value="short term"><a href="#">Short term</a></li>
           <li class="cat-li" data-value="ultra short term"><a href="#">Ultra Short term</a></li>
           <li class="cat-li" data-value="floating rate"><a href="#">Floating rate</a></li>
           <li class="cat-li" data-value="gilt short term"><a href="#">Gilt Short term</a></li>
           <li class="cat-li" data-value="gilt long term"><a href="#">Gilt Long term</a></li>
           <li class="cat-li" data-value="mip"><a href="#">MIP</a></li>
           <li class="cat-li" data-value="credit opps"><a href="#">Credit Opps.</a></li>
           <li class="dropdown-header">Hybrid</li>
           <li class="cat-li" data-value="balanced"><a href="#">Balanced</a></li>
           <li class="cat-li" data-value="ess"><a href="#">ESS</a></li>
        </ul>
      </div>
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">    
      <div class="search-spacing">      
        <div class="input-group search_bar search_export">
          <input type="text" class="form-control search_text" placeholder="Search to add funds" id="fundSearch"/>
          <div class="input-group-btn">
            <button class="btn btn-default search-button " type="submit">
            <i class=" material-icons pull-right search_icon">search</i>
            </button>
          </div>
        </div>
      </div>
      <button class=" edit_icon edit_icon_active pull-right"><i class="material-icons pull-right export_icon">file_download</i></button>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fund_compare_section  padding-lr-zero" id="compare_data">
    </div>



    <div class="col-xs-12 col-sm-12 colmd-12 col-lg-12 benchmar_padding padding-lr-zero">
      <h2 class="headtxt">Comparison Graph   </h2>
      <h4 class="seperator ">From</h4> <span id="from_date" class="compare_from ">DD-MM-YYYY</span>
      <!-- <h4 class="seperator ">TO </h4>  -->
      <span id="to_date" class="compare_to positive"></span>  
        
<!--       <select class="graph-years pull-right" >
              <option value="2">5 Days</option>
              <option value="3">1 month</option>
              <option value="4" selected>3 month</option>
              <option value="5">1 Year</option>
              <option value="6">5 Year</option>
              <option value="7">max</option>
      </select> -->
        <button class=" dropdown-toggle  pull-right" type="button" id="pick-date-id" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          3 Months
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="pick-date-id">
            <li class="date-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="date-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="date-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="date-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="date-li" data-value="1"><a href="#">3 Year</a></li>
            <li class="date-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="date-li" data-value="7"><a href="#">Max</a></li>
            <li class="date-li" data-value="9"><a href="#">temp</a></li>
        </ul>
      <div class="card graph_card" id="graph-view-div">

      </div>
    </div>
  </div>
<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
</body>
<script type="text/javascript" src = "{{url('js/smartsearch.js')}}"></script>
<script type="text/javascript" src = "{{url('js/comparison.js?v=1.1')}}"></script>
<script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>

</html>