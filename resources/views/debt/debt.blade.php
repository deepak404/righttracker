<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>RightTracker</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"
    integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
    crossorigin="anonymous"></script>


      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="css/global.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <link href="css/schemes.css" rel="stylesheet">
    <link href="css/schemes-debt.css" rel="stylesheet">
    <link href="css/schemes-popup.css" rel="stylesheet">
     <link href="css/equity.css" rel="stylesheet">
       <link href="css/debt.css" rel="stylesheet">
  </head>

<body class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle active_navbar_header" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
                  <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container schemes-padding">
  <div class="schemes-fixed">
  <h2 class="headtxt">{{$name}}</h2>
  <div>
  <h4 >Top performing funds</h4>
    <hr class="hr_mod">
  </div>
  </div></div>

<div class="container card table_padding ">   
  <div class="head_border  table_mod" >  
    <div class=" Schemes_data "><span class="col-lg-1 padding-lr-zero "></span>
    <span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>
    </div>
      <div class="NAV_head " >Todays NAV</div>
      <div class="d_head active-sort">D<sub>R</sub>
        <i class="material-icons arr-icon d_icon" data-sort="asc">keyboard_arrow_down</i>
      </div>
      <div class=" w_head">W<sub>R</sub>
        <i class="material-icons arr-icon w_icon" data-sort="desc">keyboard_arrow_down</i>
      </div>
      <div class=" m_head">M<sub>R</sub>
        <i class="material-icons arr-icon m_icon" data-sort="desc">keyboard_arrow_down</i>
      </div>
      <div class="y_head ">Y<sub>R</sub>
        <i class="material-icons arr-icon y_icon" data-sort="desc">keyboard_arrow_down</i>
      </div>
      <div class=" ys_head">5Y<sub>(CAGR)</sub>
        <i class="material-icons arr-icon ys_icon" data-sort="desc">keyboard_arrow_down</i>
      </div>
      <div class="asset_size_head ">Asset size  Cr.</div>
  </div>
  <div class="data-container">
    <?php $datacount = 0; ?>
    <?php if (count($schemeData) != 0): ?>
      @foreach($schemeData as $value)
      @if ($datacount != 5) 
      <div class="data_border table_data_mod"  data-toggle="collapse" href="#{{$value['scheme_code']}}">  
        <div class=" Schemes_data ">
          <span class="col-lg-1 padding-lr-zero ">
            <i class="material-icons list_radio">keyboard_arrow_down</i>
          </span>
          <span class="col-lg-11 padding-lr-zero  fund_name">{{$value['scheme_name']}}</span>
        </div>
        <div class="NAV_head positive" >{{$value['todays_nav']}}</div>
            <?php if ($value['day_return'] > 0): ?>
        <div class=" d_data positive d_val">{{$value['day_return']}}</div>
          <?php else: ?>
        <div class=" d_data negetive d_val">{{$value['day_return']}}</div>
        <?php endif ?>
        <?php if (array_key_exists('week_return', $value)): ?>
        <?php if ($value['week_return'] > 0): ?>
        <div class="  w_data positive w_val">{{$value['week_return']}}</div>
          <?php else: ?>
        <div class="  w_data negetive w_val">{{$value['week_return']}}</div>
        <?php endif ?>
          <?php else: ?>
        <div class="  w_data">-</div>
        <?php endif ?>

        <?php if (array_key_exists('month_return', $value)): ?>
        <?php if ($value['month_return'] > 0): ?>
        <div class="  m_data positive m_val">{{$value['month_return']}}</div>
          <?php else: ?>
        <div class="  m_data negetive m_val">{{$value['month_return']}}</div>
        <?php endif ?>
          <?php else: ?>
        <div class="  m_data"></div>
        <?php endif ?>

        <?php if (array_key_exists('year_return', $value)): ?>
        <?php if ($value['year_return'] > 0): ?>
        <div class=" y_data positive y_val">{{$value['year_return']}}</div>
          <?php else: ?>
        <div class=" y_data negetive y_val">{{$value['year_return']}}</div>
        <?php endif ?>
          <?php else: ?>
        <div class=" y_data">-</div>
        <?php endif ?>

        <?php if (array_key_exists('five_year_return', $value)): ?>
          <?php if ($value['five_year_return'] > 0): ?>
          <div class=" ys_data positive ys_val">{{$value['five_year_return']}}</div>
            <?php else: ?>
          <div class=" ys_data negetive ys_val">{{$value['five_year_return']}}</div>
          <?php endif ?>
        <?php else: ?>
          <div class=" ys_data">-</div>
        <?php endif ?>
        <div class="asset_size_head ">{{$value['asset_size']}}</div>

        <div id="{{$value['scheme_code']}}" class="panel-collapse collapse">
          <div class=" Schemes_data ">
            <span class="col-lg-1 padding-lr-zero "></span>
            <span class="col-lg-6 padding-lr-zero  exit_load">Exit load <br>
              <span class="exit_load_data">{{$value['exit_load']}}</span>
            </span>
            <span class="col-lg-5 padding-lr-zero  exit_load">BRK <br>
              <span class="exit_load_data">{{$value['brk']}}</span>
            </span>
          </div>
          <div class="NAV_head B-Ratio" >Avg.Maturity<br>
           <span class="B-Ratio_data">{{$value['avg_mat']}}</span>
          </div>
          <div class=" d_data Sharpe_Ratio">Modi Dur. <br>
            <span class="Sharpe_Ratio_data">{{$value['mod_duration']}}</span>
          </div>
          <div class="  m_data Composition">
            <span class="legents">
              <i class="material-icons Composition_circles Composition_circles_s">lens</i>%>AA Rated
              <span class="smallcap_percent">   {{$value['AA_return']}}%</span>

              <i class="material-icons Composition_circles Composition_circles_m">lens</i>Govt.Sec
              <span class="midcap_percent">   {{$value['gov_sec']}}%</span>

              <i class="material-icons Composition_circles Composition_circles_o">lens</i>Others
              <span class="largecap_percent">{{$value['others']}}%</span>
              
                <span class="total">Total<span> {{$value['total']}}%</span></span>
            </span>
            <br>
            <ul class="Composition_bar list-inline ">
                <li class="Composition_smallcap padding-lr-zero" style="width: {{$value['AA_return']}}% !important;"></li>
                <li class="Composition_midcap padding-lr-zero" style="width: {{$value['gov_sec']}}% !important;"></li>
                <li class="Composition_others padding-lr-zero" style="width: {{$value['others']}}% !important;"></li>
            </ul>
          </div>
        </div>
      </div>
      @else
        <h4 class="padding_tb allscheme-title">All Schemes</h4>

        <div class="container table_padding">   
          <div class="head_border  table_mod" >  
            <div class=" Schemes_data ">
              <span class="col-lg-1 padding-lr-zero "></span>
              <span class="col-lg-11 padding-lr-zero  fund_name">Schemes</span>
            </div>
            <div class="NAV_head " >Todays NAV</div>
            <div class="d_head active-sort">D<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>
            <div class=" w_head">W<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>
            <div class=" m_head">M<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>
            <div class="y_head ">Y<sub>R</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>
            <div class=" ys_head">5Y<sub>(CAGR)</sub><i class="material-icons arr-icon">keyboard_arrow_down</i></div>
            <div class="asset_size_head ">Asset size  Cr.</div>
          </div>    
        </div>
        <div class="data_border table_data_mod"  data-toggle="collapse" href="#{{$value['scheme_code']}}">  
        <div class=" Schemes_data ">
          <span class="col-lg-1 padding-lr-zero ">
            <i class="material-icons list_radio">keyboard_arrow_down</i>
          </span>
          <span class="col-lg-11 padding-lr-zero  fund_name">{{$value['scheme_name']}}</span>
        </div>
        <div class="NAV_head positive" >{{$value['todays_nav']}}</div>
            <?php if ($value['day_return'] > 0): ?>
        <div class=" d_data positive d_val">{{$value['day_return']}}</div>
          <?php else: ?>
        <div class=" d_data negetive d_val">{{$value['day_return']}}</div>
        <?php endif ?>
        <?php if (array_key_exists('week_return', $value)): ?>
        <?php if ($value['week_return'] > 0): ?>
        <div class="  w_data positive w_val">{{$value['week_return']}}</div>
          <?php else: ?>
        <div class="  w_data negetive w_val">{{$value['week_return']}}</div>
        <?php endif ?>
          <?php else: ?>
        <div class="  w_data">-</div>
        <?php endif ?>

        <?php if (array_key_exists('month_return', $value)): ?>
        <?php if ($value['month_return'] > 0): ?>
        <div class="  m_data positive m_val">{{$value['month_return']}}</div>
          <?php else: ?>
        <div class="  m_data negetive m_val">{{$value['month_return']}}</div>
        <?php endif ?>
          <?php else: ?>
        <div class="  m_data"></div>
        <?php endif ?>

        <?php if (array_key_exists('year_return', $value)): ?>
        <?php if ($value['year_return'] > 0): ?>
        <div class=" y_data positive y_val">{{$value['year_return']}}</div>
          <?php else: ?>
        <div class=" y_data negetive y_val">{{$value['year_return']}}</div>
        <?php endif ?>
          <?php else: ?>
        <div class=" y_data">-</div>
        <?php endif ?>

        <?php if (array_key_exists('five_year_return', $value)): ?>
          <?php if ($value['five_year_return'] > 0): ?>
          <div class=" ys_data positive ys_val">{{$value['five_year_return']}}</div>
            <?php else: ?>
          <div class=" ys_data negetive ys_val">{{$value['five_year_return']}}</div>
          <?php endif ?>
        <?php else: ?>
          <div class=" ys_data">-</div>
        <?php endif ?>
        <div class="asset_size_head ">{{$value['asset_size']}}</div>

        <div id="{{$value['scheme_code']}}" class="panel-collapse collapse">
          <div class=" Schemes_data ">
            <span class="col-lg-1 padding-lr-zero "></span>
            <span class="col-lg-6 padding-lr-zero  exit_load">Exit load <br>
              <span class="exit_load_data">{{$value['exit_load']}}</span>
            </span>
            <span class="col-lg-5 padding-lr-zero  exit_load">BRK <br>
              <span class="exit_load_data">{{$value['brk']}}</span>
            </span>
          </div>
          <div class="NAV_head B-Ratio" >Avg.Maturity<br>
           <span class="B-Ratio_data">{{$value['avg_mat']}}</span>
          </div>
          <div class=" d_data Sharpe_Ratio">Modi Dur. <br>
            <span class="Sharpe_Ratio_data">{{$value['mod_duration']}}</span>
          </div>
          <div class="  m_data Composition">
            <span class="legents">
              <i class="material-icons Composition_circles Composition_circles_s">lens</i>%>AA Rated
              <span class="smallcap_percent">   {{$value['AA_return']}}%</span>

              <i class="material-icons Composition_circles Composition_circles_m">lens</i>Govt.Sec
              <span class="midcap_percent">   {{$value['gov_sec']}}%</span>

              <i class="material-icons Composition_circles Composition_circles_o">lens</i>Others
              <span class="largecap_percent">{{$value['others']}}%</span>
              
              <span class="total">Total<span> {{$value['total']}}%</span></span>
           
            </span>
            <br>
            <ul class="Composition_bar list-inline ">
                <li class="Composition_smallcap padding-lr-zero" style="width: {{$value['AA_return']}}% !important;"></li>
                <li class="Composition_midcap padding-lr-zero" style="width: {{$value['gov_sec']}}% !important;"></li>
                <li class="Composition_others padding-lr-zero" style="width: {{$value['others']}}% !important;"></li>
            </ul>
          </div>
        </div>
      </div>
      @endif
      <?php $datacount += 1; ?>
      @endforeach
     <?php else: ?>
        <div class="data_border table_data_mod" style="height: 40px; text-align: center;">No Data Found</div>
    <?php endif ?>
  </div>
</div>
<script type="text/javascript" src="js/addscheme.js"></script>
<script type="text/javascript" src="{{url('js/diversified.js')}}"></script>

</body>
</html>