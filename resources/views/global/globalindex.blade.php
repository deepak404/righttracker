<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>RightTracker</title>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	    <!-- Optional theme -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	    <script   src="https://code.jquery.com/jquery-3.2.1.js"
	      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
	      crossorigin="anonymous"></script>
	    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	    <link rel="stylesheet" href="css/globalindex.css" >
	    <link rel="stylesheet" href="css/global.css" >
	    <link rel="stylesheet" type="text/css" href="css/schemes.css">

	</head>

<body class="body">
	<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle active_navbar_header"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

   <div class=" container  schemes-padding padding-lr-zero">
   	<div class="row">
   	<h2 class=" headtxt " >Global Index</h2>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 index-spacing padding-lr-zero">
	    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 side-bar padding-l-zero" id="scroll-customize">

	      	<div class="index-card card  card-custom active-card" data-title="Dow Jones Industrial Average" data-points="{{$globalindex['DowJones']['Close']}}" data-diff="{{$globalindex['DowJones']['diff']}}" data-per="{{$globalindex['DowJones']['per']}}" data-value="DowJones" id="DowJones">
	      		<div class="points-div">
		      		<h4 class="index-head">Dow Jones Industrial Average</h4>
		      		<span  class="index-value" id="">{{$globalindex['DowJones']['Close']}}</span>
		      		@if($globalindex['DowJones']['diff'] > 0)
		      		<span class="index-value positive"  id="" > {{$globalindex['DowJones']['diff']}}</span> 
		      		<span class="index-value positive"   id="">({{$globalindex['DowJones']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span class="index-value negetive"  id="" > {{$globalindex['DowJones']['diff']}}</span> 
		      		<span class="index-value negetive"   id="">({{$globalindex['DowJones']['per']}}%)</span>
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif

		      	</div>
		    </div>

		    <div class="index-card card  card-custom" data-title="NASDAQ-100" data-points="{{$globalindex['Nacdaq']['Close']}}" data-diff="{{$globalindex['Nacdaq']['diff']}}" data-per="{{$globalindex['Nacdaq']['per']}}" data-value="Nacdaq">
	      		<div class="points-div">
		      		<h4 class="index-head">NASDAQ-100</h4>
		      		<span class="index-value"  id="">{{$globalindex['Nacdaq']['Close']}}</span>
		      		@if($globalindex['Nacdaq']['diff'] > 0)
		      		<span class="index-value positive"  id="">{{$globalindex['Nacdaq']['diff']}}</span> 
		      		<span class="index-value positive"   id="">({{$globalindex['Nacdaq']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span class="index-value negetive"  id="">{{$globalindex['Nacdaq']['diff']}}</span> 
		      		<span class="index-value negetive"   id="">({{$globalindex['Nacdaq']['per']}}%)</span>	      		
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif
		      	</div>
		    </div>

		    <div class="index-card card  card-custom " data-title="FTSE 100 Index" data-points="{{$globalindex['Ftse']['Close']}}" data-diff="{{$globalindex['Ftse']['diff']}}" data-per="{{$globalindex['Ftse']['per']}}" data-value="Ftse ">
	      		<div class="points-div">
		      		<h4 class="index-head">FTSE 100 Index</h4>
		      		<span class="index-value"  id="">{{$globalindex['Ftse']['Close']}}</span>
		      		@if($globalindex['Ftse']['diff'] > 0)
		      		<span  class="index-value positive" id="" >{{$globalindex['Ftse']['diff']}}</span> 
		      		<span class="index-value positive"   id="">({{$globalindex['Ftse']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span  class="index-value negetive" id="" >{{$globalindex['Ftse']['diff']}}</span> 
		      		<span class="index-value negetive"   id="">({{$globalindex['Ftse']['per']}}%)</span>
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif

		      	</div>
		      	
		    </div>

		    <div class="index-card card  card-custom " data-title="SSE Composite Index" data-points="{{$globalindex['Sse']['Close']}}" data-diff="{{$globalindex['Sse']['diff']}}" data-per="{{$globalindex['Sse']['per']}}" data-value="Sse">
	      		<div class="points-div">
		      		<h4 class="index-head">SSE Composite Index</h4>
		      		<span  class="index-value" id="">{{$globalindex['Sse']['Close']}}</span>
		      		@if($globalindex['Sse']['diff'] > 0)
		      		<span  class="index-value positive" id="" >{{$globalindex['Sse']['diff']}}</span>
		      		<span  class="index-value positive"  id="" >({{$globalindex['Sse']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span  class="index-value negetive" id="" >{{$globalindex['Sse']['diff']}}</span>
		      		<span  class="index-value negetive"  id="" >({{$globalindex['Sse']['per']}}%)</span>
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif

		      	</div>
		      	
		    </div>

		    <div class="index-card card  card-custom " data-title="Hang Seng Index" data-points="{{$globalindex['Hansen']['Close']}}" data-diff="{{$globalindex['Hansen']['diff']}}" data-per="{{$globalindex['Hansen']['per']}}" data-value="Hansen">
	      		<div class="points-div">
		      		<h4 class="index-head">Hang Seng Index</h4>
		      		<span class="index-value"  id="">{{$globalindex['Hansen']['Close']}}</span>
		      		@if($globalindex['Hansen']['diff'] > 0)
		      		<span class="index-value positive"  id="" >{{$globalindex['Hansen']['diff']}}</span> 
		      		<span  class="index-value positive"  id="" >({{$globalindex['Hansen']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span class="index-value negetive"  id="" >{{$globalindex['Hansen']['diff']}}</span> 
		      		<span  class="index-value negetive"  id="" >({{$globalindex['Hansen']['per']}}%)</span>
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif
		      	</div>
		    </div>

		    <div class="index-card card  card-custom" data-title="Nikkei 225" data-points="{{$globalindex['Nikkei']['Close']}}" data-diff="{{$globalindex['Nikkei']['diff']}}" data-per="{{$globalindex['Nikkei']['per']}}" data-value="Nikkei">
	      		<div class="points-div">
		      		<h4 class="index-head">Nikkei 225</h4>
		      		<span class="index-value"  id="">{{$globalindex['Nikkei']['Close']}}</span>
		      		@if($globalindex['Nikkei']['diff'] > 0)
		      		<span  class="index-value positive" id="" >{{$globalindex['Nikkei']['diff']}}</span> 
		      		<span  class="index-value positive"  id="" >({{$globalindex['Nikkei']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span  class="index-value negetive" id="" >{{$globalindex['Nikkei']['diff']}}</span> 
		      		<span  class="index-value negetive"  id="" >({{$globalindex['Nikkei']['per']}}%)</span>		      		
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif
		      	</div>
		    </div>

		    <div class="index-card card  card-custom" data-title="DAX" data-points="{{$globalindex['Dax']['Close']}}" data-diff="{{$globalindex['Dax']['diff']}}" data-per="{{$globalindex['Dax']['per']}}" data-value="Dax">
	      		<div class="points-div">
		      		<h4 class="index-head">Dax</h4>
		      		<span class="index-value"  id="">{{$globalindex['Dax']['Close']}}</span>
		      		@if($globalindex['Dax']['diff'] > 0)
		      		<span  class="index-value positive" id="" >{{$globalindex['Dax']['diff']}}</span> 
		      		<span  class="index-value positive"  id="" >({{$globalindex['Dax']['per']}}%)</span>
		      		<i id=" " class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
		      		@else
		      		<span  class="index-value negetive" id="" >{{$globalindex['Dax']['diff']}}</span> 
		      		<span  class="index-value negetive"  id="" >({{$globalindex['Dax']['per']}}%)</span>		      		
		      		<i id=" " class="material-icons negetive upward_arrow arrow">arrow_drop_down</i>
		      		@endif
		      	</div>
		    </div>

	    </div>	

      	<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 ">
      		<div class="points-div ">
		      	<h4 class="index-head" id="detail-title">-</h4>
	      		<span class="graphindex-points" id="detail-point">-</span>
	      		<span class="graphindex-points positive" id="detail-diff">-</span> 
	      		<span class="graphindex-points positive" id="detail-per" >-</span>
	      		<i id="state-icon" class="material-icons positive upward_arrow arrow">arrow_drop_up</i>
	      	</div>
	      	<div class="pointssel-div">
				<span id="change-date"></span>	 |  
				<span id="change-points"></span>
				<span class="positive" id="change-per"></span>
	      	</div>
		    <select class="globalgraph-years pull-right" id="distenc-state">
	            <option value="1">1 Day</option>
	            <option value="2">5 Days</option>
	            <option value="3">1 month</option>
	            <option value="4" selected>3 month</option>
	            <option value="5">1 Year</option>
	            <option value="6">5 Years</option>
	            <option value="7">max</option>
            </select>

      		<div class="card graph_card" id="global-chart">	
        		<!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
      		</div>
      	</div>
    </div>
</div>
</div>
<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
</body>
<script type="text/javascript" src="js/global.js"></script>
<script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>

</html>
