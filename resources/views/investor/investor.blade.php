<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
		  <title>RightTracker</title>
  	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

      <script   src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>

      <script src="js/jquery-ui.js"></script>

      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>

  		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">
  		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
      <link rel="stylesheet" href="css/jquery-ui.css">
      <link href="css/global.css" rel="stylesheet">
      <link href="css/investor.css" rel="stylesheet">
      <link href="css/comparison.css" rel="stylesheet">


  	</head>

<body class="body">
	<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" class="active_navbar_header">Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="col-xs-12 header-div">
          <h2>Portfolio Track</h2>
          <button class="add-btn pull-right" id="add-option" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="material-icons">apps</i></button>
          <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="add-option">
            <li id="add-investor"><a href="#">Add Investor</a></li>
            <li id="add-investments"><a href="#">Add Investment</a></li>
            <li id="delete-data"><a href="#">Remove</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <form class="col-xs-12" id="portfolio-form">
          <input class="data-input" type="hidden" name="inv_id" id="inv_id" value="">
          <div class="col-lg-3">
            <input class="data-input" autocomplete="off" name="name" type="text" id="name-input" placeholder="Pick Investor" required>
          </div>
          <div class="col-lg-3">
            <input class="data-input date-input" autocomplete="off" name="from_date" type="text" placeholder="From Date" required>
          </div>
          <div class="col-lg-3">
            <input class="data-input date-input" autocomplete="off" name="to_date" type="text" placeholder="To Date" required>
          </div>
          <div class="col-lg-3">
            <input class="data-input sub-btn" type="submit" value="Apply">
          </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <h3>Equity</h3>
        <div class="col-xs-12">
          <h4 class="h4-color">BSE Sensex</h4>
          <div class="col-xs-12 mb-10" id="sensex-info">

          </div>
          <!-- <h3>Comparison Graph   <span>DD-MM-YYYY</span>  <span></span> </h3> -->
          <div class="col-xs-12 chart p-lr-0" id="sensex-chart">
            
          </div>
        </div>
        <div class="col-xs-12">
          <h4 class="h4-color">BSE SmallCap</h4>
          <div class="col-xs-12 mb-10" id="small-info">

          </div>
          <!-- <h3>Comparison Graph   <span>DD-MM-YYYY</span>  <span></span> </h3> -->
          <div class="col-xs-12 chart p-lr-0" id="small-chart">
            
          </div>
        </div>
        <div class="col-xs-12">
          <h4 class="h4-color">BSE MidCap</h4>
          <div class="col-xs-12 mb-10" id="mid-info">

          </div>
          <!-- <h3>Comparison Graph   <span>DD-MM-YYYY</span>  <span></span> </h3> -->
          <div class="col-xs-12 chart p-lr-0" id="mid-chart">
            
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <h3>Debt</h3>
        <div class="col-xs-12">
          <h4 class="h4-color">Gov. Bonds</h4>
          <div class="col-xs-12 mb-10" id="gov-info">
            
          </div>
          <!-- <h3>Comparison Graph   <span>DD-MM-YYYY</span>  <span></span> </h3> -->
          <div class="col-xs-12 chart p-lr-0" id="gov-chart">
            
          </div>
        </div>
        <div class="col-xs-12">
          <h4 class="h4-color">FD</h4>
          <div class="col-xs-12 mb-10" id="fd-info">
            
          </div>
          <!-- <h3>Comparison Graph   <span>DD-MM-YYYY</span>  <span></span> </h3> -->
          <div class="col-xs-12 chart p-lr-0" id="fd-chart">
            
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <h3>Hybrid</h3>
        <div class="col-xs-12">
          <h4 class="h4-color">BSE Sensex</h4>
          <div class="col-xs-12 mb-10" id="bal-info">
            
          </div>
          <!-- <h3>Comparison Graph   <span>DD-MM-YYYY</span>  <span></span> </h3> -->
          <div class="col-xs-12 chart p-lr-0" id="bal-chart">
            
          </div>
        </div>
      </div>
    </div>
    <div class="bottom">
      
    </div>
  </div>

<!-- Add Investor Modal -->
<div id="investorModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content col-xs-12">
      <div class="modal-header col-xs-12">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Investor</h4>
      </div>
      <div class="modal-body col-xs-12">
          <form class="col-xs-12" id="investor-form">
            <input id="inv_name" name="name" type="text" placeholder="Investor Name" required>
            <button type="submit">Add</button>
          </form>
      </div>
  </div>
</div>
</div>

<!-- Add Investments Modal -->
<div id="investmentModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content col-xs-12">
      <div class="modal-header col-xs-12">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Investment</h4>
      </div>
      <div class="modal-body col-xs-12">
          <form class="col-xs-12" id="investment-form">
              <select name="inv_name" id="inv_name_select">
              </select>
              <input type="text" id="search-scheme" autocomplete="off">
              <input type="text" id="tag-input" data-role="tagsinput" required>
              <button type="submit">Add</button>
          </form>
      </div>
    </div>

  </div>
</div>

<!-- Delete Modal -->
<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content col-xs-12">
      <div class="modal-header col-xs-12">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Info</h4>
      </div>
      <div class="modal-body col-xs-12">
          <div class="col-xs-11">
            <select name="Investor" id="inv_name_del">

            </select>
          </div>
          <div class="col-xs-1">
              <i class="material-icons" id="delete-user" data-user="0" title="Delete Investor">delete_forever</i>
          </div>
          <div class="col-xs-12 table-div">
            <table>
              <thead>
                <tr>
                  <th>Scheme Name</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody class="schemes-tbody">
                <tr>
                  <td>No Data Found</td>
                </tr>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
</body>
<script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
<script src="js/investor.js?v=1.2"></script>
</html> 