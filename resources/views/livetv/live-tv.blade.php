<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>RightTracker</title>

    
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    <script   src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>


    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script data-cfasync="false">
      (function(r,e,E,m,b){E[r]=E[r]||{};E[r][b]=E[r][b]||function(){
      (E[r].q=E[r].q||[]).push(arguments)};b=m.getElementsByTagName(e)[0];m=m.createElement(e);
      m.async=1;m.src=("file:"==location.protocol?"https:":"")+"//s.reembed.com/G-X30aPn.js";
      b.parentNode.insertBefore(m,b)})("reEmbed","script",window,document,"api");
    </script>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{url('css/index.css')}}" rel="stylesheet">
		<link href="{{url('css/schemes.css')}}" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/benchmark.css')}}" rel="stylesheet">
<!--     <style>
      .rmp-poster{
        display: none !important;
      }
    </style> -->
	</head>

<body class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle active_navbar_header"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
              <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container schemes-padding">
 
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding ">
  <div>
    <h2 class="headtxt">BloombergQuint Live</h2>
  </div>
  <div class="card benchmark_card" id="usd-chart">
    <iframe width="100%" height="100%" src="http://www.zengatv.com/embed?v=c3a01a97-0714-49c7-820f-b94dcffd07fa.html&t=live" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  </div>
</div>


<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding">
  <div>
    <h2 class="headtxt">Bloomberg Global Live
       
    </h2>
  </div>
  <div class="card benchmark_card" id="sgd-chart">
    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Ga3maNZ0x0w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding ">
  <div>
    <h2 class="headtxt">ET NOW Live</h2>
    
  </div>
  <div class="card benchmark_card" id="gbp-chart">
  <iframe width="100%" height="100%" src="http://www.zengatv.com/embed?v=9b91441a-99d4-11e1-85c0-123141010175.html&t=live" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
  </div>
</div>
<!-- ajax loader -->
<!-- <div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div> -->
<!-- <script type="text/javascript" src="{{url('js/forex.js')}}"></script> -->
<!-- <script type="text/javascript" src ="{{url('js/highstock.js')}}"></script> -->
  <script>
    $(document).ready(function(){ 
      $('.rmp-poster').empty();
    });
  </script>
</body>
</html>