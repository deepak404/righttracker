<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>RightTracker</title>


     <link rel="stylesheet" href="{{url('/css/bootstrap.min.css')}}">

    <!-- Optional theme -->
    <link rel="stylesheet" href="{{url('/css/bootstrap-theme.min.css')}}">
		<script>if (typeof module === 'object') {window.module = module; module = undefined;}</script>

    <script src="{{url('/js/jquery-3.2.1.js')}}"> </script>

    <script src="{{url('js/jquery-ui.js')}}"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="{{url('/js/bootstrap.min.js')}}"></script>
		<script>if (window.module) module = window.module;</script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
      <link href="css/investor.css" rel="stylesheet">
    <link href="{{url('css/index.css')}}" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/schemes.css')}}" rel="stylesheet">
    <link href="{{url('css/tvmodule.css')}}" rel="stylesheet">
      <link rel="stylesheet" href="css/jquery-ui.css">
<style>
	.no-nav{
		margin-top: 0px !important;
	}
	.master-logo{
		width: 12%;
		margin: 10px;
	}
</style>
	</head>
<?php if ($type == 'app'): ?>
	<body class="body no-nav">
<?php else: ?>
	<body class="body">
		<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>

                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>

              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li>
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle active_navbar_header"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
              <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<?php endif; ?>
  <div class="container-flude col-lg-12" id="master-div">
    <div class="col-lg-8">
      <div class="col-lg-12">
				<?php if ($type == 'app'): ?>
					<img src="{{url('/img/nav-logo.svg')}}" class="master-logo">
					<?php else: ?>
						<h3>TV MODULE</h3>
				<?php endif; ?>
        <button class=" dropdown-toggle  pull-right" type="button" id="pick-date-id" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          3 Months
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="pick-date-id">
            <li class="date-li" data-value="1"><a href="#">5 Days</a></li>
            <li class="date-li" data-value="2"><a href="#">1 Month</a></li>
            <li class="date-li" data-value="3"><a href="#">3 Months</a></li>
            <li class="date-li" data-value="4"><a href="#">1 Year</a></li>
            <li class="date-li" data-value="5"><a href="#">3 Year</a></li>
            <li class="date-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="date-li" data-value="7"><a href="#">Max</a></li>
        </ul>
				<?php if ($type != 'app'): ?>
					<button class="pull-right" id="update-pe">Upload P/E</button>
				<?php endif; ?>
      </div>
      <div class="col-lg-6">
        <h4>Sensex</h4><span class="index-val" id="sensex-val">{{$sensex['points']}}</span>
        @if($sensex['Diff'] > 0)
        <span id="sensex-chg" class="positive"> {{$sensex['Diff']}} ({{$sensex['Per']}}%)</span>
        @else
        <span id="sensex-chg" class="negetive"> {{$sensex['Diff']}} ({{$sensex['Per']}}%)</span>
        @endif
        <div class="col-lg-12 graph-div" id="sensex">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>P/E Sensex</h4><span class="index-pe" id="sensex-pe">20.50</span>
        <div class="col-lg-12 graph-div" id="pe-sensex">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>Nifty</h4><span class="index-val" id="nifty-val">{{$nifty['points']}}</span>
        @if($nifty['Diff'] > 0)
        <span id="nifty-chg" class="positive"> {{$nifty['Diff']}} ({{$nifty['Per']}}%)</span>
        @else
        <span id="nifty-chg" class="negetive"> {{$nifty['Diff']}} ({{$nifty['Per']}}%)</span>
        @endif
        <div class="col-lg-12 graph-div" id="nifty">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>P/E Nifty</h4><span class="index-pe" id="nifty-pe">20.50</span>
        <div class="col-lg-12 graph-div" id="pe-nifty">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>NSE Midcap</h4><span class="index-val" id="midcap-val">{{$midcap['points']}}</span>
        @if($midcap['Diff'] > 0)
        <span id="midcap-chg" class="positive"> {{$midcap['Diff']}} ({{$midcap['Per']}}%)</span>
        @else
        <span id="midcap-chg" class="negetive"> {{$midcap['Diff']}} ({{$midcap['Per']}}%)</span>
        @endif
        <div class="col-lg-12 graph-div" id="midcap">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>P/E NSE Midcap</h4><span class="index-pe" id="midcap-pe">20.50</span>
        <div class="col-lg-12 graph-div" id="pe-midcap">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>NSE Smallcap</h4><span class="index-val" id="smallcap-val">{{$smallcap['points']}}</span>
        @if($smallcap['Diff'] > 0)
          <span id="smallcap-chg" class="positive"> {{$smallcap['Diff']}} ({{$smallcap['Per']}}%)</span>
        @else
          <span id="smallcap-chg" class="negetive"> {{$smallcap['Diff']}} ({{$smallcap['Per']}}%)</span>
        @endif
        <div class="col-lg-12 graph-div" id="smallcap">

        </div>
      </div>
      <div class="col-lg-6">
        <h4>P/E NSE Smallcap</h4><span class="index-pe" id="smallcap-pe">20.50</span>
        <div class="col-lg-12 graph-div" id="pe-smallcap">

        </div>
      </div>
    </div>
		<div class="col-lg-4 table-header">
			<table id="my-head">
				<thead>
					<tr>
						<th id="sector">Sector</th>
						<th>1M</th>
						<th>1Yr</th>
						<th>3Yr</th>
						<th>P/E</th>
					</tr>
				</thead>
			</table>
		</div>
    <div class="col-lg-4 table-div">
      <table class="table-hover col-lg-12">
        <tbody>
          @foreach($sector as $key=>$value)
            <tr>
              <td>{{$key}}</td>
              @if(empty($value['month_return']))
                <td>NA</td>
              @else
                @if($value['month_return'] > 0)
                  <td class="positive">{{$value['month_return']}}%</td>
                @else
                  <td class="negetive">{{$value['month_return']}}%</td>
                @endif
              @endif
              @if(empty($value['year_return']))
                <td>NA</td>
              @else
                @if($value['year_return'] > 0)
                  <td class="positive">{{$value['year_return']}}%</td>
                @else
                  <td class="negetive">{{$value['year_return']}}%</td>
                @endif
              @endif
              @if(empty($value['three_year_return']))
              <td>NA</td>
              @else
                @if($value['three_year_return'] > 0)
                  <td class="positive">{{$value['three_year_return']}}%</td>
                @else
                  <td class="negetive">{{$value['three_year_return']}}%</td>
                @endif
              @endif
              <td class="pe-val" data-id="1" title="{{$value['pedate']}}">{{$value['pe']}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-lg-4 card">
      <iframe id="myiframe" width="100%" height="100%" src="https://www.bloombergquint.com/live-TV" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
  <div class="modal fade" id="pemodal" role="dialog">
    <div class="modal-dialog" id="modal-dialog">
      <div class="modal-content col-lg-12">
        <div class="modal-header col-lg-12">
          <h3 class="modal-title" id="ptmodal-title">Update P/E</h3>
          <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
        </div>
        <div class="modal-body col-lg-12">
          <form  id="policy-update-form">
            <div class="group  col-md-12  col-lg-12 padding-lr-zero">
              <input type="text" name="date" id="pe-date" required>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label class="modal-label">Date (YYYY/MM/DD)</label>
            </div>
            <div class="group  col-md-12  col-lg-12 padding-lr-zero">
              <input type="text" name="sensex">
              <span class="highlight"></span>
              <span class="bar"></span>
              <label class="modal-label">Sensex P/E</label>
            </div>
            <div class="group  col-md-12  col-lg-12 padding-lr-zero">
              <input type="text" name="nifty">
              <span class="highlight"></span>
              <span class="bar"></span>
              <label class="modal-label">Nifty P/E</label>
            </div>
            <div class="group  col-md-12  col-lg-12 padding-lr-zero">
              <input type="text" name="smallcap">
              <span class="highlight"></span>
              <span class="bar"></span>
              <label class="modal-label">Smallcap P/E</label>
            </div>
            <div class="group  col-md-12  col-lg-12 padding-lr-zero">
              <input type="text" name="midcap">
              <span class="highlight"></span>
              <span class="bar"></span>
              <label class="modal-label">Midcap P/E</label>
            </div>
            <div class="col-lg-12 padding-lr-zero">
              <button type="submit" class="btn button_popup btn-primary ">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>
<script type="text/javascript" src ="{{url('js/tvmodule.js?v=1.5')}}"></script>
</body>
</html>
