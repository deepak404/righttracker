<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Righttacker TV</title>
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <script src="{{url('/js/jquery-3.2.1.js')}}"> </script>

    <style>
        body{
            margin: 5px !important;
        }
        .cord{
            background: gray !important;
        }
        .master {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-gap: 5px;
            grid-template-rows: 10%;
            grid-auto-rows: minmax(29%, 29%);
            height: 99vh;
        }
        #one-title{
            grid-column: 1/2;
            grid-row: 1;
        }
        #one-g1{
            grid-column: 1/2;
            grid-row: 2;
        }
        #one-g2{
            grid-column: 1/2;
            grid-row: 3;
        }
        #one-g3{
            grid-column: 1/2;
            grid-row: 4;
        }
        #two-title{
            grid-column: 2/3;
            grid-row: 1;
        }
        #two-g1{
            grid-column: 2/3;
            grid-row: 2;
        }
        #two-g2{
            grid-column: 2/3;
            grid-row: 3;
        }
        #two-g3{
            grid-column: 2/3;
            grid-row: 4;
        }
        .title>.index{
            margin-top: 10px;
            margin-left: 5px;
            font-size: 40px;
            font-weight: bold;
            /* color: #f88407 !important; */
        }
        .val{
            font-size: 35px !important;
        }
        .chg{
            font-size: 35px !important;
        }
        .per{
            font-size: 32px !important;
            font-weight: 200;
        }
        .pe{
            font-size: 20px !important;
            color: #f88407 !important;
        }
    </style>
</head>
<body class="body">
    <div class="master">
        <div class="title" id="one-title">
            <P class="index">{{$leftTitle}} <span class="val">100000</span> <span class="chg negetive">100</span> <span class="per negetive">(1.00%)</span> <span class="pe">PE: 10</span></P>
        </div>
        <div class="cord" id="one-g1"></div>
        <div class="cord" id="one-g2"></div>
        <div class="cord" id="one-g3"></div>
        <div class="title" id="two-title">
            <P class="index">{{$rightTitle}} <span class="val">100000</span> <span class="chg positive">100</span> <span class="per positive">(1.00%)</span> <span class="pe">PE: 10</span></P>
        </div>
        <div class="cord" id="two-g1"></div>
        <div class="cord" id="two-g2"></div>
        <div class="cord" id="two-g3"></div>
    </div>
    <script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>
    <script>
        $(document).ready(function () {
            var chart_bg_color = '#111111';
            var markerFillColor = 'rgba(247, 154, 53, 0.2)';
            var graphColor = '#F88407';

            var leftData = {!! json_encode($finalLeft) !!};
            var rightData = {!! json_encode($finalRight) !!};
            var count = 1;

            $.each(leftData, function (key, val) {  
                var id = 'one-g'+count;
                var id2 = 'two-g'+count;
                populate_data(id,val);
                populate_data(id2,rightData[key]);
                count++;
            });
            
            function populate_data(chart_id, data) 
            {
                Highcharts.stockChart(chart_id, {
                xAxis: {
                        gapGridLineWidth: 0,
                        lineColor: '#232323',
                    },
                yAxis: {
                        gridLineColor: '#232323'
                    },
                    credits: {
                        text: 'Righttracker',
                        href: 'http://www.righttracker.in',
                        enabled:false
                    },
                    rangeSelector: {
                        selected: 5,
                        enabled:false
                        
                    },
                    chart: {
                        backgroundColor: chart_bg_color,
                        selectionMarkerFill: markerFillColor,
                                    events: {
                                selection: function(event) {
                                    if (event.xAxis) {
                                        return false;
                                    }
                                }
                            },
                            enabled:false,
                            zoomType: 'x'
                        
                    },
                    navigator: {
                        enabled: false
                    },
                    scrollbar: {
                        enabled: false
                    },
                    plotOptions: {
                    series: {
                        allowPointSelect: true,
                        marker: {
                            states: {
                                select: {
                                    fillColor: 'red',
                                    lineWidth: 0
                                }
                            }
                        },
                        point: {
                        events: {
                            mouseOver: function () {
                            //   if(range_stat == 1 && start_nav == '') {
                            //     var row_index = parseInt(this.index) - 1;
                            //     selected_nav = data[row_index];
                            //     start_date = new Date(data[row_index][0]);
                            //     start_nav = parseFloat(selected_nav[1]);console.log(start_nav);console.log(selected_nav);
                            //   }
                            //   var current_index = parseFloat(this.y);
                            //   if(range_stat == 1 && start_nav != '' ) {
                            //     var diff = current_index - start_nav;
                            //     var change = ((diff/start_nav)*100).toFixed(2); //console.log(current_nav);console.log(start_nav);console.log(diff);console.log(change);
                            //     myDate = new Date(this.x);
                            //     $(date_id).text(start_date.getDate()+'-'+(start_date.getMonth()+1)+'-'+start_date.getFullYear()+ ' -> '+myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
                            //     $(value_id).text(' '+current_index.toFixed(2)+' ');
                            //     $(diff_id).html(' ('+change+'%)');
                            //     $(diff_id).removeClass('positive');
                            //     $(diff_id).removeClass('negetive');
                            //     if(change < 0) {
                            //       $(diff_id).addClass('negetive');
                            //     } else {
                            //       $(diff_id).addClass('positive');
                            //     }
                            //   } else {
                            //     myDate = new Date(this.x);
                            //     $(date_id).text(myDate.getDate()+'-'+(myDate.getMonth()+1)+'-'+myDate.getFullYear());
                            //     $(value_id).text(' '+current_index.toFixed(2)+' ');
                            //     $(diff_id).html('');


                            //   }
                            }
                        },    
                        },
                    }
                    },
                    series: [{
                        name: 'EOD',
                        type: 'line',
                        data: data,
                        gapSize: 0,
                        color : graphColor,
                        tooltip: {
                            valueDecimals: 2,
                            enabled: false,
                        },
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, 'rgba(248, 132, 7, 0.5)'],
                                [1, 'rgba(248, 132, 7, 0.0)'],

                            ]
                        },
                        threshold: null
                    }]
                });
            }
        });
    </script>
</body>
</html>