<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>RightTracker</title>

    
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    <script   src="https://code.jquery.com/jquery-3.2.1.js"
      integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
      crossorigin="anonymous"></script>


    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{url('css/index.css')}}" rel="stylesheet">
		<link href="{{url('css/schemes.css')}}" rel="stylesheet">
    <link href="{{url('css/global.css')}}" rel="stylesheet">
    <link href="{{url('css/benchmark.css')}}" rel="stylesheet">
	</head>

<body class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>
             
                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>
               
              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li> 
        </ul> 
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li> 
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle active_navbar_header"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container schemes-padding">
 
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding ">
  <div>
    <h2 class="headtxt">USD/INR    &nbsp 
        <span id="usd-span" class="nav_value">{{$usd['points']}}&nbsp</span>
        <?php if ($usd['Diff'] > 0): ?>
          <span class="positive" id="usd-current">{{$usd['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="usd-current">{{$usd['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($usd['Per'] > 0): ?>
          <span id="usd-per" class="positive">({{$usd['Per']}}%)</span>
          <i id="up" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="usd-per" class="negetive">({{$usd['Per']}}%)</span>
          <i id="down" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
    </h2>
    <h4>
      <span id="usd-date" class="benchmark_date"></span> 
      <span>| &nbsp    </span> 
      <span id="usd-rate"></span> 
      <span id="usd-diff" class="positive"></span>
      <span>
        <button class=" dropdown-toggle  graph_period pull-right" type="button" id="usd-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          3 Months
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="dropdownMenu1">
            <li class="usd-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="usd-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="usd-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="usd-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="usd-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="usd-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="usd-li" data-value="7"><a href="#">Max</a></li>
        </ul>
      </span>
    </h4>
  </div>
  <div class="card benchmark_card" id="usd-chart">

  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding ">
  <div>
    <h2 class="headtxt">GBP/INR   &nbsp 
        <span id="gbp-span" class="nav_value">{{$gbp['points']}}&nbsp</span>
        <?php if ($gbp['Diff'] > 0): ?>
          <span class="positive" id="gbp-current">{{$gbp['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="gbp-current">{{$gbp['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($gbp['Per'] > 0): ?>
          <span id="gbp-per" class="positive">({{$gbp['Per']}}%)</span>
          <i id="up" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="gbp-per" class="negetive">({{$gbp['Per']}}%)</span>
          <i id="down" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
    </h2>
    <h4 >
      <span id="gbp-date" class="benchmark_date"></span> 
      <span>| &nbsp    </span> 
      <span id="gbp-rate"></span> 
      <span id="gbp-diff" class="positive"></span>
      <span>
        <button class=" dropdown-toggle pull-right graph_period " type="button" id="gbp-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        3 Months
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="dropdownMenu1">
            <li class="gbp-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="gbp-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="gbp-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="gbp-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="gbp-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="gbp-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="gbp-li" data-value="7"><a href="#">Max</a></li>
        </ul>
      </span>
    </h4>
  </div>
  <div class="card benchmark_card" id="gbp-chart">
    <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding">
  <div>
    <h2 class="headtxt">SGD/INR   &nbsp 
        <span id="sgd-span" class="nav_value">{{$sgd['points']}}&nbsp</span>
        <?php if ($sgd['Diff'] > 0): ?>
          <span class="positive" id="sgd-current">{{$sgd['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="sgd-current">{{$sgd['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($sgd['Per'] > 0): ?>
          <span id="sgd-per" class="positive">({{$sgd['Per']}}%)</span>
          <i id="up" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="sgd-per" class="negetive">({{$sgd['Per']}}%)</span>
          <i id="down" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
    </h2>
    <h4 >
      <span id="sgd-date" class="benchmark_date"></span> 
      <span>| &nbsp    </span> 
      <span id="sgd-rate"></span> 
      <span id="sgd-diff" class="positive"></span>
      <span>
        <button class=" dropdown-toggle pull-right graph_period " type="button" id="sgd-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        3 Months
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="dropdownMenu1">
            <li class="sgd-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="sgd-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="sgd-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="sgd-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="sgd-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="sgd-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="sgd-li" data-value="7"><a href="#">Max</a></li>
        </ul>
      </span>
    </h4>
  </div>
  <div class="card benchmark_card" id="sgd-chart">
    <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding  ">
  <div>
    <h2 class="headtxt">YEN/INR   &nbsp
      <span id="yen-span" class="nav_value">{{$yen['points']}}&nbsp</span>
        <?php if ($yen['Diff'] > 0): ?>
          <span class="positive" id="yen-current">{{$yen['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="yen-current">{{$yen['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($yen['Per'] > 0): ?>
          <span id="yen-per" class="positive">({{$yen['Per']}}%)</span>
          <i id="up" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="yen-per" class="negetive">({{$yen['Per']}}%)</span>
          <i id="down" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
    </h2>
    <h4 >
      <span id="yen-date" class="benchmark_date"></span> 
      <span>| &nbsp    </span> 
      <span id="yen-rate"></span> 
      <span id="yen-diff" class="positive"></span>
      <span>
        <button class=" dropdown-toggle pull-right graph_period " type="button" id="yen-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        3 Months
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="dropdownMenu1">
            <li class="yen-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="yen-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="yen-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="yen-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="yen-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="yen-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="yen-li" data-value="7"><a href="#">Max</a></li>
        </ul>
      </span>
    </h4>
  </div>
  <div class="card benchmark_card" id="yen-chart">
    <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding  ">
  <div>
    <h2 class="headtxt">CHF/INR   &nbsp
      <span id="chf-span" class="nav_value">{{$chf['points']}}&nbsp</span>
        <?php if ($chf['Diff'] > 0): ?>
          <span class="positive" id="chf-current">{{$chf['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="chf-current">{{$chf['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($chf['Per'] > 0): ?>
          <span id="chf-per" class="positive">({{$chf['Per']}}%)</span>
          <i id="up" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="chf-per" class="negetive">({{$chf['Per']}}%)</span>
          <i id="down" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
    </h2>
    <h4 >
      <span id="chf-date" class="benchmark_date"></span> 
      <span>| &nbsp    </span> 
      <span id="chf-rate"></span> 
      <span id="chf-diff" class="positive"></span>
      <span>
        <button class=" dropdown-toggle pull-right graph_period " type="button" id="chf-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        3 Months
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="dropdownMenu1">
            <li class="chf-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="chf-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="chf-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="chf-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="chf-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="chf-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="chf-li" data-value="7"><a href="#">Max</a></li>
        </ul>
      </span>
    </h4>
  </div>
  <div class="card benchmark_card" id="chf-chart">
    <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  benchmar_padding  ">
  <div>
    <h2 class="headtxt">EUR/INR   &nbsp
      <span id="eur-span" class="nav_value">{{$eur['points']}}&nbsp</span>
        <?php if ($eur['Diff'] > 0): ?>
          <span class="positive" id="eur-current">{{$eur['Diff']}}&nbsp</span>
          <?php else: ?>
          <span class="negetive" id="eur-current">{{$eur['Diff']}}&nbsp</span>
        <?php endif ?>
        <?php if ($eur['Per'] > 0): ?>
          <span id="eur-per" class="positive">({{$eur['Per']}}%)</span>
          <i id="up" class="material-icons positive upward_arrow">arrow_drop_up</i>
          <?php else: ?>
          <span id="eur-per" class="negetive">({{$eur['Per']}}%)</span>
          <i id="down" class="material-icons negetive upward_arrow">arrow_drop_down</i>
        <?php endif ?>
    </h2>
    <h4 >
      <span id="eur-date" class="benchmark_date"></span> 
      <span>| &nbsp    </span> 
      <span id="eur-rate"></span> 
      <span id="eur-diff" class="positive"></span>
      <span>
        <button class=" dropdown-toggle pull-right graph_period " type="button" id="eur-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        3 Months
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu pull-right dropdown_period" aria-labelledby="dropdownMenu1">
            <li class="eur-li" data-value="1"><a href="#">1 Day</a></li>
            <li class="eur-li" data-value="2"><a href="#">5 Days</a></li>
            <li class="eur-li" data-value="3"><a href="#">1 Month</a></li>
            <li class="eur-li" data-value="4"><a href="#">3 Months</a></li>
            <li class="eur-li" data-value="5"><a href="#">1 Year</a></li>
            <li class="eur-li" data-value="6"><a href="#">5 Year</a></li>
            <li class="eur-li" data-value="7"><a href="#">Max</a></li>
        </ul>
      </span>
    </h4>
  </div>
  <div class="card benchmark_card" id="eur-chart">
    <!-- <img class="graph_spacing padding-lr-zero" src="img/graph.svg"> -->
  </div>
</div>

</div>
<!-- ajax loader -->
<div class="overlay" style="display: none;">
  <div class="loader-outer">
      <div class="loader"></div>
  </div>
</div>
<script type="text/javascript" src="{{url('js/forex.js')}}"></script>
<script type="text/javascript" src ="{{url('js/highstock.js')}}"></script>
  
</body>
</html>