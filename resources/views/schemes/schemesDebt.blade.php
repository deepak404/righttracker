<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


		<title>RightTracker</title>
		<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script   src="https://code.jquery.com/jquery-3.2.1.js"  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="  crossorigin="anonymous"></script>


      <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <link href="css/global.css" rel="stylesheet">
    <link href="css/schemes.css" rel="stylesheet">
    <link href="css/schemes-popup.css" rel="stylesheet">
      <link href="css/schemes-debt.css" rel="stylesheet">
	</head>

<body class="body">
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/nav-logo.svg " class="nav-logo">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-header">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle "  data-toggle="dropdown">Equity</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
            <li><a href="/diversified">Diversified Fund</a></li>
            <li><a href="/largecap">Large Cap</a></li>
            <li><a href="/smallmidcap">Small & Mid Cap </a></li>
            <li><a href="/elss">ELSS</a></li>

                        <li><a href="/arbitrage">Arbitrage</a></li>
            <li><a href="/sectoral">Sectoral</a></li>
            </ul>
          </li>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Debt</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/liquid">Liquid Fund</a></li>
              <li><a href="/fmp">FMP</a></li>
              <li><a href="/longterm">Long term</a></li>
              <li><a href="/shortterm">Short term</a></li>
              <li><a href="/ultrashortterm">Ultra short term</a></li>
              <li><a href="/floatingrate">Floating rate</a></li>
              <li><a href="/giltshortterm">Gilt Short Term</a></li>
              <li><a href="/giltlongterm">Gilt Long Term</a></li>
              <li><a href="/mip">MIP</a></li>

              <li><a href="/creditopps">Credit Oppt.</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown  dropdown_nav">
            <a class="dropdown-toggle" data-toggle="dropdown" >Hybrid</a>
            <ul class="dropdown-menu card dropdown-menu_nav">
              <li><a href="/balanced">Balanced</a></li>
              <li><a href="/ess">ESS</a></li>
            </ul>
          </li>
        </ul>
          <li><a href="/benchmark">Benchmark</a></li>
          <li><a href="/crisil">Crisil</a></li>
          <li><a href="/comparison" >Comparison</a></li>
          <li><a href="/policy" >Policy tracking</a></li>
          <li class="dropdown  dropdown_nav ">
            <a class="dropdown-toggle active_navbar_header"  data-toggle="dropdown">More</a>
            <ul class="dropdown-menu card dropdown-menu_nav ">
              <li><a href="/globalIndex" >Global index</a></li>
              <li><a href="/forex">Forex</a></li>
              <li><a href="/commodities">Commodities</a></li>
              <li><a href="/schemes" >Schemes</a></li>
                            <li><a href="/rmadmin" >RM Dashboard</a></li>
              <li><a href="/live_tv" >Live TV</a></li> <li><a href="/tvmodule" >TV Module</a></li>
              <li><a href="http://139.59.9.237" >Attendance System</a></li>
            </ul>
          </li>
        </ul>
        <ul class="navbar-nav list-inline pull-right">
          <li>
            <a href="logout" class="pull-right logout" >logout<i class="material-icons logout-icon">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container schemes-padding">
  <div class="schemes-fixed">
  <h2 class="headtxt">Schemes</h2>
  <ul class="edit_options list-inline">
    <li><a href="#"><i class="material-icons edit_icon edit_icon_active">mode_edit</i></a></li>
    <li><a href="#"><i class="material-icons delete_icon delete_icon_active">delete</i></a></li>
    <li><button class="add_button" id="myBtn"><i class="material-icons add_icon">add</i> Add Scheme</button></li>
  </ul>
  <ul class="fund-period list-inline">
      <li><a href="/schemes">Equity</a></li>
        <li><a href="/schemesDebt" class="active_fund_period" >Debt</a></li>
        <li><a href="/schemesBalanced">Hybrid</a></li>
      </ul>

      <hr>
    </div>
  </div>



<div class="container card ">
  <div class="table-wrapper" id="scroll-customize">
    <table class="table table-responsive table-hover">
      <thead>
        <tr>
          <th class="header-width">
            <div class="col-xs-2 padding-lr-zero"></div><div class="col-lg-10 padding-lr-zero  schemes head_schemes_padding">Schemes</div></th>
          <th class="scheme_code">Scheme Code </th>
          <th class="small_cap">Classification</th>
          <th class="mid_cap">% > AA Rated</th>
          <th class="large_cap">Average Maturity</th>
          <th class="large_cap">Modified Duration</th>
          <th class="large_cap">% of Govt Securities</th>
          <th class="assert_size">Asset size  Cr.</th>
        </tr>
      </thead>

      <tbody>
        @foreach($debt as $value)
        <tr>
          <td>
            <div class="col-xs-2 ">
              <i class="material-icons select_state list_radio" data-schemes="{{$value['scheme_code']}}" data-funds="{{$value['fund_type']}}">radio_button_checked</i>
            </div>
            <div class="col-lg-10 padding-lr-zero schemes_spacing">{{$value['scheme_name']}}</div>
          </td>
          <td>{{$value['scheme_code']}}</td>
          <td class="positive">{{$value['classification']}}</td>
          <td class="positive" >{{$value['AA_return']}}</td>
          <td class="positive">{{$value['avg_mat']}}</td>
          <td class="positive"> {{$value['mod_duration']}}</td>
          <td class="positive"> {{$value['gov_sec']}}</td>
          <td>{{$value['asset_size']}}</td>
        </tr>
        @endforeach
        <?php if (count($debt) == 0): ?>
          <tr>
            <td>No Data Found.</td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
</div>
<!-- Add Scheme Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content col-lg-12">
      <div class="modal-header col-lg-12">
        <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
        <h4 class="modal-title"> Add Scheme Details</h4>
      </div>
      <div class="modal-body col-lg-12">
        <form id="add-scheme-form">
          <div class="forminput">

          <div class=" dropdown col-md-6  col-lg-6 padding-lr-zero" id="amc-div">
              <select name="amc_name" required id="amc_name">
                <option value="" disabled selected>AMC Name</option>
                <?php foreach ($amcs as $value): ?>
                <option value="{{$value}}">{{$value}}</option>
                <?php endforeach ?>
              </select>
              <h5 class="add_amc pull-right" id="add-amc">Add AMC</h5>
          </div>

            <div class="form_select1 col-md-6 col-lg-6 padding-lr-zero">
              <select name="fund_type" required id="fund_type">
              <option value="" disabled selected>Fund Type</option>
              <option value="1">Equity</option>
              <option value="2">Debt</option>
              <option value="3">Balanced</option>
            </select>
            </div>

            <div class="form_select dropdown col-md-6 col-lg-6 padding-lr-zero">
              <select name="classification" required id="classification">
              <option value="" disabled selected>Classification</option>
              <option value="1">small cap</option>
              <option value="2">mid cap</option>
              <option value="3">large cap</option>
            </select>
            </div>

            <div class="group col-md-6 col-lg-6 padding-lr-zero">
              <input type="text" name="scheme_name" required id="scheme_name">
              <span class="bar"></span>
              <label>Scheme Name</label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero">
              <input type="text" name="scheme_code" required id="scheme_code">
              <span class="bar"></span>
              <label>Scheme Code   </label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero" id="field_one">
              <input type="text" name="small_cap" required id="small_cap">
              <span class="bar"></span>
              <label>% Small Cap</label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero" id="field_two">
              <input type="text" name="mid_cap" required id="mid_cap">
              <span class="bar"></span>
              <label>% Mid Cap</label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero" id="field_three">
              <input type="text" name="larg_cap" required id="larg_cap">
              <span class="bar"></span>
              <label>% Large Cap</label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero">
              <input type="text" name="asset_size" required id="asset_size">
              <span class="bar"></span>
              <label>Asset size</label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero">
              <input type="text" name="exit_load" required id="exit_load">
              <span class="bar"></span>
              <label>Exit load</label>
            </div>
            <div class="group fileupload-grp col-md-6 col-lg-6 padding-lr-zero ">
                <input type="file" class="file_input" name="csv_file" id="file_upload">
                <label for="file_upload" id="file_uploadevent" ><span class="label-upload">CSV File Upload</span><i class="material-icons pull-right file_uploadicon">file_upload</i></label>
            </div>
            <div class="group col-md-6 col-lg-6 padding-lr-zero" id="field_four">

            </div>

            <div class="col-lg-12 padding-lr-zero" id="submit_btn">
              <button type="submit" class="btn button_popup btn-primary" data-val="equity" id="add_scheme">Add Scheme</button>
              <span class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 composition">Total Composition:<span>100%</span></span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Edit Scheme Modal -->
    <div class="modal fade" id="debtedit-modal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content col-lg-12">
          <div class="modal-header col-lg-12">
            <h4 class="modal-title">Edit Scheme Details</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
          </div>
          <div class="modal-body col-lg-12">
            <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="scheme-edit">
              <div class="forminput">
                <input type="hidden" name="scheme_code" id="schemeCode-edit" value="">
                <input type="hidden" name="fund_type" id="fundType-edit" value="">
                <div class="group  col-md-6  col-lg-6 padding-lr-zero">
                  <input type="text" name="scheme_name" id="schemeName-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Scheme Name</label>
                </div>
                <div class="group dropdown col-md-6  col-lg-6 padding-lr-zero" >
                  <select name="classification" required="" id="classification-edit">
                    <option value="liquid">Liquid</option>
                    <option value="lp">LP</option>
                    <option value="fmp">FMP</option>
                    <option value="long term">Long term</option>
                    <option value="short term">Short term</option>
                    <option value="ultra short term">Ultra Short term</option>
                    <option value="floating rate">Floating rate</option>
                    <option value="credit opps">Credit Opps.</option>
                    <option value="mip">MIP</option>
                    <option value="hybrid">Hybrid</option>
										<option value="overnight">Over Night fund</option>
										<option value="gilt short term">Gilt Short term</option>
                    <option value="gilt long term">Gilt Long term</option>
                  </select>
                </div>
                <div class="group  col-md-6  col-lg-6 padding-lr-zero">
                  <input type="text" name="avg_mat" id="avgMat-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Average Maturity</label>
                </div>
                <div class="group  col-md-6  col-lg-6 padding-lr-zero">
                  <input type="text" name="mod_duration" id="modDuration-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Modification Duration</label>
                </div>
                <div class="group col-md-6   col-lg-6 padding-lr-zero">
                  <input type="text" name="gov_sec" id="govSec-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>% of Gov.Securities</label>
                </div>
                <div class="group col-md-6  col-md-6 col-lg-6 padding-lr-zero">
                  <input type="text" name="AA_return" id="AA-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>% AA Rate</label>
                </div>
                <div class="group col-md-6  col-md-6 col-lg-6 padding-lr-zero">
                  <input type="text" name="asset_size" id="asset-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Asset size</label>
                </div>

                <div class="group  col-md-6   col-lg-6 padding-lr-zero">
                  <input type="text" name="exit_load" id="exitLoad-edit" required>
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Exit load</label>
                </div>

                <div class="col-lg-12 padding-lr-zero">
                  <button type="submit" class="btn button_popup btn-primary ">Update</button>
                  <span class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 composition">Total Composition:<span>100%</span></span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
<!-- sucess model -->
    <div id="sucess-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="card popup-card center-block">
          <img class="popup-img center-block" src="img/sucessfull.svg">
          <h3 class="text-center sucessfull sucess-msg" >Scheme added Sucessfully</h3>
          <button class="center-block popup-button alert-btn" >Okay</button>
        </div>
      </div>
    </div>
<!-- Failer Model -->
    <div id="fail-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="card popup-card center-block">
          <img class="popup-img center-block" src="img/failed.svg">
          <h3 class="text-center failed sucess-msg">Scheme added Failed</h3>
          <button class="center-block popup-button alert-btn">Okay</button>
        </div>
      </div>
    </div>

<script type="text/javascript" src="js/addscheme.js"></script>


</body>
</html>
