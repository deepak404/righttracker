<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('scheme_code');
            $table->string('scheme_name');
            $table->string('amc_name');
            $table->string('classification');
            $table->string('fund_type');
            $table->double('AA_return');
            $table->double('avg_mat');
            $table->double('mod_duration');
            $table->double('gov_sec');
            $table->double('asset_size');
            $table->string('exit_load');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debts');
    }
}
