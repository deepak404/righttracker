<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_code');
            $table->string('scheme_name');
            $table->string('amc_name');
            $table->string('fund_type');
            $table->string('fund_category');
            $table->decimal('debt_percent')->nullable();
            $table->decimal('avg_maturity')->nullable();
            $table->decimal('aa_rate')->nullable();
            $table->decimal('exit_load');
            $table->integer('is_wish_list');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheme_details');
    }
}
