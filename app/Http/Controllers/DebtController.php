<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\debt;
use Carbon\Carbon;
use DB;

class DebtController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        // \Debugbar::disable();
    }
    public function liquid()
	{
		$schemeDetails = $this->getDebtData('liquid');
		return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Liquid Fund']);
	}

    public function longterm()
    {
        $schemeDetails = $this->getDebtData('long term');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Long term']);
    }

    public function shortterm()
    {
        $schemeDetails = $this->getDebtData('short term');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Short term']);
    }

    public function ultrashortterm()
    {
        $schemeDetails = $this->getDebtData('ultra short term');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Ultra short term']);
    }

    public function floatingrate()
    {
        $schemeDetails = $this->getDebtData('floating rate');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Floating rate']);
    }

    public function creditopps()
    {
        $schemeDetails = $this->getDebtData('credit opps');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Credit Oppt.']);
    }
    public function mip()
    {
        $schemeDetails = $this->getDebtData('mip');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'MIP']);
    }
    public function hybrid()
    {
        $schemeDetails = $this->getDebtData('hybrid');
        return view('debt.debt',['schemeData'=>$schemeDetails]);
    }
    public function giltshortterm()
    {
        $schemeDetails = $this->getDebtData('gilt short term');
        return view('debt.debt',['schemeData'=>$schemeDetails,'name'=>'Gilt Short term']);
    }
    public function giltlongterm()
    {
        $schemeDetails = $this->getDebtData('gilt long term');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'Gilt Long term']);
    }
    public function fmp()
    {
        $schemeDetails = $this->getDebtData('fmp');
        return view('debt.debt',['schemeData'=>$schemeDetails, 'name'=>'FMP']);
    }

    public function getDebtData($classification)
    {
        $Dr = Carbon::now()->subDays(2)->toDateString();
        $Wr = Carbon::now()->subDays(6)->toDateString();
        $Mr = Carbon::now()->subMonths(1)->toDateString();
        $Yr = Carbon::now()->subMonths(12)->toDateString();
        $five_Yr = Carbon::now()->subMonths(60)->toDateString();
        $schemes = debt::where('classification', $classification)->get()->toArray();
        $scheme_info = [];
        foreach ($schemes as $scheme) {
            $DataArray = [];
            $day_val = 0;
            $week_val = 0;
            $month_val = 0;
            $year_val = 0;
            $five_year_val = 0;

            $DataArray['scheme_name'] = $scheme['scheme_name'];
            $DataArray['exit_load'] = $scheme['exit_load'];
            $DataArray['brk'] = $scheme['brk'];
            $DataArray['avg_mat'] = $scheme['avg_mat'];
            $DataArray['mod_duration'] = $scheme['mod_duration'];
            $DataArray['AA_return'] = $scheme['AA_return'];
            $DataArray['gov_sec'] = $scheme['gov_sec'];
            $DataArray['asset_size'] = $scheme['asset_size'];
            $DataArray['scheme_code'] = $scheme['scheme_code'];

            $othersVal = 100 - ($scheme['AA_return'] + $scheme['gov_sec']);
            if ($othersVal < 0) {
                $othersVal = 0;
            }
            $DataArray['others'] = round($othersVal, 2);
            $DataArray['total'] = $scheme['AA_return'] + $scheme['gov_sec'] + $othersVal;
            $amc_name = $scheme['amc_name'];
            $scheme_code = $scheme['scheme_code'];
            $historicData = DB::table($amc_name)->where('scheme_code',$scheme_code)
                                    ->orderBy('date','dese')
                                    ->get()->toArray();
            $DataArray['todays_nav'] = $historicData[0]->nav;
            
            foreach ($historicData as $pickedData) {
                if ($five_Yr >= $pickedData->date) {
                    if ($five_year_val == 0) {
                        $five_year_val = 1;
                        // $five_year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $five_year_return = (pow(($historicData[0]->nav/$pickedData->nav), (1/5))-1)*100;
                        
                        $DataArray['five_year_return'] = round($five_year_return, 2);
                    }
                }elseif ($Yr >= $pickedData->date) {
                    if ($year_val == 0) {
                        $year_val = 1;
                        $year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['year_return'] = round($year_return, 2);
                    }
                }elseif ($Mr >= $pickedData->date) {
                    if ($month_val == 0) {
                        $month_val = 1;
                        $month_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['month_return'] = round($month_return, 2);
                    }
                }elseif ($Wr >= $pickedData->date) {
                    if ($week_val == 0) {
                        $week_val = 1;
                        $week_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['week_return'] = round($week_return, 2);
                    }
                }elseif ($Dr >= $pickedData->date) {
                    if ($historicData[0]->date > $pickedData->date) {
                        if ($day_val == 0) {
                            $day_val = 1;
                            $day_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                            $DataArray['day_return'] = round($day_return, 2);
                        }
                    }
                }
            }
            if (empty($DataArray['day_return'])) {
                $DataArray['day_return'] = 0;
            }
            array_push($scheme_info, $DataArray);
        }
        usort($scheme_info, function($a, $b) {
            return $b['day_return'] <=> $a['day_return'];
        });
        return $scheme_info;
    }
}
