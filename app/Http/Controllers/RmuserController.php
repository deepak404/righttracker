<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\equity;
use App\debt;
use App\balanced;
use App\RmInfo;

class RmuserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('RmUser');
    }

    public function index()
    {
    	$user_id = \Auth::id();
    	$amc = RmInfo::where('user_id',$user_id)->get()->toArray();
    	$amc_name = $amc[0]['amc_name'];
    	$equity = equity::where('amc_name',$amc_name)->get()->toArray();
    	// dd($equity);
    	return view('rmuser.rmequity',['rminfo'=>$amc[0],'equity'=>$equity]);

    }

    public function debt()
    {
    	$user_id = \Auth::id();
    	$amc = RmInfo::where('user_id',$user_id)->get()->toArray();
    	$amc_name = $amc[0]['amc_name'];
    	$debt = debt::where('amc_name',$amc_name)->get()->toArray();
    	// dd($debt);
    	return view('rmuser.rmdebt',['rminfo'=>$amc[0],'debt'=>$debt]);
    }

    public function hybrid()
    {
    	$user_id = \Auth::id();
    	$amc = RmInfo::where('user_id',$user_id)->get()->toArray();
    	$amc_name = $amc[0]['amc_name'];
    	$balanced = balanced::where('amc_name',$amc_name)->get()->toArray();
    	// dd($balanced);
    	return view('rmuser.rmhybrid',['rminfo'=>$amc[0],'balanced'=>$balanced]);
    }

    public function compState()
    {
        $amc_name = RmInfo::where('user_id',\Auth::id())->value('amc_name');

        $equity = equity::where('amc_name',$amc_name)->get()->toArray();
        $eq_count = count($equity);
        $debt = debt::where('amc_name',$amc_name)->get()->toArray();
        $debt_count = count($debt);
        $balanced = balanced::where('amc_name',$amc_name)->get()->toArray();
        $bal_count = count($balanced);
        $total = $eq_count+$debt_count+$bal_count;

        $equity_c = equity::where('amc_name',$amc_name)->where('state',1)->get()->toArray();
        $eq_com = count($equity_c);
        $debt_c = debt::where('amc_name',$amc_name)->where('state',1)->get()->toArray();
        $db_com = count($debt_c);
        $balanced_c = balanced::where('amc_name',$amc_name)->where('state',1)->get()->toArray();
        $bal_com = count($balanced_c);
        $comp = $eq_com+$db_com+$bal_com;

        $final = round(($comp/$total)*100);
        return $final;

    }

    public function addEquity(Request $request)
    {
    	$scheme_code = $request['scheme_code'];
    	$asset_size = $request['asset'];
    	$no_year = $request['no_year'];
    	$percentage = $request['percentage'];
    	$small_cap = $request['small_cap'];
    	$mid_cap = $request['mid_cap'];
        $brk = $request['brk'];
    	$large_cap = $request['larg_cap'];

    	$sum = $small_cap + $mid_cap + $large_cap;
    	if ($sum > 100) {
    		return response()->json(['msg'=>0, 'info'=>'Total has to be 100%.']);
    	}
        if ($no_year == 0) {
            $exit_load = 0;
        }else{
    		$exit_load = $no_year.'/'.$percentage;
    	}

    	$update = equity::where('scheme_code',$scheme_code)
    					->update(['small_cap'=>$small_cap,
    							'mid_cap'=>$mid_cap,
    							'large_cap'=>$large_cap,
    							'exit_load'=>$exit_load,
    							'asset_size'=>$asset_size,
    							'state'=>1,
                                'brk'=>$brk]);
    	if ($update) {
            $progress = $this->compState();
            RmInfo::where('user_id',\Auth::id())->update(['rm_progress'=>$progress]);
    		return response()->json(['msg'=>1]);
    	}else{
    		return response()->json(['msg'=>0, 'info'=>'Data storing has some problem.']);
    	}
    }

    public function addHybrid(Request $request)
    {
    	$scheme_code = $request['scheme_code'];
    	$asset_size = $request['asset'];
    	$no_year = $request['no_year'];
    	$percentage = $request['percentage'];
    	$small_cap = $request['small_cap'];
    	$mid_cap = $request['mid_cap'];
    	$large_cap = $request['larg_cap'];
        $brk = $request['brk'];
    	$debt = $request['debt'];

    	$sum = $small_cap + $mid_cap + $large_cap + $debt;
    	if ($sum > 100) {
    		return response()->json(['msg'=>0, 'info'=>'Total has to be 100%.']);
    	}
    	if ($no_year == 0) {
    		$exit_load = 0;
    	}else{
    		$exit_load = $no_year.'/'.$percentage;
    	}

    	$update = balanced::where('scheme_code',$scheme_code)
    					->update(['small_cap'=>$small_cap,
    							'mid_cap'=>$mid_cap,
    							'large_cap'=>$large_cap,
    							'debt'=>$debt,
    							'exit_load'=>$exit_load,
    							'asset_size'=>$asset_size,
    							'state'=>1,
                                'brk'=>$brk]);
    	if ($update) {
            $progress = $this->compState();
            RmInfo::where('user_id',\Auth::id())->update(['rm_progress'=>$progress]);
    		return response()->json(['msg'=>1]);
    	}else{
    		return response()->json(['msg'=>0, 'info'=>'Data storing has some problem.']);
    	}
    }

    public function addDebt(Request $request)
    {
    	$scheme_code = $request['scheme_code'];
    	$asset_size = $request['asset'];
    	$no_year = $request['no_year'];
    	$percentage = $request['percentage'];
    	$avg_mat = $request['avg_mat'];
    	$mod_dur = $request['mod_dur'];
    	$gov_sec = $request['gov_sec'];
        $aa_rate = $request['aa_rate'];
    	$brk = $request['brk'];

		$sum = $gov_sec + $aa_rate;
    	if ($sum > 100) {
    		return response()->json(['msg'=>0, 'info'=>'Total has to be 100%.']);
    	}
    	if ($no_year == 0) {
    		$exit_load = 0;
    	}else{
    		$exit_load = $no_year.'/'.$percentage;
    	}

    	$update = debt::where('scheme_code',$scheme_code)
    					->update(['AA_return'=>$aa_rate,
    							'avg_mat'=>$avg_mat,
    							'mod_duration'=>$mod_dur,
    							'gov_sec'=>$gov_sec,
    							'exit_load'=>$exit_load,
    							'asset_size'=>$asset_size,
    							'state'=>1,
                                'brk'=>$brk]);
    	if ($update) {
            $progress = $this->compState();
            RmInfo::where('user_id',\Auth::id())->update(['rm_progress'=>$progress]);
    		return response()->json(['msg'=>1]);
    	}else{
    		return response()->json(['msg'=>0, 'info'=>'Data storing has some problem.']);
    	}
    }
}
