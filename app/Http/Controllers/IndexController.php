<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistoricBse;
use App\HistoricMidcap;
use App\HistoricNse;
use App\HistoricSmallcap;
use Carbon\Carbon;
use App\HistoricChf;
use App\HistoricGbp;
use App\HistoricSgd;
use App\HistoricUsd;
use App\HistoricYen;
use App\Chf;
use App\Eur;
use App\DowJones;
use App\Ftse;
use App\Hansen;
use App\Nacdaq;
use App\Nikkei;
use App\Sse;
use DateTimeZone;
use App\Dax;

class IndexController extends Controller
{
      public function __construct()
      {
        $this->middleware('auth');
        // \Debugbar::disable();
      }

   	public function benchmark()
   	{
         $bse = HistoricBse::orderBy('date','desc')->get()->toArray();
         $smallcap = HistoricSmallcap::orderBy('date','desc')->get()->toArray();
         $midcap = HistoricMidcap::orderBy('date','desc')->get()->toArray();
         $nse = HistoricNse::orderBy('date','desc')->get()->toArray();

         foreach ($bse as $selected) {
            if ($bse[0]['date'] > $selected['date']) {
               $bseDiff = $bse[0]['close'] - $selected['close'];
               $bsePer = ($bseDiff / $bse[0]['close'])*100;
               $bse_val = ['points' => round($bse[0]['close'], 2),
                           'Diff' => round($bseDiff, 2),
                           'Per' => round($bsePer, 2)];
              break;
            }
         }
         foreach ($nse as $selected) {
           if ($nse[0]['date'] > $selected['date']) {
             $nseDiff = $nse[0]['close'] - $selected['close'];
             $nsePer = ($nseDiff / $nse[0]['close'])*100;
             $nse_val = ['points' => round($nse[0]['close'], 2),
                         'Diff' => round($nseDiff, 2),
                         'Per' => round($nsePer, 2)];
            break;
           }
         }
         foreach ($smallcap as $selected) {
           if ($smallcap[0]['date'] > $selected['date']) {
             $smallcapDiff = $smallcap[0]['close'] - $selected['close'];
             $smallcapPer = ($smallcapDiff / $smallcap[0]['close'])*100;
             $smallcap_val = ['points' => round($smallcap[0]['close'], 2),
                              'Diff' => round($smallcapDiff, 2),
                              'Per' => round($smallcapPer, 2)];
             break;
           }
         }
         foreach ($midcap as $selected) {
           if ($midcap[0]['date'] > $selected['date']) {
             $midcapDiff = $midcap[0]['close'] - $selected['close'];
             $midcapPer = ($midcapDiff / $midcap[0]['close'])*100;
             $midcap_val = ['points' => round($midcap[0]['close'], 2),
                            'Diff' => round($midcapDiff, 2),
                            'Per' => round($midcapPer, 2)];
             break;
           }
         }


   		return view('benchmark.benchmark',['sensex'=>$bse_val,'nifty'=>$nse_val,'smallcap'=>$smallcap_val,'midcap'=>$midcap_val]);
   	}


      public function getIndex(Request $request)
      {
        $months;
        $scheme_code;
            $performance_data = array();
            $duration = $request['duration'];
            $index = $request['index'];
            $index_range_date = '';
            $day_data = false;
            switch($duration) {
                case '1':
                {
                    $index_range_date = '1';
                    break;
                }
                case '2':
                {
                    //calculating the range date to prepare data
                    $index_range_date = '2';
                    // $index_range_date = Carbon::now()->subDays(6)->toDateString();
                    break;
                }
                case '4':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                    break;
                }
                case '5':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                }
                case '6':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                case '7':
                {
                    //calculating the range date to prepare data
                    $index_range_date = '';//Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                case '8':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(24)->toDateString();
                    break;
                }
                default :
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                }
            }

            $current_date = date('Y-m-d').' 00:00:00';
            switch($index){
                case 'sensex':
                {
                  if ($index_range_date == '1') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5EBSESN&range=1d&interval=1m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date == '2') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5EBSESN&range=5d&interval=1h&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricBse::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  } else{
                     $index_details = HistoricBse::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'nifty':
                {
                  if ($index_range_date == '1') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5ENSEI&range=1d&interval=1m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date == '2') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5ENSEI&range=5d&interval=1h&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricNse::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = HistoricNse::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'smallcap':
                {
                  if ($index_range_date == '1') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-SMLCAP.BO&range=1d&interval=1m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date == '2') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-SMLCAP.BO&range=5d&interval=1h&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricSmallcap::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = HistoricSmallcap::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'midcap':
                {
                  if ($index_range_date == '1') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-MIDCAP.BO&range=1d&interval=1m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }if ($index_range_date == '2') {
                      $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-MIDCAP.BO&range=5d&interval=1h&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricMidcap::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = HistoricMidcap::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
            }

            if(!empty($index_details)) {
                foreach($index_details as $index_detail) {

                      $performance_data[] = array(strtotime($index_detail['date'])*1000,round($index_detail['close'], 2));
                    }
                return response()->json(['msg' => '1', 'data' => $performance_data]);
            } else {
                return response()->json(['msg' => '0']);
            }
   }

   public function forex()
   {
      $usd = HistoricUsd::orderBy('date','desc')->get()->toArray();
      $sgd = HistoricSgd::orderBy('date','desc')->get()->toArray();
      $yen = HistoricYen::orderBy('date','desc')->get()->toArray();
      $gbp = HistoricGbp::orderBy('date','desc')->get()->toArray();
      $chf = Chf::orderBy('date','desc')->get()->toArray();
      $eur = Eur::orderBy('date','desc')->get()->toArray();

      $usdDiff = $usd[0]['exchange_value'] - $usd[1]['exchange_value'];
      $usdPer = ($usdDiff / $usd[0]['exchange_value'])*100;
      $usd_val = ['points' => round($usd[0]['exchange_value'], 2),
                  'Diff' => round($usdDiff, 2),
                  'Per' => round($usdPer, 2)];

      $gbpDiff = $gbp[0]['exchange_value'] - $gbp[1]['exchange_value'];
      $gbpPer = ($gbpDiff / $gbp[0]['exchange_value'])*100;
      $gbp_val = ['points' => round($gbp[0]['exchange_value'], 2),
                  'Diff' => round($gbpDiff, 2),
                  'Per' => round($gbpPer, 2)];

      $sgdDiff = $sgd[0]['exchange_value'] - $sgd[1]['exchange_value'];
      $sgdPer = ($sgdDiff / $sgd[0]['exchange_value'])*100;
      $sgd_val = ['points' => round($sgd[0]['exchange_value'], 2),
                       'Diff' => round($sgdDiff, 2),
                       'Per' => round($sgdPer, 2)];

      $yenDiff = $yen[0]['exchange_value'] - $yen[1]['exchange_value'];
      $yenPer = ($yenDiff / $yen[0]['exchange_value'])*100;
      $yen_val = ['points' => round($yen[0]['exchange_value'], 2),
                     'Diff' => round($yenDiff, 2),
                     'Per' => round($yenPer, 2)];

      $chfDiff = $chf[0]['exchange_value'] - $chf[1]['exchange_value'];
      $chfPer = ($chfDiff / $chf[0]['exchange_value'])*100;
      $chf_val = ['points' => round($chf[0]['exchange_value'], 2),
                     'Diff' => round($chfDiff, 2),
                     'Per' => round($chfPer, 2)];

      $eurDiff = $eur[0]['exchange_value'] - $eur[1]['exchange_value'];
      $eurPer = ($eurDiff / $eur[0]['exchange_value'])*100;
      $eur_val = ['points' => round($eur[0]['exchange_value'], 2),
                     'Diff' => round($eurDiff, 2),
                     'Per' => round($eurPer, 2)];
      return view('forex.forex',['usd'=>$usd_val,'sgd'=>$sgd_val,'yen'=>$yen_val,'gbp'=>$gbp_val,'chf'=>$chf_val,'eur'=>$eur_val]);
   }

   public function getForex(Request $request)
      {

            $performance_data = array();
            $duration = $request['duration'];
            $index = $request['index'];
            $index_range_date = '';
            $day_data = false;
            switch($duration) {
                case '1':
                {
                    $index_range_date = '1';
                    break;
                }
                case '2':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subDays(6)->toDateString();
                    break;
                }
                case '4':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                    break;
                }
                case '5':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                }
                case '6':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                case '7':
                {
                    //calculating the range date to prepare data
                    $index_range_date = '';//Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                default :
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                }
            }

            // $current_date = date('Y-m-d').' 00:00:00';
            switch($index){
                case 'usd':
                {
                  if ($index_range_date == '1') {
                       $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=INR=X&range=1d&interval=5m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = historicUsd::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = historicUsd::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'sgd':
                {
                  if ($index_range_date == '1') {
                       $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=SGDINR=X&range=1d&interval=5m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricSgd::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = HistoricSgd::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'yen':
                {
                  if ($index_range_date == '1') {
                       $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=JPYINR%3DX&range=1d&interval=5m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricYen::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = HistoricYen::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'gbp':
                {
                  if ($index_range_date == '1') {
                       $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=GBPINR%3DX&range=1d&interval=5m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = HistoricGbp::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = HistoricGbp::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }
                case 'chf':
                {
                  if ($index_range_date == '1') {
                       $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=CHFINR%3DX&range=1d&interval=5m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = Chf::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = Chf::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                    break;
                }

                case 'eur':
                {
                  if ($index_range_date == '1') {
                       $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=EURINR%3DX&range=1d&interval=5m&indicators=close');
                      $data = json_decode($file);
                      $time = $data->spark->result[0]->response[0]->timestamp;
                      $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
                      $processData = [];
                      for ($i=0; $i < count($time); $i++) {
                          $data = [(($time[$i] +19800)* 1000),$value[$i]];
                          array_push($processData, $data);
                      }
                      return response()->json(['msg' => '1', 'data' => $processData]);
                  }elseif ($index_range_date != '') {
                     $index_details = Eur::where('date','>=',$index_range_date)->orderBy('date', 'asc')
                     ->get()->toArray();
                  }else{
                     $index_details = Eur::orderBy('date', 'asc')
                     ->get()->toArray();
                  }
                }
                  break;
            }

            if(!empty($index_details)) {
                foreach($index_details as $index_detail) {

                      $performance_data[] = array(strtotime($index_detail['date'])*1000,round($index_detail['exchange_value'], 2));
                    }
                return response()->json(['msg' => '1', 'data' => $performance_data]);
            } else {
                return response()->json(['msg' => '0']);
            }
   }

   public function globalIndex()
   {
     $DowJones = DowJones::orderBy('date','desc')->get()->toArray();
     $DowJones2 = DowJones::where('date','<',$DowJones[0]['date'])->orderBy('date','desc')->get()->toArray();
     $DowJonesDiff = $DowJones[0]['close'] - $DowJones2[0]['close'];
     $DowJonesper = ($DowJonesDiff / $DowJones2[0]['close']) * 100;

     $Nikkei = Nikkei::orderBy('date','desc')->get()->toArray();
     $Nikkei2 = Nikkei::where('date','<',$Nikkei[0]['date'])->orderBy('date','desc')->get()->toArray();
     $NikkeiDiff = $Nikkei[0]['close'] - $Nikkei2[0]['close'];
     $Nikkeiper = ($NikkeiDiff / $Nikkei2[0]['close']) * 100;

     $Hansen = Hansen::orderBy('date','desc')->get()->toArray();
     $Hansen2 = Hansen::where('date','<',$Hansen[0]['date'])->orderBy('date','desc')->get()->toArray();
     $HansenDiff = $Hansen[0]['close'] - $Hansen2[0]['close'];
     $Hansenper = ($HansenDiff / $Hansen2[0]['close']) * 100;

     $Sse = Sse::orderBy('date','desc')->get()->toArray();
     $Sse2 = Sse::where('date','<',$Sse[0]['date'])->orderBy('date','desc')->get()->toArray();
     $SseDiff = $Sse[0]['close'] - $Sse2[0]['close'];
     $Sseper = ($SseDiff / $Sse2[0]['close']) * 100;

     $Nacdaq = Nacdaq::orderBy('date','desc')->get()->toArray();
     $Nacdaq2 = Nacdaq::where('date','<',$Nacdaq[0]['date'])->orderBy('date','desc')->get()->toArray();
     $NacdaqDiff = $Nacdaq[0]['close'] - $Nacdaq2[0]['close'];
     $Nacdaqper = ($NacdaqDiff / $Nacdaq2[0]['close']) * 100;

     $Ftse = Ftse::orderBy('date','desc')->get()->toArray();
     $Ftse2 = Ftse::where('date','<',$Ftse[0]['date'])->orderBy('date','desc')->get()->toArray();
     $FtseDiff = $Ftse[0]['close'] - $Ftse2[0]['close'];
     $Ftseper = ($FtseDiff / $Ftse2[0]['close']) * 100;

     $Dax = Dax::orderBy('date','desc')->get()->toArray();
     $Dax2 = Dax::where('date','<',$Dax[0]['date'])->orderBy('date','desc')->get()->toArray();
     $DaxDiff = $Dax[0]['close'] - $Dax2[0]['close'];
     $Daxper = ($DaxDiff / $Dax2[0]['close']) * 100;

     $globalIndex = [];
     $globalIndex['DowJones'] = ['Close' => round($DowJones[0]['close'],2),
                                'diff' => round($DowJonesDiff, 2),
                                'per' => round($DowJonesper, 2)];
     $globalIndex['Nikkei'] = ['Close' => round($Nikkei[0]['close'],2),
                                'diff' => round($NikkeiDiff, 2),
                                'per' => round($Nikkeiper, 2)];
     $globalIndex['Hansen'] = ['Close' => round($Hansen[0]['close'],2),
                                'diff' => round($HansenDiff, 2),
                                'per' => round($Hansenper, 2)];
     $globalIndex['Sse'] = ['Close' => round($Sse[0]['close'],2),
                                'diff' => round($SseDiff, 2),
                                'per' => round($Sseper, 2)];
     $globalIndex['Nacdaq'] = ['Close' => round($Nacdaq[0]['close'],2),
                                'diff' => round($NacdaqDiff, 2),
                                'per' => round($Nacdaqper, 2)];
     $globalIndex['Ftse'] = ['Close' => round($Ftse[0]['close'],2),
                                'diff' => round($FtseDiff, 2),
                                'per' => round($Ftseper, 2)];
     $globalIndex['Dax'] = ['Close' => round($Dax[0]['close'],2),
                                'diff' => round($DaxDiff, 2),
                                'per' => round($Daxper, 2)];

     return view('global.globalindex',['globalindex' => $globalIndex]);
   }

   public function selectedIndex(Request $request)
   {
     $performance_data = array();
            $duration = $request['duration'];
            $index = $request['index'];
            $index_range_date = '';
            $day_data = false;
            switch($duration) {
                case '1':
                {
                    $index_range_date = Carbon::now()->subDays(3)->toDateString();
                    break;
                }
                case '2':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subDays(6)->toDateString();
                    break;
                }
                case '4':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                    break;
                }
                case '5':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                }
                case '6':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                case '7':
                {
                    //calculating the range date to prepare data
                    $index_range_date = '';//Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                default :
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                }
            }

            $ModelName = $request['index'];
            $ModelName = 'App\\'.$ModelName;

            if ($index_range_date != '') {
               $index_details = $ModelName::where('date','>=',$index_range_date)->orderBy('date', 'asc')
               ->get()->toArray();
            }else{
               $index_details = $ModelName::orderBy('date', 'asc')
               ->get()->toArray();
            }

            if(!empty($index_details)) {
                foreach($index_details as $index_detail) {

                      $performance_data[] = array(strtotime($index_detail['date'])*1000,round($index_detail['close'], 2));
                    }
                return response()->json(['msg' => '1', 'data' => $performance_data]);
            } else {
                return response()->json(['msg' => '0']);
            }
   }

   public function liveIndex()
    {
        $finalDate = [];

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5EBSESN&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['sensex']['close'] = $currentClose;
        $finalDate['sensex']['change'] = round($change,2);
        $finalDate['sensex']['per'] = $per;

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=^NSEI&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['nifty']['close'] = $currentClose;
        $finalDate['nifty']['change'] = round($change,2);
        $finalDate['nifty']['per'] = $per;

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-MIDCAP.BO&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['midcap']['close'] = $currentClose;
        $finalDate['midcap']['change'] = round($change,2);
        $finalDate['midcap']['per'] = $per;

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-SMLCAP.BO&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['smallcap']['close'] = $currentClose;
        $finalDate['smallcap']['change'] = round($change,2);
        $finalDate['smallcap']['per'] = $per;

        return response()->json(['msg'=>1,'data'=>$finalDate]);
    }

}
