<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use DB;
use App\HistoricBse;
use App\HistoricMidcap;
use App\HistoricNse;
use App\HistoricSmallcap;
use App\equity;
use App\debt;
use App\balanced;
use Carbon\Carbon;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportPdf(Request $request)
    {
        set_time_limit(600000);
        $date = Carbon::now();
        $date =  date('M-Y', strtotime($date));
        $data = json_decode($request['data']);
        $svg = json_decode($request['svg']);
        $name = mt_rand();
        // $charts = $_SERVER['DOCUMENT_ROOT'].'/chart.svg';
        // if (file_exists($charts)) {
        //     unlink($charts);
        // }
        // $pdf = $_SERVER['DOCUMENT_ROOT'].'/export.pdf';
        // if (file_exists($pdf)) {
        //     unlink($pdf);
        // }
        $dur = '';
        $timespace = json_decode($request['timespace']);
        switch ($timespace) {
            case 2:
                $dur = '5 Days';
                break;
            case 3:
                $dur = '1 Month';
                break;
            case 4:
                $dur = '3 Months';
                break;
            case 5:
                $dur = '1 Year';
                break;
            case 1:
                $dur = '3 Years';
                break;
            case 6:
                $dur = '5 Years';
                break;
            case 8:
                $dur = '6 Months';
                break;
            case 9:
                $dur = '2 Years';
                break;
            default:
                $dur = 'Max';
                break;
        }
        file_put_contents($name.'.svg', $svg);
        PDF::loadView('pdfexport.comparisonpdf', ['data'=>$data, 'date' => $date, 'name' => $name, 'dur' => $dur])->setPaper('a4', 'portrait')->setWarnings(false)->save($name.'.pdf');
        return response()->json(['msg'=>1,'file'=>$name.'.pdf']);
    }
}
