<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SchemeDetail;
use App\Amc;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\equity;
use App\debt;
use App\balanced;
use DB;

class SchemeController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        // \Debugbar::disable();
    }

    public function getSchemes()
    {
    	$amcs = Amc::orderBy('name','dsec')->pluck('name')->toArray();
    	$equity = equity::orderBy('scheme_name','asc')->get()->toArray();
    	$equity_count = equity::get()->count();
    	$debt_count = debt::get()->count();
    	$balanced_count = balanced::get()->count();
    	$scheme_count = $equity_count+$debt_count+$balanced_count;
    	return view('schemes.schemes',['amcs'=>$amcs, 'equity'=>$equity, 'scheme_count'=>$scheme_count]);
    }

    public function schemesDebt()
    {
    	$amcs = Amc::orderBy('name','dsec')->pluck('name')->toArray();
    	$debt = debt::orderBy('scheme_name','asc')->get()->toArray();
    	return view('schemes.schemesDebt',['amcs'=>$amcs, 'debt'=>$debt]);
    }

    public function schemesBalanced()
    {
    	$amcs = Amc::orderBy('name','dsec')->pluck('name')->toArray();
    	$balanced = balanced::orderBy('scheme_name','asc')->get()->toArray();
    	return view('schemes.schemesBalanced',['amcs'=>$amcs, 'balanced'=>$balanced]);
    }

    public function addSchemes(Request $request)
    {
    	set_time_limit(600);
    	$amcs = Amc::pluck('name')->toArray();
    	if (in_array($request['amc_name'], $amcs)) {
			if ($request->hasFile('csv_file')) {
					$save_scheme = 0;
					if ($request['fund_type'] == 1) {
						$codeChack = equity::all()->pluck('scheme_code')->toArray();
						if (in_array($request['scheme_code'], $codeChack)) {
							return response()->json(['msg'=>2,'info'=>'Try new scheme code']);
						}
						$save_scheme = equity::insert([
										'scheme_code'=>$request['scheme_code'],
										'scheme_name'=>$request['scheme_name'],
										'amc_name'=>$request['amc_name'],
										'classification'=>$request['classification'],
										'fund_type'=>$request['fund_type'],
										'small_cap'=>$request['small_cap'],
										'mid_cap'=>$request['mid_cap'],
										'large_cap'=>$request['larg_cap'],
										'asset_size'=>$request['asset_size'],
										'exit_load'=>$request['exit_load']
									]);
					}elseif ($request['fund_type'] == 2) {
						$codeChack = debt::all()->pluck('scheme_code')->toArray();
						if (in_array($request['scheme_code'], $codeChack)) {
							return response()->json(['msg'=>2,'info'=>'Try new scheme code']);
						}
						$save_scheme = debt::insert([
										'scheme_code'=>$request['scheme_code'],
										'scheme_name'=>$request['scheme_name'],
										'amc_name'=>$request['amc_name'],
										'classification'=>$request['classification'],
										'fund_type'=>$request['fund_type'],
										'AA_return'=>$request['aa_return'],
										'avg_mat'=>$request['avg_mat'],
										'mod_duration'=>$request['mod_due'],
										'gov_sec'=>$request['gov_sec'],
										'asset_size'=>$request['asset_size'],
										'exit_load'=>$request['exit_load']
									]);
					}elseif ($request['fund_type'] == 3) {
						$codeChack = balanced::all()->pluck('scheme_code')->toArray();
						if (in_array($request['scheme_code'], $codeChack)) {
							return response()->json(['msg'=>2,'info'=>'Try new scheme code']);
						}
						$save_scheme = balanced::insert([
										'scheme_code'=>$request['scheme_code'],
										'scheme_name'=>$request['scheme_name'],
										'amc_name'=>$request['amc_name'],
										'classification'=>$request['classification'],
										'fund_type'=>$request['fund_type'],
										'small_cap'=>$request['small_cap'],
										'mid_cap'=>$request['mid_cap'],
										'large_cap'=>$request['larg_cap'],
										'debt'=>$request['debt'],
										'asset_size'=>$request['asset_size'],
										'exit_load'=>$request['exit_load']
									]);
					}
					if ($save_scheme) {

						$file = $request->file('csv_file');
						$status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());
						if ($status) {
							if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE)
				    		{
					        $column=fgetcsv($handle);
					        while(!feof($handle)){
					         $rowData[]=fgetcsv($handle);
					        }
					        foreach ($rowData as $key => $value) {
					            if ($value[0] != "") {
					                $inserted_data=array('scheme_code'=>$value[0],
					                                 'scheme_name'=>$value[1],
					                                 'nav'=>$value[2],
					                                 'date'=>date('Y-m-d',strtotime($value[3])),
					                            );
					                DB::table($request['amc_name'])->insert($inserted_data);
					            }
					        }
		        			return response()->json(['msg'=>'1']);
				    		}
				    		else{
				    			return response()->json(['msg'=>'0']);
				    		}
						}
						//else of status
						else{
							return response()->json(['msg'=>'0']);
						}
					}
					//else of failed to save scheme
					else{
						return response()->json(['msg'=>'0']);
					}
			}else{
				return response()->json(['msg'=>'0']);
			}
    	}else{
	        Schema::create($request['amc_name'], function(Blueprint $table){
	            $table->increments('id');
	            $table->string('scheme_code');
	            $table->string('scheme_name');
	            $table->date('date');
	            $table->double('nav');
	            $table->timestamps();
	        });
	        $amcs = Amc::insert(['name'=>$request['amc_name']]);
	        if ($amcs) {
				if ($request->hasFile('csv_file')) {

					$save_scheme = 0;

					if ($request['fund_type'] == 1) {
						$codeChack = equity::all()->pluck('scheme_code')->toArray();
						if (in_array($request['scheme_code'], $codeChack)) {
							return response()->json(['msg'=>2,'info'=>'Try new scheme code']);
						}
						$save_scheme = equity::insert([
										'scheme_code'=>$request['scheme_code'],
										'scheme_name'=>$request['scheme_name'],
										'amc_name'=>$request['amc_name'],
										'classification'=>$request['classification'],
										'fund_type'=>$request['fund_type'],
										'small_cap'=>$request['small_cap'],
										'mid_cap'=>$request['mid_cap'],
										'large_cap'=>$request['larg_cap'],
										'asset_size'=>$request['asset_size'],
										'exit_load'=>$request['exit_load']
									]);
					}elseif ($request['fund_type'] == 2) {
						$codeChack = debt::all()->pluck('scheme_code')->toArray();
						if (in_array($request['scheme_code'], $codeChack)) {
							return response()->json(['msg'=>2,'info'=>'Try new scheme code']);
						}
						$save_scheme = debt::insert([
										'scheme_code'=>$request['scheme_code'],
										'scheme_name'=>$request['scheme_name'],
										'amc_name'=>$request['amc_name'],
										'classification'=>$request['classification'],
										'fund_type'=>$request['fund_type'],
										'AA_return'=>$request['aa_return'],
										'avg_mat'=>$request['avg_mat'],
										'mod_duration'=>$request['mod_due'],
										'gov_sec'=>$request['gov_sec'],
										'asset_size'=>$request['asset_size'],
										'exit_load'=>$request['exit_load']
									]);
					}elseif ($request['fund_type'] == 3) {
						$codeChack = balanced::all()->pluck('scheme_code')->toArray();
						if (in_array($request['scheme_code'], $codeChack)) {
							return response()->json(['msg'=>2,'info'=>'Try new scheme code']);
						}
						$save_scheme = balanced::insert([
										'scheme_code'=>$request['scheme_code'],
										'scheme_name'=>$request['scheme_name'],
										'amc_name'=>$request['amc_name'],
										'classification'=>$request['classification'],
										'fund_type'=>$request['fund_type'],
										'small_cap'=>$request['small_cap'],
										'mid_cap'=>$request['mid_cap'],
										'large_cap'=>$request['larg_cap'],
										'debt'=>$request['debt'],
										'asset_size'=>$request['asset_size'],
										'exit_load'=>$request['exit_load']
									]);
					}
					if ($save_scheme) {

						$file = $request->file('csv_file');

						$status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());

						if ($status) {
							if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE)
				    		{
					        $column=fgetcsv($handle);
					        while(!feof($handle)){
					         $rowData[]=fgetcsv($handle);
					        }
					        foreach ($rowData as $key => $value) {
					            if ($value[0] != "") {
					                $inserted_data=array('scheme_code'=>$value[0],
					                                 'scheme_name'=>$value[1],
					                                 'nav'=>$value[2],
					                                 'date'=>date('Y-m-d',strtotime($value[3])),
					                            );
					                DB::table($request['amc_name'])->insert($inserted_data);
					            }
					        }


		        			return response()->json(['msg'=>'1']);
				    		}
				    		else{
				    			return response()->json(['msg'=>'0']);
				    		}
						}
						//else of status
						else{
							return response()->json(['msg'=>'0']);
						}
					}
					//else of failed to save scheme
					else{
						return response()->json(['msg'=>'0']);
					}

				}
	        }else{
	        	return response()->json(['msg'=>'0']);
	        }
    	}
    }

    public function deleteScheme(Request $request)
    {
    	$fund_type = '';
    	if ($request['fund_type'] == 1) {
    		$fund_type = 'equity';
    	}elseif ($request['fund_type'] == 2) {
    		$fund_type = 'debt';
    	}elseif ($request['fund_type'] == 3) {
    		$fund_type = 'balanced';
    	}

    	$ModelName = 'App\\'.$fund_type;
    	$amcName = $ModelName::where('scheme_code',$request['scheme_code'])->value('amc_name');
    	$removeData = $ModelName::where('scheme_code',$request['scheme_code'])->delete();
    	if ($removeData) {
    		$removeNavs = DB::table($amcName)->where('scheme_code',$request['scheme_code'])->delete();
    		if ($removeNavs) {
    			return response()->json(['msg'=>1]);
    		}else{
    			return response()->json(['msg'=>0]);
    		}
    	}else{
    		return response()->json(['msg'=>0]);
    	}
    }

    public function getSchemeInfo(Request $request)
    {
    	$fund_type = '';
    	if ($request['fund_type'] == 1) {
    		$fund_type = 'equity';
    	}elseif ($request['fund_type'] == 2) {
    		$fund_type = 'debt';
    	}elseif ($request['fund_type'] == 3) {
    		$fund_type = 'balanced';
    	}

    	$ModelName = 'App\\'.$fund_type;
    	$schemeInfo = $ModelName::where('scheme_code',$request['scheme_code'])->get()->toArray();
    	return response()->json(['msg'=>1,'schemeInfo'=>$schemeInfo]);
    }

    public function editSchemeInfo(Request $request)
    {
    	$fund_type = '';
    	if ($request['fund_type'] == 1) {
    		$fund_type = 'equity';
	    	$ModelName = 'App\\'.$fund_type;
	    	$schemeUpdate = $ModelName::where('scheme_code',$request['scheme_code'])
	    							->update(['scheme_name'=>$request['scheme_name'],
	    									'classification'=>$request['classification'],
	    									'small_cap'=>$request['small_cap'],
	    									'mid_cap'=>$request['mid_cap'],
	    									'large_cap'=>$request['large_cap'],
	    									'asset_size'=>$request['asset_size'],
	    									'exit_load'=>$request['exit_load']]);
	    	if ($schemeUpdate) {
	    		return response()->json(['msg'=>1]);
	    	}else{
	    		return response()->json(['msg'=>0]);
	    	}
    	}elseif ($request['fund_type'] == 2) {
    		$fund_type = 'debt';
	    	$ModelName = 'App\\'.$fund_type;
	    	$schemeUpdate = $ModelName::where('scheme_code',$request['scheme_code'])
	    							->update(['scheme_name'=>$request['scheme_name'],
	    									'classification'=>$request['classification'],
	    									'AA_return'=>$request['AA_return'],
	    									'gov_sec'=>$request['gov_sec'],
	    									'mod_duration'=>$request['mod_duration'],
	    									'avg_mat'=>$request['avg_mat'],
	    									'asset_size'=>$request['asset_size'],
	    									'exit_load'=>$request['exit_load']]);
	    	if ($schemeUpdate) {
	    		return response()->json(['msg'=>1]);
	    	}else{
	    		return response()->json(['msg'=>0]);
	    	}
    	}elseif ($request['fund_type'] == 3) {
    		$fund_type = 'balanced';
	    	$ModelName = 'App\\'.$fund_type;
	    	$schemeUpdate = $ModelName::where('scheme_code',$request['scheme_code'])
	    							->update(['scheme_name'=>$request['scheme_name'],
	    									'classification'=>$request['classification'],
	    									'small_cap'=>$request['small_cap'],
	    									'mid_cap'=>$request['mid_cap'],
	    									'large_cap'=>$request['large_cap'],
	    									'debt'=>$request['debt'],
	    									'asset_size'=>$request['asset_size'],
	    									'exit_load'=>$request['exit_load']]);
	    	if ($schemeUpdate) {
	    		return response()->json(['msg'=>1]);
	    	}else{
	    		return response()->json(['msg'=>0]);
	    	}
    	}

    }

}
