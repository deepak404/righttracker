<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amc;
use App\equity;
use App\balanced;
use App\debt;
use DB;
use App\HistoricBse;
use Carbon\Carbon;
class EquityController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
        // \Debugbar::disable();
    }

    public function diversified()
    {
        $diversified = $this->getEquityData('diversified');
    	return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'Diversified Equity']);
    }

    public function largCap()
    {
        $diversified = $this->getEquityData('large cap');
        return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'Large Cap']);
    }

    public function smallmidCap()
    {
        $diversified = $this->getEquityData('sm');
        return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'Small & Mid Cap']);
    }

    public function elss()
    {
        $diversified = $this->getEquityData('ELSS');
        return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'ELSS']);
    }

    public function sectoral()
    {
        $diversified = $this->getEquityData('sectoral');
        return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'sectoral']);
    }

    public function hybrid()
    {
        $diversified = $this->getEquityData('hybrid');
        return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'sectoral']);
    }

    public function arbitrage()
    {
        $diversified = $this->getEquityData('arbitrage');
        return view('equity.diversified-equity',['schemeData'=>$diversified, 'name'=>'Arbitrage']);
    }

    public function getEquityData($classification)
    {
        $Today = Carbon::now()->toDateString();
        $Dr = Carbon::now()->subDays(2)->toDateString();
        $Wr = Carbon::now()->subDays(6)->toDateString();
        $Mr = Carbon::now()->subMonths(1)->toDateString();
        $Yr = Carbon::now()->subMonths(12)->toDateString();
        $five_Yr = Carbon::now()->subMonths(60)->toDateString();
        $current_sensex = HistoricBse::orderBy('date','DESC')->get()->toArray();
        $one_year_sensex = HistoricBse::where('date','<=',$Yr)->orderBy('date','DESC')->get()->toArray();

        if ($classification == 'sm') {
            $data1 = equity::where('classification','small cap')->get()->toArray();
            $data2 = equity::where('classification','mid cap')->get()->toArray();
            $schemes = array_merge($data1,$data2);
        }else{
            $schemes = equity::where('classification', $classification)->get()->toArray();
        }
        $scheme_info = [];
        foreach ($schemes as $scheme) {
            $DataArray = [];
            $day_val = 0;
            $week_val = 0;
            $month_val = 0;
            $year_val = 0;
            $five_year_val = 0;

            $DataArray['scheme_name'] = $scheme['scheme_name'];
            $DataArray['exit_load'] = $scheme['exit_load'];
            $DataArray['brk'] = $scheme['brk'];
            $DataArray['small_cap'] = $scheme['small_cap'];
            $DataArray['mid_cap'] = $scheme['mid_cap'];
            $DataArray['large_cap'] = $scheme['large_cap'];
            $DataArray['asset_size'] = $scheme['asset_size'];
            $DataArray['scheme_code'] = $scheme['scheme_code'];
            $others = $scheme['small_cap']+$scheme['mid_cap']+$scheme['large_cap'];
            $others = 100 - $others;
            if ($others < 0) {
                $others = 0;
            }
            $DataArray['others'] = $others;
            $DataArray['total'] = $others+$scheme['small_cap']+$scheme['mid_cap']+$scheme['large_cap'];

            $amc_name = $scheme['amc_name'];
            $scheme_code = $scheme['scheme_code'];
            $historicData = DB::table($amc_name)->where('scheme_code',$scheme_code)
                                    ->orderBy('date','dese')
                                    ->get()->toArray();
            $sharpe_ratio = $this->getSharpeRatio($scheme_code, $amc_name);
            if (empty($sharpe_ratio['one_year'])) {
                $sharpe_ratio['one_year'] = 0;
            }
            $one_year_beta_ratio = $sharpe_ratio['one_year'] / (($current_sensex[0]['close'] - $one_year_sensex[0]['close'])/$one_year_sensex[0]['close']);
            $DataArray['sharpe_ratio'] = $sharpe_ratio['one_year'];
            $DataArray['todays_nav'] = $historicData[0]->nav;
            $DataArray['beta_ratio'] = round($one_year_beta_ratio, 2);
            
            foreach ($historicData as $pickedData) {
                if ($five_Yr >= $pickedData->date) {
                    if ($five_year_val == 0) {
                        $five_year_val = 1;
                        // $five_year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $five_year_return = (pow(($historicData[0]->nav/$pickedData->nav), (1/5))-1)*100;
                        $DataArray['five_year_return'] = round($five_year_return, 2);
                    }
                }elseif ($Yr >= $pickedData->date) {
                    if ($year_val == 0) {
                        $year_val = 1;
                        $year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['year_return'] = round($year_return, 2);
                    }
                }elseif ($Mr >= $pickedData->date) {
                    if ($month_val == 0) {
                        $month_val = 1;
                        $month_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['month_return'] = round($month_return, 2);

                    }
                }elseif ($Wr >= $pickedData->date) {
                    if ($week_val == 0) {        
                        $week_val = 1;
                        $week_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['week_return'] = round($week_return, 2);
                    }
                }elseif ($Dr >= $pickedData->date) {
                    if ($historicData[0]->date > $pickedData->date) {
                        if ($day_val == 0) {
                            $day_val = 1;
                            $day_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                            $DataArray['day_return'] = round($day_return, 2);
                        }
                    }
                }
            }
            if (empty($DataArray['day_return'])) {
                $DataArray['day_return'] = 0;
            }
            array_push($scheme_info, $DataArray);
        }
        usort($scheme_info, function($a, $b) {
            return $b['day_return'] <=> $a['day_return'];
        });

        return $scheme_info;
    }

    public function getSharpeRatio($scheme_code, $amc_name)
    {
        $risk_free_monthly_return = 8/12;
        $scheme_returns = array();
        $sharpe_ratio = array();
        $range_date = Carbon::now();
        $year_completed = true;
        while($year_completed) {
            $range_start_date = $range_date->toDateString();
            $range_date->subMonths(1);
            $range_end_date = $range_date->toDateString();
            //748823798574
            $range_data = DB::table($amc_name)->where('scheme_code', $scheme_code)
                ->whereBetween('date',[$range_end_date,$range_start_date])
                ->orderBy('date', 'asc')
                ->get();
            if($range_data->count() > 0) {
                $range_start_data = $range_data->first();
                $range_end_data = $range_data->last();
                //Log::info($range_start_date);Log::info($range_end_date);
                //Log::info($range_start_data);//Log::info($range_end_data);
                $start_nav = $range_start_data->nav;
                $end_nav = $range_end_data->nav;
                $ratio = (($end_nav - $start_nav)/$start_nav)*100;
                $scheme_returns[] = $ratio - 0.67;//Log::info($scheme_returns);
                //$scheme_returns['year'] = ;
            } else {

                $year_completed = false;
            }

            if(count($scheme_returns) == 12) {
                $standard_deviation = array();
                $average_return = array_sum($scheme_returns) / 6;
                $deviation;
                foreach($scheme_returns as $scheme_return) {
                     $standard_deviation[] = pow(($scheme_return-$average_return),2);
                }
                $sharpe_ratio['one_year'] = round(($average_return)/(array_sum($standard_deviation)/6),2);
                $year_completed = false;
            } else if(count($scheme_returns) == 6){
                $standard_deviation = array();
                $average_return = array_sum($scheme_returns) / 6;
                $deviation;
                foreach($scheme_returns as $scheme_return) {
                     $standard_deviation[] = pow(($scheme_return-$average_return),2);
                }
                $sharpe_ratio['six_month'] = round(($average_return)/(array_sum($standard_deviation)/12),2);
                //$sharpe_ratio['six_month'] = (array_sum($scheme_returns) / 6)/stats_standard_deviation($scheme_returns);
            }
        }
        return $sharpe_ratio;

    }

}
