<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvestorInfo;
use App\InvestmentInfo;
use App\equity;
use App\debt;
use App\balanced;
use DB;
use App\HistoricBse;
use App\HistoricSmallcap;
use App\HistoricMidcap;
use App\HistoricNse;
use App\BseAuto;
use App\BseHealthCare;
use App\BseInfra;
use App\BsePower;
use App\BseEnergy;
use App\BseBanking;
use Carbon\Carbon;

class PortfolioController extends Controller
{
	public function index()
	{
		return view('investor.investor');
	}

	public function addInvestor(Request $request)
	{
		$insert = InvestorInfo::insert(['name'=>$request['name']]);
		if ($insert) {
			return response()->json(['msg'=>1]);
		}else{
			return response()->json(['msg'=>0]);
		}
	}

	public function getGroup()
	{
		$users = InvestorInfo::all()->toArray();
		return response()->json(['msg'=>1,'user'=>$users]);
	}

	public function addInvestments(Request $request)
	{
		$inv_id = $request['inv_name'];
		$scheme_code = explode(',', $request['scheme_code']);
		foreach ($scheme_code as $value) {
			$check = InvestmentInfo::where('inv_id',$inv_id)->where('scheme_code',$value)->get();
			if (count($check) == 0) {
				$insert = InvestmentInfo::insert(['inv_id'=>$inv_id,'scheme_code'=>$value]); 
			}
		}
		return response()->json(['msg'=>1]);
	}

	public function userSearch(Request $request)
	{
		$names = InvestorInfo::where('name','LIKE','%'.$request['search-input'].'%')->get()->toArray();
		return json_encode($names);
	}

	public function trackUser(Request $request)
	{
		$inv_id = $request['inv_id'];
		$from = date('Y-m-d', strtotime($request['from_date']));
		$to = date('Y-m-d',strtotime($request['to_date']));
		$scheme_code = InvestmentInfo::where('inv_id',$inv_id)->get()->pluck('scheme_code')->toArray();
		// $days = date_diff(strtotime($request['to_date']),strtotime($request['from_date']))->format('%d');
		$datediff = strtotime($request['to_date']) - strtotime($request['from_date']);
		$days = $datediff / (60 * 60 * 24);


		$scheme_details = [];
		foreach ($scheme_code as $value) {
			$equity = equity::where('scheme_code',$value)->get()->toArray();
			$debt = debt::where('scheme_code',$value)->get()->toArray();
			$balanced = balanced::where('scheme_code',$value)->get()->toArray();
			if (count($equity) != 0) {
				$scheme_details[] = $equity[0];
			}elseif (count($debt) != 0) {
				$scheme_details[] = $debt[0];
			}elseif(count($balanced) != 0){
				$scheme_details[] = $balanced[0];
			}
		}

		$sensex_values = HistoricBse::whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
		$sensex_count = count($sensex_values)-1;
		$sensex_change = $sensex_values[$sensex_count]['close'] - $sensex_values[0]['close'];
		$sensex_per = ($sensex_change/$sensex_values[0]['close'])*100;
		$sensex_chart = [];
		$cummilative_return = 0;
        $currentIndex = '';
        $preIndex = '';
        foreach($sensex_values as $index_detail) {
                if (empty($preIndex)) {
                    $preIndex = $index_detail;
                    $sensex_chart[] = array(strtotime($index_detail['date'])*1000,0,round($index_detail['close'], 2));
                }else{
                    $currentIndex = $index_detail;
                    $day_ratio = (($currentIndex['close'] - $preIndex['close'])/$preIndex['close'])*100;
                    $cummilative_return += $day_ratio;
                    $sensex_chart[] = array(strtotime($index_detail['date'])*1000,round($cummilative_return, 2),round($index_detail['close'], 2));
                }            
                $preIndex = $index_detail;
        }

		$smallcap_values = HistoricSmallcap::whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
		$smallcap_count = count($smallcap_values)-1;
		$small_change = $smallcap_values[$smallcap_count]['close'] - $smallcap_values[0]['close'];
		$smallcap_per = ($small_change/$smallcap_values[0]['close'])*100;
		$smallcap_chart = [];
		$cummilative_return = 0;
        $currentIndex = '';
        $preIndex = '';
        foreach($smallcap_values as $index_detail) {
                if (empty($preIndex)) {
                    $preIndex = $index_detail;
                    $smallcap_chart[] = array(strtotime($index_detail['date'])*1000,0,round($index_detail['close'], 2));
                }else{
                    $currentIndex = $index_detail;
                    $day_ratio = (($currentIndex['close'] - $preIndex['close'])/$preIndex['close'])*100;
                    $cummilative_return += $day_ratio;
                    $smallcap_chart[] = array(strtotime($index_detail['date'])*1000,round($cummilative_return, 2),round($index_detail['close'], 2));
                }            
                $preIndex = $index_detail;
        }

		$midcap_values = HistoricMidcap::whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
		$midcap_count = count($midcap_values)-1;
		$midcap_change = $midcap_values[$midcap_count]['close'] - $midcap_values[0]['close'];
		$midcap_per = ($midcap_change/$midcap_values[0]['close'])*100;
		$midcap_chart = [];
		$cummilative_return = 0;
        $currentIndex = '';
        $preIndex = '';
        foreach($midcap_values as $index_detail) {
                if (empty($preIndex)) {
                    $preIndex = $index_detail;
                    $midcap_chart[] = array(strtotime($index_detail['date'])*1000,0,round($index_detail['close'], 2));
                }else{
                    $currentIndex = $index_detail;
                    $day_ratio = (($currentIndex['close'] - $preIndex['close'])/$preIndex['close'])*100;
                    $cummilative_return += $day_ratio;
                    $midcap_chart[] = array(strtotime($index_detail['date'])*1000,round($cummilative_return, 2),round($index_detail['close'], 2));
                }            
                $preIndex = $index_detail;
        }

        $processData2 = [];
        $data = 100;
        $rate = 0.0178571428;
        $timeData = Carbon::now()->subDays($days)->toDateString();
        $processData2[] = array(strtotime($timeData)*1000,0,100);
        $val = $days;
        for ($i=1; $i < $days; $i++) {
            $val -= 1;
            $timeData = Carbon::now()->subDays($val)->toDateString();
            // $data = $data*(((6.5/100)+1)^($i/365));
            // $per = 0.06*$data;
            $data = $data+$rate;
            $diff = round(($data-100), 2);
            // $diff = round((($data-1)/1)*100);
            $processData2[] = array(strtotime($timeData)*1000,$diff,round($data,2));
        }

        // $finalData[$value] = $processData2;
        $processData3 = [];
        $data2 = 100;
        $rate2 = 0.0212328767;
        $timeData2 = Carbon::now()->subDays($days)->toDateString();
        $processData3[] = array(strtotime($timeData2)*1000,0,100);
        $val2 = $days;
        for ($i=1; $i < $days; $i++) {
            $val2 -= 1;
            $timeData2 = Carbon::now()->subDays($val2)->toDateString();
            // $data2 = $data2*(((6.5/100)+1)^($i/365));
            // $per = 0.06*$data2;
            $data2 = $data2+$rate2;
            $diff2 = round(($data2-100), 2);
            // $diff = round((($data2-1)/1)*100);
            $processData3[] = array(strtotime($timeData2)*1000,$diff2,round($data2,2));
        }
        // $finalData[$value] = $processData2;

		$equity_detail = [];
		$equity_val = [];

		$sensex_funds = [];
		$sensex_funds['sensex']['scheme_name'] = 'BSE Sensex';
		$sensex_funds['sensex']['nav'] = $sensex_values[$sensex_count]['close'];
		$sensex_funds['sensex']['per'] = round($sensex_per, 2); 
		$sensex_data = [];
        $sensex_data['sensex'] = $sensex_chart;

		$small_funds = [];
		$small_funds['small_cap']['scheme_name'] = 'BSE SmallCap';
		$small_funds['small_cap']['nav'] = $smallcap_values[$smallcap_count]['close'];
		$small_funds['small_cap']['per'] = round($smallcap_per, 2);
		$small_data = [];
		$small_data['small_cap'] = $smallcap_chart;

		$mid_funds = [];
		$mid_funds['mid_cap']['scheme_name'] = 'BSE MidCap';
		$mid_funds['mid_cap']['nav'] = $midcap_values[$midcap_count]['close'];
		$mid_funds['mid_cap']['per'] = round($midcap_per, 2); 
		$mid_data = [];
		$mid_data['mid_cap'] = $midcap_chart;

		$debt_detail = [];
		$debt_val = [];

		$fd_funds = [];
		$fd_funds['fd']['scheme_name'] = 'FD';
		$fd_funds['fd']['nav'] = round($data, 2);
		$fd_funds['fd']['per'] = $diff;
		$fd_data = [];
		$fd_data['fd'] = $processData2;

		$gov_funds = [];
		$gov_funds['gov']['scheme_name'] = 'Gov. Bonds';
		$gov_funds['gov']['nav'] = round($data2, 2);
		$gov_funds['gov']['per'] = $diff2;
		$gov_data = [];
		$gov_data['gov'] = $processData3;

		$balanced_detail = [];
		$balanced_detail['sensex']['scheme_name'] = 'BSE Sensex';
		$balanced_detail['sensex']['nav'] = $sensex_values[$sensex_count]['close'];
		$balanced_detail['sensex']['per'] = round($sensex_per, 2); 
		$balanced_val = [];
		$balanced_val['sensex'] = $sensex_chart;



		foreach ($scheme_details as $schemes) {
			if ($schemes['fund_type'] == 1) {
				if ($schemes['classification'] == 'small cap') {
					$amc = $schemes['amc_name'];
					$scheme_code = $schemes['scheme_code'];
					$eq_data = DB::table($amc)->where('scheme_code',$scheme_code)->whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
					$e_count = count($eq_data) - 1;
					$change = $eq_data[$e_count]->nav - $eq_data[0]->nav;
					$per = ($change/$eq_data[0]->nav)*100;
					$small_funds[$scheme_code]['scheme_name'] = $schemes['scheme_name'];
					$small_funds[$scheme_code]['nav'] = round($eq_data[$e_count]->nav, 2);
					$small_funds[$scheme_code]['per'] = round($per, 2);
					$performance_data = [];
					$cummilative_return = 0;
	                $currentIndex = '';
	                $preIndex = '';
	                foreach($eq_data as $index_detail) {
	                        if (empty($preIndex)) {
	                            $preIndex = $index_detail;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
	                        }else{
	                            $currentIndex = $index_detail;
	                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
	                            $cummilative_return += $day_ratio;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
	                        }            
	                        $preIndex = $index_detail;
	                }
	                $small_data[$scheme_code] = $performance_data;
				}elseif ($schemes['classification'] == 'mid cap') {
					$amc = $schemes['amc_name'];
					$scheme_code = $schemes['scheme_code'];
					$eq_data1 = DB::table($amc)->where('scheme_code',$scheme_code)->whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
					$e_count = count($eq_data1) - 1;
					$change = $eq_data1[$e_count]->nav - $eq_data1[0]->nav;
					$per = ($change/$eq_data1[0]->nav)*100;
					$mid_funds[$scheme_code]['scheme_name'] = $schemes['scheme_name'];
					$mid_funds[$scheme_code]['nav'] = round($eq_data1[$e_count]->nav, 2);
					$mid_funds[$scheme_code]['per'] = round($per, 2);
					$performance_data = [];
					$cummilative_return = 0;
	                $currentIndex = '';
	                $preIndex = '';
	                foreach($eq_data1 as $index_detail) {
	                        if (empty($preIndex)) {
	                            $preIndex = $index_detail;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
	                        }else{
	                            $currentIndex = $index_detail;
	                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
	                            $cummilative_return += $day_ratio;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
	                        }            
	                        $preIndex = $index_detail;
	                }
	                $mid_data[$scheme_code] = $performance_data;
				}else{
					$amc = $schemes['amc_name'];
					$scheme_code = $schemes['scheme_code'];
					$eq_data2 = DB::table($amc)->where('scheme_code',$scheme_code)->whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
					$e_count = count($eq_data2) - 1;
					$change = $eq_data2[$e_count]->nav - $eq_data2[0]->nav;
					$per = ($change/$eq_data2[0]->nav)*100;
					$sensex_funds[$scheme_code]['scheme_name'] = $schemes['scheme_name'];
					$sensex_funds[$scheme_code]['nav'] = round($eq_data2[$e_count]->nav, 2);
					$sensex_funds[$scheme_code]['per'] = round($per, 2);
					$performance_data = [];
					$cummilative_return = 0;
	                $currentIndex = '';
	                $preIndex = '';
	                foreach($eq_data2 as $index_detail) {
	                        if (empty($preIndex)) {
	                            $preIndex = $index_detail;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
	                        }else{
	                            $currentIndex = $index_detail;
	                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
	                            $cummilative_return += $day_ratio;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
	                        }            
	                        $preIndex = $index_detail;
	                }
	                $sensex_data[$scheme_code] = $performance_data;
				}
			}elseif ($schemes['fund_type'] == 2) {
				if ($schemes['classification'] == 'fmp' || $schemes['classification'] == 'long term' || $schemes['classification'] == 'mip' || $schemes['classification'] == 'credit opps') {
					$amc = $schemes['amc_name'];
					$scheme_code2 = $schemes['scheme_code'];
					$debt_data = DB::table($amc)->where('scheme_code',$scheme_code2)->whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
					$d_count = count($debt_data) - 1;
					$change = $debt_data[$d_count]->nav - $debt_data[0]->nav;
					$per = ($change/$debt_data[0]->nav)*100;
					$gov_funds[$scheme_code2]['scheme_name'] = $schemes['scheme_name'];
					$gov_funds[$scheme_code2]['nav'] = round($change, 2);
					$gov_funds[$scheme_code2]['per'] = round($per, 2);
					$performance_data = [];
					$cummilative_return = 0;
	                $currentIndex = '';
	                $preIndex = '';
	                foreach($debt_data as $index_detail) {
	                        if (empty($preIndex)) {
	                            $preIndex = $index_detail;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
	                        }else{
	                            $currentIndex = $index_detail;
	                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
	                            $cummilative_return += $day_ratio;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
	                        }            
	                        $preIndex = $index_detail;
	                }
	                $gov_data[$scheme_code2] = $performance_data;
				}else{
					$amc = $schemes['amc_name'];
					$scheme_code2 = $schemes['scheme_code'];
					$debt_data2 = DB::table($amc)->where('scheme_code',$scheme_code2)->whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
					$d_count = count($debt_data2) - 1;
					$change = $debt_data2[$d_count]->nav - $debt_data2[0]->nav;
					$per = ($change/$debt_data2[0]->nav)*100;
					$fd_funds[$scheme_code2]['scheme_name'] = $schemes['scheme_name'];
					$fd_funds[$scheme_code2]['nav'] = round($debt_data2[0]->nav, 2);
					$fd_funds[$scheme_code2]['per'] = round($per, 2);
					$performance_data = [];
					$cummilative_return = 0;
	                $currentIndex = '';
	                $preIndex = '';
	                foreach($debt_data2 as $index_detail) {
	                        if (empty($preIndex)) {
	                            $preIndex = $index_detail;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
	                        }else{
	                            $currentIndex = $index_detail;
	                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
	                            $cummilative_return += $day_ratio;
	                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
	                        }            
	                        $preIndex = $index_detail;
	                }
	                $fd_data[$scheme_code2] = $performance_data;
				}
				
			}elseif ($schemes['fund_type'] == 3) {
				$amc = $schemes['amc_name'];
				$scheme_code3 = $schemes['scheme_code'];
				$balanced_data = DB::table($amc)->where('scheme_code',$scheme_code3)->whereBetween('date',[$from,$to])->orderBy('date','asc')->get()->toArray();
				$b_count = count($balanced_data) - 1;
				$change = $balanced_data[$b_count]->nav - $balanced_data[0]->nav;
				$per = ($change/$balanced_data[0]->nav)*100;
				$balanced_detail[$scheme_code3]['scheme_name'] = $schemes['scheme_name'];
				$balanced_detail[$scheme_code3]['nav'] = round($balanced_data[0]->nav, 2);
				$balanced_detail[$scheme_code3]['per'] = round($per, 2);
				$performance_data = [];
				$cummilative_return = 0;
                $currentIndex = '';
                $preIndex = '';
                foreach($balanced_data as $index_detail) {
                        if (empty($preIndex)) {
                            $preIndex = $index_detail;
                            $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
                        }else{
                            $currentIndex = $index_detail;
                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
                            $cummilative_return += $day_ratio;
                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
                        }            
                        $preIndex = $index_detail;
                }
                $balanced_val[$scheme_code3] = $performance_data;
			}
		}

		$equity_detail['small_cap'] = $small_funds;
		$equity_detail['mid_cap'] = $mid_funds;
		$equity_detail['sensex'] =$sensex_funds;

		$equity_val['small_cap'] = $small_data;
		$equity_val['mid_cap'] = $mid_data;
		$equity_val['sensex'] = $sensex_data;

		$debt_detail['fd'] = $fd_funds;
		$debt_detail['gov'] = $gov_funds;

		$debt_val['fd'] = $fd_data;
		$debt_val['gov'] = $gov_data;


		return response()->json(['equity_detail'=>(array)$equity_detail,
								'equity_val'=>$equity_val,
								'debt_detail'=>(array)$debt_detail,
								'debt_val'=>$debt_val,
								'balanced_detail'=>$balanced_detail,
								'balanced_val'=>$balanced_val]);
	}

	public function getInv()
	{
		$investor = InvestorInfo::all('id','name')->toArray();
		return response()->json(['inv_name'=>$investor]);
	}

	public function getInvestments(Request $request)
	{
		$investments = InvestmentInfo::where('inv_id',$request['user_id'])->get()->toArray();
		$finalData = [];
		$count = 0;
		foreach ($investments as $key => $value) {
			$equity = equity::where('scheme_code',$value['scheme_code'])->value('scheme_name');
			$debt = debt::where('scheme_code',$value['scheme_code'])->value('scheme_name');
			$balanced = balanced::where('scheme_code', $value['scheme_code'])->value('scheme_name');
			if (!empty($equity)) {
				$finalData[$count]['id'] = $value['id'];
				$finalData[$count]['name'] = $equity;
			}elseif (!empty($debt)) {
				$finalData[$count]['id'] = $value['id'];
				$finalData[$count]['name'] = $debt;
			}elseif (!empty($balanced)) {
				$finalData[$count]['id'] = $value['id'];
				$finalData[$count]['name'] = $balanced;
			}
			$count += 1;
		}
		return response()->json(['val'=>$finalData]);
	}

	public function removeScheme(Request $request)
	{
		$update = InvestmentInfo::where('id',$request['inv_id'])->delete();
		if ($update) {
			return response()->json(['msg'=>1]);
		}else{
			return response()->json(['msg'=>0]);
		}
	}

	public function removeUser(Request $request)
	{
		$remove = InvestmentInfo::where('inv_id',$request['inv_id'])->delete();
		$remove_user = InvestorInfo::where('id',$request['inv_id'])->delete();
		if ($remove_user) {
			return response()->json(['msg'=>1]);
		}else{
			return response()->json(['msg'=>0]);
		}
	}
}
