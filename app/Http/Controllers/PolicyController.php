<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Wpi;
use App\Cpi;
use App\Slr;
use App\RepoRate;
use App\TenYearBond;
use App\Gdp;
use Carbon\Carbon;

class PolicyController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        // \Debugbar::disable();
    }

    public function index()
    {
        $wpi = Wpi::orderBy('date','desc')->get()->toArray();
        $gdp = Gdp::orderBy('date','desc')->get()->toArray();
        $cpi = Cpi::orderBy('date','desc')->get()->toArray();
        $slr = Slr::orderBy('date','desc')->get()->toArray();
        $rr = RepoRate::orderBy('date','desc')->get()->toArray();
        $ten = TenYearBond::orderBy('date','desc')->get()->toArray();
        
        $wpiDate = $wpi[0]['date'];
        $wpiPrice = $wpi[0]['price'];
        $gdpDate = $gdp[0]['date'];
        $gdpPrice = $gdp[0]['price'];
        $cpiDate = $cpi[0]['date'];
        $cpiPrice = $cpi[0]['price'];
        $tenDate = $ten[0]['date'];
        $tenPrice = $ten[0]['price'];
        $rrDate = $rr[0]['date'];
        $rrPrice = $rr[0]['price'];
        $slrDate = $slr[0]['date'];
        $slrPrice = $slr[0]['price'];


    	return view('policy.policy',['wpiDate'=>$wpiDate,'wpiPrice'=>$wpiPrice,'gdpDate'=>$gdpDate,'gdpPrice'=>$gdpPrice,'cpiDate'=>$cpiDate,'cpiPrice'=>$cpiPrice,'tenDate'=>$tenDate, 'tenPrice'=>$tenPrice,'rrDate'=>$rrDate,'rrPrice'=>$rrPrice,'slrPrice'=>$slrPrice,'slrDate'=>$slrDate]);
    }

    public function insertPolicy(Request $request)
    {
	    $validator = Validator::make($request->all(), [
	        'policy' => 'required',
	        'date' => 'required',
            'price' => 'required',
	    ]);

        if ($validator->fails()) {
        	return response()->json(['msg' => 0]);
        }else{
            switch ($request['policy']) {
                case 'wpi':
                    $data = Wpi::where('date',date('Y-m-d',strtotime($request['date'])))->get()->toArray();
                        if (count($data) > 0) {
                            $insert = Wpi::where('date',date('Y-m-d',strtotime($request['date'])))->update(['price'=>$request['price']]);
                        }else{
                            $insert = Wpi::insert(['date'=>date('Y-m-d',strtotime($request['date'])),'price'=>$request['price']]);
                        }
                    break;
                case 'cpi':
                    $data = Cpi::where('date',date('Y-m-d',strtotime($request['date'])))->get()->toArray();
                        if (count($data) > 0) {
                            $insert = Cpi::where('date',date('Y-m-d',strtotime($request['date'])))->update(['price'=>$request['price']]);
                        }else{
                            $insert = Cpi::insert(['date'=>date('Y-m-d',strtotime($request['date'])),'price'=>$request['price']]);
                        }
                    break;
                case 'gdp':
                    $data = Gdp::where('date',date('Y-m-d',strtotime($request['date'])))->get()->toArray();
                        if (count($data) > 0) {
                            $insert = Gdp::where('date',date('Y-m-d',strtotime($request['date'])))->update(['price'=>$request['price']]);
                        }else{
                            $insert = Gdp::insert(['date'=>date('Y-m-d',strtotime($request['date'])),'price'=>$request['price']]);
                        }
                    break;
                case 'rr':
                    $data = RepoRate::where('date',date('Y-m-d',strtotime($request['date'])))->get()->toArray();
                        if (count($data) > 0) {
                            $insert = RepoRate::where('date',date('Y-m-d',strtotime($request['date'])))->update(['price'=>$request['price']]);
                        }else{
                            $insert = RepoRate::insert(['date'=>date('Y-m-d',strtotime($request['date'])),'price'=>$request['price']]);
                        }
                    break;
                case 'ten':
                    $data = TenYearBond::where('date',date('Y-m-d',strtotime($request['date'])))->get()->toArray();
                        if (count($data) > 0) {
                            $insert = TenYearBond::where('date',date('Y-m-d',strtotime($request['date'])))->update(['price'=>$request['price']]);
                        }else{
                            $insert = TenYearBond::insert(['date'=>date('Y-m-d',strtotime($request['date'])),'price'=>$request['price']]);
                        }
                    break;
                case 'slr':
                    $data = Slr::where('date',date('Y-m-d',strtotime($request['date'])))->get()->toArray();
                        if (count($data) > 0) {
                            $insert = Slr::where('date',date('Y-m-d',strtotime($request['date'])))->update(['price'=>$request['price']]);
                        }else{
                            $insert = Slr::insert(['date'=>date('Y-m-d',strtotime($request['date'])),'price'=>$request['price']]);
                        }
                    break;
            }
            if ($insert) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0]);
            }
        }
    }

    public function addPolicy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required',
            'policy' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 0]);
        }else{
            $model = '';
            if ($request['policy'] == 'rr') {
                $model = 'RepoRate';
            }elseif ($request['policy'] == 'wpi') {
                $model = 'Wpi';
            }elseif ($request['policy'] == 'cpi') {
                $model = 'Cpi';
            }elseif ($request['policy'] == 'gdp') {
                $model = 'Gdp';
            }elseif ($request['policy'] == 'ten') {
                $model = 'TenYearBond';
            }elseif ($request['policy'] == 'slr') {
                $model = 'Slr';
            }
            $ModelName = 'App\\'.$model;

            set_time_limit(60000);
            $file = $request->file('file');
            $status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());
            if ($status) {
                if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE)
                {
                    $ModelName::where('id','>',0)->delete();  
                $column=fgetcsv($handle);
                while(!feof($handle)){
                 $rowData[]=fgetcsv($handle);
                }
                foreach ($rowData as $key => $value) {
                    if ($value[0] != "") {

                        $inserted_data=array('date' => date('Y-m-d',strtotime($value[0])),
                                            'price' => $value[1]);
                        $ModelName::insert($inserted_data);
                    }
                }
                return response()->json(['msg'=>'1']);
                }
                else{
                    return response()->json(['msg'=>'0']);
                }
            }
            //else of status
            else{
                return response()->json(['msg'=>'0']);
            }
        }
    }

    public function selectedPolicy(Request $request){

        $validator = Validator::make($request->all(), [
            'duration' => 'required',
            'policy' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg'=>0]);
        }else{

            $duration = $request['duration'];
            $policy = $request['policy'];
            $index_range_date = '';
            $performance_data = [];

            switch ($duration) {
                case '1':
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                case '2':
                    $index_range_date = Carbon::now()->subMonths(5)->toDateString();
                    break;
                case '3':
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                case '4':
                    $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                    break;
                default:
                    $index_range_date = '';
                    break;
            }

            switch ($policy) {
                case 'cpi':
                    if ($index_range_date == '') {
                        $policy_info = Cpi::orderBy('date','asc')->get()->toArray();
                    }else{
                        $policy_info = Cpi::where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
                    }
                    break;
                case 'wpi':
                    if ($index_range_date == '') {
                        $policy_info = Wpi::orderBy('date','asc')->get()->toArray();
                    }else{
                        $policy_info = Wpi::where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
                    }
                    break;
                case 'gdp':
                    if ($index_range_date == '') {
                        $policy_info = Gdp::orderBy('date','asc')->get()->toArray();
                    }else{
                        $policy_info = Gdp::where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
                    }
                    break;
                case 'rr':
                    if ($index_range_date == '') {
                        $policy_info = RepoRate::orderBy('date','asc')->get()->toArray();
                    }else{
                        $policy_info = RepoRate::where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
                    }
                    break;
                case 'ten':
                    if ($index_range_date == '') {
                        $policy_info = TenYearBond::orderBy('date','asc')->get()->toArray();
                    }else{
                        $policy_info = TenYearBond::where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
                    }
                    break;
                case 'slr':
                    if ($index_range_date == '') {
                        $policy_info = Slr::orderBy('date','asc')->get()->toArray();
                    }else{
                        $policy_info = Slr::where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
                    }
                    break;
            }
            if(!empty($policy_info)) {
                foreach($policy_info as $policy) {
                      
                      $performance_data[] = array(strtotime($policy['date'])*1000,$policy['price']);
                    }
                return response()->json(['msg' => '1', 'data' => $performance_data]);
            } else {
                return response()->json(['msg' => '0']);
            }

        }
    }
}
