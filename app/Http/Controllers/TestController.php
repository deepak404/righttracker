<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DowJones;
use App\Ftse;
use App\Hansen;
use App\HistoricBse;
use App\HistoricChf;
use App\HistoricGbp;
use App\HistoricMidcap;
use App\HistoricNse;
use App\HistoricSgd;
use App\HistoricSmallcap;
use App\HistoricUsd;
use App\HistoricYen;
use App\Nacdaq;
use App\Nikkei;
use App\Sse;
use App\TodayBse;
use App\TodayMidcap;
use App\TodayNse;
use App\TodaySmallcap;
use DateTime;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use DB;
use App\equity;
use App\debt;
use App\balanced;
use App\Gold;
use App\Silver;
use App\NaturalGas;
use App\Wpi;
use App\Cpi;
use App\Slr;
use App\RepoRate;
use App\TenYearBond;
use App\Gdp;
use App\Chf;
use App\Eur;
use Carbon\Carbon;
use App\BseAuto;
use App\BseHealthCare;
use App\BseInfra;
use App\BsePower;
use App\BseEnergy;
use App\BseBanking;
use App\Dax;
use App\NseSmallCap;
use App\NseMidCap;
use App\Crisil;
use App\NseSector;
use DatePeriod;
use DateInterval;

class TestController extends Controller
{

  //   public function indexs()
  //   {
  //   	$array = [['index'=>'%5EIXIC','modelName'=>'Nacdaq'],
		//     	 ['index'=>'%5EFTSE','modelName'=>'Ftse'],
		//     	 ['index'=>'000001.SS','modelName'=>'Sse'],
		//     	 ['index'=>'%5EHSI','modelName'=>'Hansen'],
		//     	 ['index'=>'%5EDJI','modelName'=>'DowJones'],
		//     	 ['index'=>'%5EN225','modelName'=>'Nikkei']];

		// foreach ($array as $value) {
  //   	$file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$value['index'].'?interval=1d');
  //   	$data = json_decode($file);
  //   	$values = $data->chart->result[0]->indicators->quote[0];
  //   	$timestamps = $data->chart->result[0]->timestamp;
  //   	$count = count($timestamps) - 1;
  //   	$date = new DateTime();
  //   	$date->setTimestamp($timestamps[$count]);

  //   	$finalDate = $date->format('Y-m-d');
  //   	$closeVal = round($values->close[$count], 2);
  //   	$openVal = round($values->open[$count], 2);
  //   	$lowVal = round($values->low[$count], 2);
  //   	$highVal = round($values->high[$count], 2);
  //       $dataToinsert = ['date'=>$finalDate,'close'=>$closeVal,'open'=>$openVal,'low'=>$lowVal,'high'=>$highVal];
  //   	// dd($dataToinsert);
  //   	$ModelName = $value['modelName'];
  //   	$ModelName = 'App\\'.$ModelName;
  //   	// dd($ModelName);
  //   	$eod = $ModelName::insert($dataToinsert); 
  //       }
  //       dd('data upto date.');
  //   }

    // public function oneDay()
    // {
    //     // $equity = equity::all()->toArray();
    //     // foreach ($equity as $selectEquity) {
    //     //     $scheme_code = $selectEquity['scheme_code'];
    //     //     $amc_name = $selectEquity['amc_name'];
    //     //     $scheme_name = $selectEquity['scheme_name'];  
    //     //     $data = DB::table($amc_name)->where('scheme_code',$scheme_code)->where('date','2018-03-28')->value('nav');
    //     //     $update =  DB::table($amc_name)->insert(['scheme_code'=>$scheme_code,'scheme_name'=>$scheme_name,'date'=>date('Y-m-d',strtotime('2018-03-30')),'nav'=>$data]);
    //     // }
    //     $debt = debt::all()->toArray();
    //     foreach ($debt as $selectDebt) {
    //         $scheme_code = $selectDebt['scheme_code'];
    //         $amc_name = $selectDebt['amc_name'];
    //         $scheme_name = $selectDebt['scheme_name'];  
    //         $data = DB::table($amc_name)->where('scheme_code',$scheme_code)->where('date','2018-03-28')->value('nav');
    //         if (empty($data)) {
    //             $data = DB::table($amc_name)->where('scheme_code',$scheme_code)->where('date','2018-03-29')->value('nav');
    //         }
    //         $update =  DB::table($amc_name)->insert(['scheme_code'=>$scheme_code,'scheme_name'=>$scheme_name,'date'=>date('Y-m-d',strtotime('2018-03-30')),'nav'=>$data]);
    //     }
    //     $bal = balanced::all()->toArray();
    //     foreach ($bal as $selectBal) {
    //         $scheme_code = $selectBal['scheme_code'];
    //         $amc_name = $selectBal['amc_name'];
    //         $scheme_name = $selectBal['scheme_name'];  
    //         $data = DB::table($amc_name)->where('scheme_code',$scheme_code)->where('date','2018-03-28')->value('nav');
    //         if (empty($data)) {
    //             $data = DB::table($amc_name)->where('scheme_code',$scheme_code)->where('date','2018-03-29')->value('nav');
    //         }
    //         $update =  DB::table($amc_name)->insert(['scheme_code'=>$scheme_code,'scheme_name'=>$scheme_name,'date'=>date('Y-m-d',strtotime('2018-03-30')),'nav'=>$data]);
    //     }
    //     dd('work done!!');
    // }

    public function csvUpload()
    {
    	set_time_limit(6000);
    	// $file = file_get_contents('SENSEX.csv');

    	// $file = preg_split("/\r\n|\n|\r/", $file);
    	// foreach ($file as $value) {
    	// 	$data = explode(',', $value);
     //        $date = date('Y-m-d',strtotime($data[0]));
    	// 	$update = HistoricBse::insert(['date'=>$date,'open'=>$data[1],'high'=>$data[2],'low'=>$data[3],'close'=>$data[4]]);
    	// }
    	// dd('done!!!!!');

     //    Sse::where('id','>',0)->delete();

     //    $array = [['index'=>'^IXIC','modelName'=>'Nacdaq'],
     //             ['index'=>'000001.SS','modelName'=>'Sse'],
     //             ['index'=>'^HSI','modelName'=>'Hansen'],
     //             ['index'=>'^DJI','modelName'=>'DowJones'],
     //             ['index'=>'^N225','modelName'=>'Nikkei']];



                $file = fopen('NIFTY.csv',"r");
                $column=fgetcsv($file);
                while(!feof($file)){
                 $rowData[]=fgetcsv($file);
                }
                foreach ($rowData as $key => $value) {
                    if ($value[1] != "-") {

                        HistoricNse::where('date',date('Y-m-d',strtotime($value[0])))->update(['pe'=>$value[1]]);
                    }
                }

        dd('work down...');
    }

    // public function createTable($name)
    // {
    //     Schema::create($name, function(Blueprint $table){
    //         $table->increments('id');
    //         $table->string('scheme_code');
    //         $table->string('scheme_name');
    //         $table->date('date');
    //         $table->double('nav');
    //         $table->timestamps();
    //     });
    // }

    // public function indexTest()
    // {
    //     $client = new \GuzzleHttp\Client();
    //     $res = $client->get('https://www.nseindia.com/live_market/dynaContent/live_watch/stock_watch/niftyStockWatch.json');
    //     $data = json_decode($res->getBody(), true); 
    //     $niftydate = date('Y-m-d', strtotime($data['time']));
    //     $latest_data = $data['latestData'];
    //     $open_nifty = str_replace(',', '', $latest_data[0]['open']);
    //     $high_nifty = str_replace(',', '', $latest_data[0]['high']);
    //     $low_nifty = str_replace(',', '', $latest_data[0]['low']);
    //     $close_hifty = str_replace(',', '', $latest_data[0]['ltp']);
    //     dd($data);

    // }

    // public function getNav()
    // {

    //     // $equity = equity::select('scheme_code','amc_name')->get()->toArray();
    //     // $debt = debt::select('scheme_code','amc_name')->get()->toArray();
    //     // $balanced = balanced::select('scheme_code','amc_name')->get()->toArray();

    //     // $finalArray = array_merge($equity,$debt,$balanced);

    //     $file = file("https://www.amfiindia.com/spages/NAVAll.txt");
    //      // foreach ($finalArray as $searchthis) {
    //         foreach ($file as $line) {
    //             $line_exp = explode(";", $line);
    //             dd($line_exp);
    //             if(count($line_exp) > 6) {
    //                 if($line_exp[0] == $searchthis['scheme_code']) {
    //                     $date = date("Y-m-d",strtotime($line_exp[5]));
    //                     DB::table($searchthis['amc_name'])
    //                                 ->insert(['date' => $date,
    //                                         'scheme_code' => $line_exp[0],
    //                                         'scheme_name' => $line_exp[3],
    //                                         'nav' => $line_exp[4]]);
    //                 }
    //             }
    //         }       
    //     // }
    //     dd('Nav update ..!..');

    // }

    // public function getLiveSmallcap(){

    //     $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5EBSESN&range=1d&interval=5m&indicators=close');
    //     $data = json_decode($file);
    //     // dd($data->spark->result[0]->response[0]->timestamp);
    //     $time = $data->spark->result[0]->response[0]->timestamp;
    //     $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
    //     $processData = [];
    //     for ($i=0; $i < count($time); $i++) { 
    //         $data = [$time[$i],$value[$i]];
    //         array_push($processData, $data);
    //     }
    //     dd($processData);
    //     // return response()->json(['msg' => '1', 'data' => $processData]);
    // }

    // public function goldData(){
    //     set_time_limit(6000);
    //     $getData = file_get_contents('https://www.quandl.com/api/v1/datasets/CHRIS/MCX_GM1.json');
    //     // $getData = file_get_contents('https://www.quandl.com/api/v1/datasets/CHRIS/MCX_SI1.json');
    //     // $getData = file_get_contents('https://www.quandl.com/api/v1/datasets/CHRIS/MCX_NG1.json');
    //     $output = json_decode($getData);
    //     foreach ($output->data as $select) {
    //         // Gold::insert(['date'=>date('Y-m-d',strtotime($select[0])),'price'=>$select[4]]);
    //         Gold::insert(['date'=>date('Y-m-d',strtotime($select[0])),'price'=>$select[1]]);
    //     }
    //     dd('WoW it worked');
    // }

    // public function policy(){
    //     set_time_limit(6000);
    //     $timeing = Carbon::now()->timezone('Asia/Kolkata');
    //     $timenow = date('H:i',strtotime($timeing));
    //     if ($timenow < '15:17') {
    //         dd('now');
    //     }else{
    //         dd('not now'.$timenow);
    //     }


    //     // $getData = file_get_contents('https://www.quandl.com/api/v3/datasets/CURRFX/CHFINR.json?api_key=F4khnuVJdZM7D7Dys3_5');
    //     // $outputs = json_decode($getData);
    //     // $output = $outputs->dataset;
    //     // foreach ($output->data as $select) {
    //     //     Chf::insert(['date'=>date('Y-m-d',strtotime($select[0])),'exchange_value'=>$select[1]]);
    //     // }
    //     // $getData2 = file_get_contents('https://www.quandl.com/api/v3/datasets/CURRFX/EURINR.json?api_key=F4khnuVJdZM7D7Dys3_5');
    //     // $outputs2 = json_decode($getData2);
    //     // $output2 = $outputs2->dataset;
    //     // foreach ($output2->data as $select) {
    //     //     Eur::insert(['date'=>date('Y-m-d',strtotime($select[0])),'exchange_value'=>$select[1]]);
    //     // }

    // }

    // public function removeData()
    // {
    //     $removeEquity = equity::where('id','>',0)->update(['small_cap'=>0,'mid_cap'=>0,'large_cap'=>0,'asset_size'=>0,'exit_load'=>0]);
    //     $removeDebt = debt::where('id','>',0)->update(['AA_return'=>0,'avg_mat'=>0,'mod_duration'=>0,'gov_sec'=>0,'asset_size'=>0,'exit_load'=>0]);
    //     $removeBal = balanced::where('id','>',0)->update(['small_cap'=>0,'mid_cap'=>0,'large_cap'=>0,'asset_size'=>0,'exit_load'=>0,'debt'=>0]);
    //     dd($removeBal,$removeDebt,$removeEquity);
    // }

    public function guzzlePost()
    {
        set_time_limit(90000000);

        $dateArray = ['06072018','07072018','08072018','09072018'];


        foreach ($dateArray as $myDate) {
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://www.crisil.com/content/crisil/en/home/what-we-do/financial-products/indices/_jcr_content/wrapper_100_par/tabs_copy/1/indices.json");
            curl_setopt($ch, CURLOPT_POST, 1);
            
            // -------------------------POSTFIELDS Option 1-----------------------------------
            // curl_setopt($ch, CURLOPT_POSTFIELDS,
                        // "filepath=/etc/crisil_indices/indiangraph05072018/indiangraph05072018&basepath=/content/dam/crisil/indices/Indian");
            
            // -------------------------POSTFIELDS Option 2-----------------------------------
            // in real life you should use something like:
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
                     http_build_query(
                        array(
                            'filepath' => '/etc/crisil_indices/indiangraph'.$myDate.'/indiangraph'.$myDate,
                            'basepath'=>'/content/dam/crisil/indices/Indian'
                            )
                        )
                 );


            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec ($ch);

            curl_close ($ch);

            $output = json_decode($server_output);
            // dd($output);
            $count = 0;
            foreach ($output as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    $dataArray = $value2->graphData;
                    $tablename = Crisil::where('indexname',$key2)->value('tablename');
                    $dataValue = $dataArray[count($dataArray)-1];
                    $dateprice = explode(',', $dataValue);
                    $timestamp = (int)($dateprice[0]/1000);
                    $date = new DateTime();
                    $date->setTimestamp($timestamp);
                    $dateFormated = $date->format('Y-m-d');
                    $inserted_data=array('date'=>$dateFormated,
                                         'price'=>round($dateprice[1],2) 
                                    );
                    if ($dateprice[1] != 0) {
                        DB::table($tablename)->insert($inserted_data);
                    }
                }
            }

        }
        dd('work done!!!!');
        // foreach ($output as $key => $value) {
        //     foreach ($value as $key2 => $value2) {
        //         // $tablename = 'crisil'.$count;
        //         // Crisil::insert(['indices'=>$key,'indexname'=>$key2,'tablename'=>$tablename]);
        //         // Schema::create($tablename, function(Blueprint $table){
        //         //     $table->increments('id');
        //         //     $table->string('date');
        //         //     $table->double('price');
        //         //     $table->timestamps();
        //         // });
        //         $dataArray = $value2->graphData;
        //         // foreach ($dataArray as $dataValue) {
        //         //     if ($dataArray[0] != $dataValue) {
        //         //         $dateprice = explode(',', $dataValue);
        //         //         $timestamp = (int)($dateprice[0]/1000);
        //         //         $date = new DateTime();
        //         //         $date->setTimestamp($timestamp);
        //         //         $dateFormated = $date->format('Y-m-d');
        //         //         $inserted_data=array('date'=>$dateFormated,
        //         //                              'price'=>round($dateprice[1],2) 
        //         //                         );
        //         //         DB::table($tablename)->insert($inserted_data);
        //         //     }
        //         // }
        //         // $count += 1;
        //         $tablename = Crisil::where('indexname',$key2)->get()->value('tablename');
        //         dd($tablename);
        //     }
        // }

        // $output = $this->object_to_array($output);
        // $creditIndices = $this->object_to_array($output['Credit Indices']);
        // $graphData = $creditIndices['CRISIL AAA Long Term Bond Index']->graphData;
        // $myData = $graphData[count($graphData)-1];
        // $dateprice = explode(',', $myData);
        // $timestamp = (int)($dateprice[0]/1000);
        // $date = new DateTime();
        // $date->setTimestamp($timestamp);
        // $dateFormated = $date->format('Y-m-d');
        // dd($dateFormated);
        // $output['Credit Indices']
        // $compositeIndices = $this->object_to_array($output['Composite Indices']);
        // $output['Hybrid Indices']
        // $output['GILT Indices']
        // $output['Money Market Indices']
        // $output['Commodity Indices']
        // $output['SDL Indices']
        // $output['FPI Indices']
        // dd($compositeIndices);


    }

    public function object_to_array($object)
    {
        return (array) $object;
    }


    public function liveIndex()
    {
        $finalDate = [];

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=%5EBSESN&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['sensex']['close'] = $currentClose;
        $finalDate['sensex']['change'] = round($change,2);
        $finalDate['sensex']['per'] = $per;

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=^NSEI&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['nifty']['close'] = $currentClose;
        $finalDate['nifty']['change'] = round($change,2);
        $finalDate['nifty']['per'] = $per;

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-MIDCAP.BO&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['midcap']['close'] = $currentClose;
        $finalDate['midcap']['change'] = round($change,2);
        $finalDate['midcap']['per'] = $per;

        $file = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols=BSE-SMLCAP.BO&range=1d&interval=1m&indicators=close');
        $data = json_decode($file);
        $previousClose = $data->spark->result[0]->response[0]->meta->previousClose;
        $value = $data->spark->result[0]->response[0]->indicators->quote[0]->close;
        $array = array_diff($value, [null]);
        $currentClose = $array[count($array)-1];
        $change = $currentClose-$previousClose;
        $per = round(($change/$previousClose)*100,2);
        $finalDate['smallcap']['close'] = $currentClose;
        $finalDate['smallcap']['change'] = round($change,2);
        $finalDate['smallcap']['per'] = $per;
        
        dd($finalDate);
    }

    public function sectorUpdate()
    {
        // $tableArrya = [['nse_auto','NSE Auto'],
        //                ['nse_bank','NSE Bank'],
        //                ['nse_commodities','NSE Commodities'],
        //                ['nse_consumption','NSE Consumption'],
        //                ['nse_cpse','NSE CSPE'],
        //                ['nse_energy','NSE Enegry'],
        //                ['nse_financ','NSE Financial Service'],
        //                ['nse_fmcg','NSE FMCG'],
        //                ['nse_it','NSE IT'],
        //                ['nse_infra','NSE Infra'],
        //                ['nse_media','NSE Media'],
        //                ['nse_metal','NSE Metal'],
        //                ['nse_pharma','NSE Pharma'],
        //                ['nse_prvbank','NSE Private Bank'],
        //                ['nse_psu','NSE PSU Bank'],
        //                ['nse_realty','NSE Realty'] 
        //            ];
        // foreach ($tableArrya as $key => $value) {
        //     $insert = NseSector::insert(['table'=>$value[0],'name'=>$value[1]]);
        //     if ($insert) {
        //         Schema::create($value[0], function(Blueprint $table){
        //             $table->increments('id');
        //             $table->string('date');
        //             $table->double('close');
        //             $table->double('pe')->nullable();
        //             $table->timestamps();
        //         });
                DB::table('nse_realty')->truncate();
                $file = fopen('1.csv',"r");
                $column=fgetcsv($file);
                while(!feof($file)){
                 $rowData[]=fgetcsv($file);
                }
                foreach ($rowData as $rec) {
                    if ($rec[2] != "-") {
                        DB::table('nse_realty')->insert(['date'=>date('Y-m-d',strtotime($rec[0])),'close'=>$rec[1],'pe'=>$rec[2]]);
                    }else{
                        DB::table('nse_realty')->insert(['date'=>date('Y-m-d',strtotime($rec[0])),'close'=>$rec[1]]);
                    }
                }
            // }
        // }
        dd('work done!!!!');
    }

    public function cronTest()
    {
        $tableArrya = [['nse_auto','%5ECNXAUTO'],
                       ['nse_bank','%5ENSEBANK'],
                       ['nse_commodities','%5ECNXCMDT'],
                       ['nse_consumption','%5ECNXCONSUM'],
                       ['nse_cpse','CPSE.NS'],
                       ['nse_energy','%5ECNXENERGY'],
                       ['nse_financ','%5ECNXFIN'],
                       ['nse_fmcg','%5ECNXFMCG'],
                       ['nse_it','%5ECNXIT'],
                       ['nse_infra','%5ECNXINFRA'],
                       ['nse_media','%5ECNXMEDIA'],
                       ['nse_metal','%5ECNXMETAL'],
                       ['nse_pharama','%5ECNXPHARMA'],
                       ['nse_prvbank','NIFTYPVTBANK.NS'],
                       ['nse_psu','%5ECNXPSUBANK'],
                       ['nse_realty','%5ECNXREALTY'] 
                   ];
        foreach ($tableArrya as $value) {
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$value[1].'?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $timestamps = $data->chart->result[0]->timestamp;
            $count = count($timestamps) - 1;
            $date = new DateTime();
            $date->setTimestamp($timestamps[$count]);

            $finalDate = $date->format('Y-m-d');
            $closeVal = round($values->close[$count], 2);
            DB::table($value[0])->insert(['date'=>$finalDate,'close'=>$closeVal]);
        }
    }

    public function forexUpdate()
    {
        set_time_limit(6000);
        $period = ['2018-08-20','2018-08-21','2018-08-22','2018-08-23','2018-08-24','2018-08-25','2018-08-26','2018-08-27'];
        $currencies = array('USD', 'SGD', 'CNY', 'GBP', 'CHF','EUR');
        // $currency_date = Carbon::now()->subDays(1)->toDateString();
        foreach ($period as $key => $date) {
            $currency_date = $date;
            foreach ($currencies as $value) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://free.currencyconverterapi.com/api/v6/convert?q=".$value."_INR&compact=ultra&date=".$currency_date);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                $output = json_decode($output);
                $inrdetails;
                
                switch($value) {
                    case 'USD': {
                        $inrdetails = new HistoricUsd();
                        break;
                    }
                    case 'SGD': {
                        $inrdetails = new HistoricSgd();
                        break;
                    }
                    case 'GBP': {
                        $inrdetails = new HistoricGbp();
                        break;
                    }
                    case 'CNY': {
                        $inrdetails = new HistoricYen();
                        break;
                    }
                    case 'CHF': {
                        $inrdetails = new Chf();
                        break;
                    }
                    case 'EUR':{
                        $inrdetails = new Eur();                    
                        break;
                    }
                        
                }
                foreach ($output as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        $inrdetails->exchange_value = round($value3,3);
                        $inrdetails->date = $key3;
                        $inrdetails->save();
                    }
                 } 
            }
            echo $currency_date;
        }
        dd('worked!!!');
    }
}
