<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gold;
use App\Silver;
use App\NaturalGas;
use Carbon\Carbon;

class CommoditieController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		// \Debugbar::disable();
	}

    public function index()
    {
		$gold = Gold::orderBy('date','desc')->get()->toArray();
        $gold1 = Gold::where('date','<',$gold[0]['date'])->orderBy('date','desc')->get()->toArray();
		$goldDiff = $gold[0]['price'] - $gold1[0]['price'];
		$goldper = ($goldDiff / $gold[0]['price']) * 100;
		$silver = Silver::orderBy('date','desc')->get()->toArray();
        $silver1 = Silver::where('date','<',$silver[0]['date'])->orderBy('date','desc')->get()->toArray();
		$silverDiff = $silver[0]['price'] - $silver1[0]['price'];
		$silverper = ($silverDiff / $silver[0]['price']) * 100;    
        $gas = NaturalGas::orderBy('date','desc')->get()->toArray();
		$gas1 = NaturalGas::where('date','<',$gas[0]['date'])->orderBy('date','desc')->get()->toArray();
		$gasDiff = $gas[0]['price'] - $gas1[0]['price'];
		$gasper = ($gasDiff / $gas[0]['price']) * 100;
		$commoditieData = [];
		$commoditieData['gold'] = ['price' => round($gold[0]['price'],2),
		                        'diff' => round($goldDiff, 2),
		                        'per' => round($goldper, 2)];
		$commoditieData['silver'] = ['price' => round($silver[0]['price'],2),
		                        'diff' => round($silverDiff, 2),
		                        'per' => round($silverper, 2)];
		$commoditieData['gas'] = ['price' => round($gas[0]['price'],2),
		                        'diff' => round($gasDiff, 2),
		                        'per' => round($gasper, 2)];

    	return view('commodities.commoditie',['commoditieData' => $commoditieData]);
    }

    public function selectedCommoditie(Request $request)
   {
     $performance_data = array();
            $duration = $request['duration'];
            $index = $request['index'];
            $index_range_date = '';
            $day_data = false;
            switch($duration) {
                case '1':
                {
                    $index_range_date = Carbon::now()->subDays(3)->toDateString();
                    break;
                }
                case '2':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subDays(6)->toDateString();
                    break;
                }
                case '4':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                    break;
                }
                case '5':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                }
                case '6':
                {
                    //calculating the range date to prepare data
                    $index_range_date = '';
                    break;
                }
                default :
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                }
            }

            $ModelName = $request['index'];
            $ModelName = 'App\\'.$ModelName;

            if ($index_range_date != '') {
               $index_details = $ModelName::where('date','>=',$index_range_date)->orderBy('date', 'asc')
               ->get()->toArray();
            }else{
               $index_details = $ModelName::orderBy('date', 'asc')
               ->get()->toArray();
            }

            if(!empty($index_details)) {
                foreach($index_details as $index_detail) {
                      
                      $performance_data[] = array(strtotime($index_detail['date'])*1000,round($index_detail['price'], 2));
                    }
                return response()->json(['msg' => '1', 'data' => $performance_data]);
            } else {
                return response()->json(['msg' => '0']);
            }
   }
}
