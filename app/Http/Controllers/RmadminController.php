<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amc;
use App\RmInfo;
use App\User;

class RmadminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$rminfo = RmInfo::get()->toArray();
    	return view('rmdash.rmdash',['rminfo'=>$rminfo]);
    }

    public function getRmAmc()
    {
    	$amc_ext = RmInfo::pluck('amc_name')->toArray();
    	if (count($amc_ext) != 0) {
	    	$amc_names = Amc::where('name','!=',$amc_ext)->orderBy('name')->pluck('name')->toArray();
    	}else{
	    	$amc_names = Amc::orderBy('name')->pluck('name')->toArray();
    	}
		return response()->json(['msg'=>1,'amc'=>$amc_names]);
    }

    public function addRmuser(Request $request)
    {
    	$create_user = User::create(['name'=>$request['rm_name'],'email'=>$request['mail_id'],'password'=>bcrypt($request['password'])]);
    	if ($create_user) {
    		$user_id = User::where('email',$request['mail_id'])->value('id');
    		$user_update = User::where('id',$user_id)->update(['role'=>3]);
    		$create_rm = new RmInfo();
    		$create_rm->user_id=$user_id;
			$create_rm->amc_name=$request['amc_name'];
			$create_rm->rm_name=$request['rm_name'];
			$create_rm->rm_progress='0';
			$create_rm->password=$request['password'];
			$create_rm->mail_id=$request['mail_id'];
			$create_rm->mobile=$request['mobile'];
			$create_rm->save();
    		if ($create_rm) {
    			return response()->json(['msg'=>1]);
    		}
    	}
    }

    public function deleteRmuser(Request $request)
    {
    	$delete_user = RmInfo::where('user_id',$request['user_id'])->delete();
    	$delete_data = User::where('id',$request['user_id'])->delete();
    	return response()->json(['msg'=>1]);
    }


    public function getRmInfo(Request $request)
    {
        $info = RmInfo::where('user_id',$request['user_id'])->get()->toArray();
        // http_response_code(500);
        // dd($info);
        return response()->json(['msg'=>1,'info'=>$info[0]]);
    }

    public function updateUser(Request $request)
    {
        $update = User::where('id',$request['user_id'])->update(['name'=>$request['rm_name'],'email'=>$request['mail_id']]);
        if ($update) {
            $infoUpdate = RmInfo::where('user_id',$request['user_id'])->update(['rm_name'=>$request['rm_name'],'mail_id'=>$request['mail_id'],'mobile'=>$request['mobile']]);
            if ($infoUpdate) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'rminfo not update']);
            }
        }else{
            return response()->json(['msg'=>0,'info'=>'user info not update']);
        }
    }

    public function changePass(Request $request)
    {
        $update = User::where('id',$request['user_id'])->update(['password'=>bcrypt($request['password'])]);
        if ($update) {
            $rmUpdate = RmInfo::where('user_id',$request['user_id'])->update(['password'=>$request['password']]);
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }
    }

}
