<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crisil;
use Carbon\Carbon;
use DB;

class CrisilController extends Controller
{
    public function index()
    {
    	$data = Crisil::all()->groupBy('indices')->toArray();
    	// dd($data);
    	return view('crisil.crisil',['value'=>$data]);
    }

   	public function getGraph(Request $request)
   	{
   		// dd($request->all());
   		$duration = $request['duration'];
   		$tablename = $request['data'];
   		switch ($duration) {
                case '1':
                    $index_range_date = Carbon::now()->subDays(6)->toDateString();
                    break;
                case '2':
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                case '3':
                    $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                    break;
                case '4':
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                case '5':
                    $index_range_date = Carbon::now()->subMonths(36)->toDateString();
                    break;
                case '6':
                    $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                    break;
                default:
                    $index_range_date = '';
                    break;
            }
			if ($index_range_date == '') {
			    $graphData = DB::table($tablename)->orderBy('date','asc')->get()->toArray();
			}else{
			    $graphData = DB::table($tablename)->where('date','>=',$index_range_date)->orderBy('date','asc')->get()->toArray();
			}

            if(!empty($graphData)) {
            	$perVal = $graphData[0]->price;
            	$curVal = $graphData[count($graphData)-1]->price;
            	$return = (($curVal-$perVal)/$perVal)*100;
                foreach($graphData as $crysildata) {
                      $performance_data[] = array(strtotime($crysildata->date)*1000,$crysildata->price);
                    }
                return response()->json(['msg' => '1', 'data' => $performance_data, 'return'=>round($return,2)]);
            } else {
                return response()->json(['msg' => '0']);
            }

   	}
}
