<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistoricBse;
use App\HistoricMidcap;
use App\HistoricSmallcap;
use App\HistoricNse;
use Carbon\Carbon;
use App\BseAuto;
use App\BseHealthCare;
use App\BseInfra;
use App\BsePower;
use App\BseEnergy;
use App\BseBanking;
use App\NseSmallCap;
use App\NseMidCap;
use App\NseSector;
use DB;

class TvController extends Controller
{
    // public function __construct()
    // {
    // 	$this->middleware('auth');
    // }

    public function index($type = 'web')
    {
        $Mr = Carbon::now()->subMonths(1)->toDateString();
        $threeMr = Carbon::now()->subMonths(3)->toDateString();
        $Yr = Carbon::now()->subMonths(12)->toDateString();
        $threeYr = Carbon::now()->subMonths(36)->toDateString();

        $bse = HistoricBse::where('date', '>=', $threeMr)
                    ->orderBy('date', 'desc')->get()->toArray();
        $smallcap = NseSmallCap::where('date', '>=', $threeMr)
                    ->orderBy('date', 'desc')->get()->toArray();
        $midcap = NseMidCap::where('date', '>=', $threeMr)
                    ->orderBy('date', 'desc')->get()->toArray();
        $nse = HistoricNse::where('date', '>=', $threeMr)
                    ->orderBy('date', 'desc')->get()->toArray();

        foreach ($bse as $selected) {
            if ($bse[0]['date'] > $selected['date']) {
                $bseDiff = $bse[0]['close'] - $selected['close'];
                $bsePer = ($bseDiff / $bse[0]['close'])*100;
                $bse_val = ['points' => round($bse[0]['close'], 2),
                           'Diff' => round($bseDiff, 2),
                           'Per' => round($bsePer, 2)];
                break;
            }
        }
        foreach ($nse as $selected) {
            if ($nse[0]['date'] > $selected['date']) {
                $nseDiff = $nse[0]['close'] - $selected['close'];
                $nsePer = ($nseDiff / $nse[0]['close'])*100;
                $nse_val = ['points' => round($nse[0]['close'], 2),
                         'Diff' => round($nseDiff, 2),
                         'Per' => round($nsePer, 2)];
                break;
            }
        }
        foreach ($smallcap as $selected) {
            if ($smallcap[0]['date'] > $selected['date']) {
                $smallcapDiff = $smallcap[0]['close'] - $selected['close'];
                $smallcapPer = ($smallcapDiff / $smallcap[0]['close'])*100;
                $smallcap_val = ['points' => round($smallcap[0]['close'], 2),
                              'Diff' => round($smallcapDiff, 2),
                              'Per' => round($smallcapPer, 2)];
                break;
            }
        }
        foreach ($midcap as $selected) {
            if ($midcap[0]['date'] > $selected['date']) {
                $midcapDiff = $midcap[0]['close'] - $selected['close'];
                $midcapPer = ($midcapDiff / $midcap[0]['close'])*100;
                $midcap_val = ['points' => round($midcap[0]['close'], 2),
                            'Diff' => round($midcapDiff, 2),
                            'Per' => round($midcapPer, 2)];
                break;
            }
        }
        $dbArray = NseSector::all()->toArray();
        // dd($Mr,$Yr,$threeYr);
        $sector = [];
        foreach ($dbArray as $key => $value) {
            $sector[$value['name']] = DB::table($value['table'])->orderBy('date', 'desc')->get()->toArray();
        }
        // dd($sector);
        // $sector['Auto'] = BseAuto::orderBy('date','desc')->get();
        // $sector['Healthcare'] = BseHealthCare::orderBy('date','desc')->get();
        // $sector['Infra'] = BseInfra::orderBy('date','desc')->get();
        // $sector['Power'] = BsePower::orderBy('date','desc')->get();
        // $sector['Energy'] = BseEnergy::orderBy('date','desc')->get();
        // $sector['Banking'] = BseBanking::orderBy('date','desc')->get();
        $DataArray = [];
        foreach ($sector as $index => $data1) {
            $month_val = 0;
            $year_val = 0;
            $three_year_val = 0;
            $pe = 0;
            foreach ($data1 as $pickedData) {
                if ($pickedData->pe != null) {
                    if ($pe == 0) {
                        $pe = 1;
                        $DataArray[$index]['pe'] = $pickedData->pe;
                        $DataArray[$index]['pedate'] = $pickedData->date;
                    }
                }
                if ($threeYr >= $pickedData->date) {
                    if ($three_year_val == 0) {
                        $three_year_val = 1;
                        $three_year_return = (($data1[0]->close-$pickedData->close)/$pickedData->close)*100;
                        $DataArray[$index]['three_year_return'] = round($three_year_return, 2);
                    }
                } elseif ($Yr >= $pickedData->date) {
                    if ($year_val == 0) {
                        $year_val = 1;
                        $year_return = (($data1[0]->close-$pickedData->close)/$pickedData->close)*100;
                        $DataArray[$index]['year_return'] = round($year_return, 2);
                    }
                } elseif ($Mr >= $pickedData->date) {
                    if ($month_val == 0) {
                        $month_val = 1;
                        $month_return = (($data1[0]->close-$pickedData->close)/$pickedData->close)*100;
                        $DataArray[$index]['month_return'] = round($month_return, 2);
                    }
                }
            }
        }
        // dd($DataArray);
        return view('livetv.tvmodule', ['sensex'=>$bse_val,'nifty'=>$nse_val,'smallcap'=>$smallcap_val,'midcap'=>$midcap_val,'sector'=>$DataArray,'type'=>$type]);
    }

    public function tvGraph(Request $request)
    {
        $index_range_date = '';
        switch ($request['duration']) {
                case '1':
                {
                    $index_range_date = Carbon::now()->subDays(6)->toDateString();
                    break;
                }
                case '2':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                    break;
                }
                case '3':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                    break;
                }
                case '4':
                {
                    $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                    break;
                }
                case '5':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(36)->toDateString();
                    break;
                }
                case '6':
                {
                    //calculating the range date to prepare data
                    $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
                default:
                {
                    //calculating the range date to prepare data
                    $index_range_date = '';//Carbon::now()->subMonths(60)->toDateString();
                    break;
                }
        }
        $index['sensex'] = HistoricBse::where('date', '>=', $index_range_date)
                    ->orderBy('date', 'asc')->get()->toArray();
        $index['nifty'] = HistoricNse::where('date', '>=', $index_range_date)
                    ->orderBy('date', 'asc')->get()->toArray();
        $index['smallcap'] = NseSmallCap::where('date', '>=', $index_range_date)
                    ->orderBy('date', 'asc')->get()->toArray();
        $index['midcap'] = NseMidCap::where('date', '>=', $index_range_date)
                    ->orderBy('date', 'asc')->get()->toArray();
        $returns = [];
        $data = [];
        $peData = [];
        foreach ($index as $key => $value) {
            $count = count($value)-1;
            $todayIndex = $value[$count]['close'];
            $t2Index = $value[0]['close'];
            $diffVall = $todayIndex - $t2Index;
            $indexPer = ($diffVall/$t2Index)*100;
            $returns[$key] = round($indexPer, 2);
            $performance_data = [];
            $pedata2 = [];
            $cummilative_return = 0;
            $currentIndex = '';
            $preIndex = '';
            foreach ($value as $index_detail) {
                if (empty($preIndex)) {
                    $preIndex = $index_detail;
                    $performance_data[] = array(strtotime($index_detail['date'])*1000,round($index_detail['close'], 2));
                    if ($index_detail['pe'] != null) {
                        $pedata2[] = array(strtotime($index_detail['date'])*1000,(float)$index_detail['pe']);
                    }
                } else {
                    $currentIndex = $index_detail;
                    $performance_data[] = array(strtotime($index_detail['date'])*1000,round($index_detail['close'], 2));
                    if ($index_detail['pe'] != null) {
                        $pedata2[] = array(strtotime($index_detail['date'])*1000,(float)$index_detail['pe']);
                    }
                }
                $preIndex = $index_detail;
            }
            $data[$key] = $performance_data;
            $peData[$key] = $pedata2;
        }
        return response()->json(['msg'=>1,'per'=>$returns,'data'=>$data,'pedata'=>$peData]);
    }

    public function addIndexPe(Request $request)
    {
        $index = $request->all();
        $modalName = '';
        foreach ($index as $key => $value) {
            if ($key != 'date') {
                switch ($key) {
                    case 'sensex':
                        $modalName = 'HistoricBse';
                        break;
                    case 'nifty':
                        $modalName = 'HistoricNse';
                        break;
                    case 'smallcap':
                        $modalName = 'NseSmallCap';
                        break;
                    case 'midcap':
                        $modalName = 'NseMidCap';
                        break;
                }
                $modalName = 'App\\'.$modalName;
                if ($value != null) {
                    $update = $modalName::where('date', date('Y-m-d', strtotime($request['date'])))->update(['pe'=>$value]);
                }
            }
        }
        return response()->json(['msg'=>1,'response'=>'Successfully Updated.']);
    }

    public function newTv($tv = 1)
    {
        $range = ['3d','3mo','3y'];
        $interval = ['1h','1d','1d'];
        $finalLeft = [];
        $finalRight = [];
        if ($tv == 1) {
            $leftIndex = '%5EBSESN';
            $leftTitle = 'SENSEX';
            $rightTitle = 'NIFTY';
            $rightIndex = '%5ENSEI';
        } else {
            $leftIndex = '%5ECNXSC';
            $rightIndex = '%5ECRSMID';
            $leftTitle = 'NSE SMALL CAP';
            $rightTitle = 'NSE MID CAP';
        }
        foreach ($range as $key => $value) {
            $fileL = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols='.$leftIndex.'&range='.$value.'&interval='.$interval[$key].'&indicators=close');
            $dataL = json_decode($fileL);
            $timeL = $dataL->spark->result[0]->response[0]->timestamp;
            $valueL = $dataL->spark->result[0]->response[0]->indicators->quote[0]->close;
            $left = [];
            for ($i=0; $i < count($timeL); $i++) {
                if($valueL[$i] != null){
                    $data = [(($timeL[$i] +19800)* 1000),$valueL[$i]];
                    array_push($left, $data);
                }
            }
            $finalLeft[$value] = $left;

            $fileR = file_get_contents('https://query1.finance.yahoo.com/v7/finance/spark?symbols='.$rightIndex.'&range='.$value.'&interval='.$interval[$key].'&indicators=close');
            $dataR = json_decode($fileR);
            $timeR = $dataR->spark->result[0]->response[0]->timestamp;
            $valueR = $dataR->spark->result[0]->response[0]->indicators->quote[0]->close;
            $right = [];
            for ($i=0; $i < count($timeR); $i++) {
                if ($valueR[$i] != null) {
                    $data = [(($timeR[$i] +19800)* 1000),$valueR[$i]];
                    array_push($right, $data);
                }
            }
            $finalRight[$value] = $right;
        }

        return view('livetv.tv-screen')->with(compact('finalRight','finalLeft','leftTitle','rightTitle'));
    }
}
