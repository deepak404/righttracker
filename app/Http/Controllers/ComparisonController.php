<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\equity;
use App\debt;
use App\balanced;
use App\HistoricBse;
use App\HistoricSmallcap;
use App\HistoricMidcap;
use App\HistoricNse;
use App\BseAuto;
use App\BseHealthCare;
use App\BseInfra;
use App\BsePower;
use App\BseEnergy;
use App\BseBanking;
use App\NseSmallCap;
use App\NseMidCap;

class ComparisonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // \Debugbar::disable();
    }

    public function comparison()
    {
        return view('comparison.comparison');
    }

    public function temp()
    {
        return view('comparison.temp');
    }

    public function getFunds(Request $request)
    {
        // $validator = \Validator::make($request->all(),[
        //     'search-input' => 'required',
        // ]);

        if ($request['search-input'] == '') {
            return response()->json([
                ['scheme_code'=>'bse','fund_type'=>'0','scheme_name'=>'Sensex'],
                ['scheme_code'=>'nse','fund_type'=>'0','scheme_name'=>'Nifty'],
                ['scheme_code'=>'nse-s','fund_type'=>'0','scheme_name'=>'NSE SmallCap'],
                ['scheme_code'=>'nse-m','fund_type'=>'0','scheme_name'=>'NSE MidCap'],
                ['scheme_code'=>'small','fund_type'=>'0','scheme_name'=>'BSE SmallCap'],
                ['scheme_code'=>'mid','fund_type'=>'0','scheme_name'=>'BSE MidCap'],
                ['scheme_code'=>'fd','fund_type'=>'0','scheme_name'=>'FD'],
                ['scheme_code'=>'gov','fund_type'=>'0','scheme_name'=>'Gov. Bond'],
                ['scheme_code'=>'save','fund_type'=>'0','scheme_name'=>'Savings'],
                ['scheme_code'=>'auto','fund_type'=>'0','scheme_name'=>'BSE Auto'],
                ['scheme_code'=>'health','fund_type'=>'0','scheme_name'=>'BSE Health Care'],
                ['scheme_code'=>'infra','fund_type'=>'0','scheme_name'=>'BSE Infra'],
                ['scheme_code'=>'power','fund_type'=>'0','scheme_name'=>'BSE Power'],
                ['scheme_code'=>'energy','fund_type'=>'0','scheme_name'=>'BSE Energy'],
                ['scheme_code'=>'bank','fund_type'=>'0','scheme_name'=>'BSE Banking']]);
        } else {
            $funds = $request['search-input'];

            $equity = equity::where('scheme_name', 'LIKE', '%'.$funds.'%')
                ->get()->toArray();
            $debt = debt::where('scheme_name', 'LIKE', '%'.$funds.'%')
                ->get()->toArray();
            $balanced = balanced::where('scheme_name', 'LIKE', '%'.$funds.'%')
                ->get()->toArray();


            $finalArray = array_merge($equity, $debt, $balanced);

            if (empty($finalArray)) {
                return response()->json([
                    ['scheme_code'=>'bse','fund_type'=>'0','scheme_name'=>'Sensex'],
                    ['scheme_code'=>'nse','fund_type'=>'0','scheme_name'=>'Nifty'],
                    ['scheme_code'=>'nse-s','fund_type'=>'0','scheme_name'=>'NSE SmallCap'],
                    ['scheme_code'=>'nse-m','fund_type'=>'0','scheme_name'=>'NSE MidCap'],
                    ['scheme_code'=>'small','fund_type'=>'0','scheme_name'=>'SmallCap'],
                    ['scheme_code'=>'mid','fund_type'=>'0','scheme_name'=>'MidCap'],
                    ['scheme_code'=>'fd','fund_type'=>'0','scheme_name'=>'FD'],
                    ['scheme_code'=>'gov','fund_type'=>'0','scheme_name'=>'Gov. Bond'],
                    ['scheme_code'=>'save','fund_type'=>'0','scheme_name'=>'Savings'],
                    ['scheme_code'=>'auto','fund_type'=>'0','scheme_name'=>'BSE Auto'],
                    ['scheme_code'=>'health','fund_type'=>'0','scheme_name'=>'BSE Health Care'],
                    ['scheme_code'=>'infra','fund_type'=>'0','scheme_name'=>'BSE Infra'],
                    ['scheme_code'=>'power','fund_type'=>'0','scheme_name'=>'BSE Power'],
                    ['scheme_code'=>'energy','fund_type'=>'0','scheme_name'=>'BSE Energy'],
                    ['scheme_code'=>'bank','fund_type'=>'0','scheme_name'=>'BSE Banking']]);
            }

            return json_encode($finalArray);
        }
    }

    public function getData(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'scheme_code' => 'required',
            'fund_type' => 'required',
            'dur' => 'required',
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        } else {
            $scheme_info = '';
            $amc_name = '';
            $scheme_name = '';
            $scheme_value = 0;
            $scheme_per = 0;
            $scheme_details = [];
            $finalData = [];
            $countD = 0;
            $scheme_codes = explode('|', $request['scheme_code']);
            $fund_types = explode('|', $request['fund_type']);
            $duration = $request['dur'];
            $index_range_date = '';
            switch ($duration) {
                            case '1':
                            {
                                $index_range_date = Carbon::now()->subMonths(36)->toDateString();;
                                break;
                            }
                            case '2':
                            {
                                //calculating the range date to prepare data
                                $index_range_date = Carbon::now()->subDays(6)->toDateString();
                                break;
                            }
                            case '3':
                            {
                                //calculating the range date to prepare data
                                $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                                break;
                            }
                            case '4':
                            {
                                //calculating the range date to prepare data
                                $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                                break;
                            }
                            case '5':
                            {
                                //calculating the range date to prepare data
                                $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                                break;
                            }
                            case '6':
                            {
                                //calculating the range date to prepare data
                                $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                                break;
                            }
                            case '7':
                            {
                                //calculating the range date to prepare data
                                $index_range_date = '';//Carbon::now()->subMonths(60)->toDateString();
                                break;
                            }
                            case '8':
                            {
                                $index_range_date = Carbon::now()->subMonths(6)->toDateString();
                                break;
                            }
                            case '24':
                            {
                                $index_range_date = Carbon::now()->subMonths(24)->toDateString();
                                break;
                            }
            }
            foreach ($scheme_codes as $value) {
                if ($value != "") {
                    if ($fund_types[$countD] != 0) {
                        if ($fund_types[$countD] == 1) {
                            $scheme_info = equity::where('scheme_code', $value)->get()->toArray();
                        } elseif ($fund_types[$countD] == 2) {
                            $scheme_info = debt::where('scheme_code', $value)->get()->toArray();
                        } elseif ($fund_types[$countD] == 3) {
                            $scheme_info = balanced::where('scheme_code', $value)->get()->toArray();
                        }
                        $amc_name = $scheme_info[0]['amc_name'];
                        $scheme_name = $scheme_info[0]['scheme_name'];
                        $performance_data = array();
                        if ($duration == 9) {
                            $data = DB::table($amc_name)->where('scheme_code', $value)->where('date', '>=', '2018-02-09')->where('date', '<=', '2018-07-19')->orderBy('date', 'asc')->get()->toArray();
                        } else {
                            $data = DB::table($amc_name)->where('scheme_code', $value)->where('date', '>=', $index_range_date)->orderBy('date', 'asc')->get()->toArray();
                        }
                        // var_dump($data);
                        // die();
                        $count = count($data)-1;
                        $todayNav = $data[$count]->nav;
                        // $data2 = DB::table($amc_name)
                        //             ->where('scheme_code',$value)
                        //             ->where('date','<',$data[$count]->date)
                        //             ->orderBy('date','desc')
                        //             ->get();
                        $daySubOne = $data[0]->nav;
                        $scheme_value = $todayNav - $daySubOne;
                        $scheme_per = ($scheme_value/$daySubOne)*100;
                        $scheme_details[] = ['scheme_code' => $value,
                                            'scheme_name' => $scheme_name,
                                            'current_value' => $todayNav,
                                            'current_per' => round($scheme_per, 2),
                                            'fund_type' => $fund_types[$countD]];
                        $performance_data = [];
                        $cummilative_return = 0;
                        $currentIndex = '';
                        $preIndex = '';
                        foreach ($data as $index_detail) {
                            if (empty($preIndex)) {
                                $preIndex = $index_detail;
                                $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
                            } else {
                                $currentIndex = $index_detail;
                                $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
                                $cummilative_return += $day_ratio;
                                $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
                            }
                            $preIndex = $index_detail;
                        }
                        $finalData[$value] = $performance_data;
                    } else {
                        if ($fund_types[$countD] == 0) {
                            switch ($value) {
                                case 'bse':
                                    $model = 'HistoricBse';
                                    $name = 'Sensex';
                                    break;
                                case 'nse':
                                    $model = 'HistoricNse';
                                    $name = 'Nifty';
                                    break;
                                case 'small':
                                    $model = 'HistoricSmallcap';
                                    $name = 'BSE Small Cap';
                                    break;
                                case 'mid':
                                    $model = 'HistoricMidcap';
                                    $name = 'BSE Mid Cap';
                                    break;
                                case 'auto':
                                    $model = 'BseAuto';
                                    $name = 'BSE Auto';
                                    break;
                                case 'infra':
                                    $model = 'BseInfra';
                                    $name = 'BSE Infra';
                                    break;
                                case 'health':
                                    $model = 'BseHealthCare';
                                    $name = 'BSE Health Care';
                                    break;
                                case 'energy':
                                    $model = 'BseEnergy';
                                    $name = 'BSE Energy';
                                    break;
                                case 'power':
                                    $model = 'BsePower';
                                    $name = 'BSE Power';
                                    break;
                                case 'bank':
                                    $model = 'BseBanking';
                                    $name = 'BSE Banking';
                                    break;
                                case 'nse-s':
                                    $model = 'NseSmallCap';
                                    $name = 'NSE Small Cap';
                                    break;
                                case 'nse-m':
                                    $model = 'NseMidCap';
                                    $name = 'NSE Mid Cap';
                                    break;
                            }
                            if (!empty($model)) {
                                $todayData = date('Y-m-d', strtotime(Carbon::now()));
                                $ModalName = 'App\\'.$model;
                                $indexData = '';
                                if ($duration == 9) {
                                    $indexData = $ModalName::where('date', '>=', '2018-02-09')->where('date', '<=', '2018-07-19')->orderBy('date', 'asc')->get()->toArray();
                                } else {
                                    $indexData = $ModalName::where('date', '>=', $index_range_date)->orderBy('date', 'asc')->get()->toArray();
                                }
                                $count = count($indexData)-1;
                                $todayIndex = $indexData[$count]['close'];
                                // $indexData2 = '';
                                // $indexData2 = $ModalName::where('date','<',$indexData[$count]['date'])->orderBy('date', 'desc')->get()->toArray();
                                $t2Index = $indexData[0]['close'];
                                $diffVall = $todayIndex - $t2Index;
                                $indexPer = ($diffVall/$t2Index)*100;
                                $scheme_details[] = ['scheme_code' => $value,
                                                    'scheme_name' => $name,
                                                    'current_value' => $todayIndex,
                                                    'current_per' => round($indexPer, 2),
                                                    'fund_type' => $fund_types[$countD]];
                                $performance_data2 = [];
                                $cummilative_return = 0;
                                $currentIndex = '';
                                $preIndex = '';
                                foreach ($indexData as $index_detail) {
                                    if (empty($preIndex)) {
                                        $preIndex = $index_detail;
                                        $performance_data2[] = array(strtotime($index_detail['date'])*1000,0,round($index_detail['close'], 2));
                                    } else {
                                        if ($index_detail['date'] != $todayData) {
                                            $currentIndex = $index_detail;
                                            $day_ratio = (($currentIndex['close'] - $preIndex['close'])/$preIndex['close'])*100;
                                            $cummilative_return += $day_ratio;
                                            $performance_data2[] = array(strtotime($index_detail['date'])*1000,round($cummilative_return, 2),round($index_detail['close'], 2));
                                        }
                                    }
                                    $preIndex = $index_detail;
                                }
                                $finalData[$value] = $performance_data2;
                            } else {
                                switch ($value) {
                                    case 'fd':
                                        $type_data = 'fd';
                                        $name = 'FD';
                                        $rate = 0.0178571428;
                                        break;
                                    case 'gov':
                                        $type_data = 'gov';
                                        $name = 'Gov. Bond';
                                        $rate = 0.0212328767;
                                        break;
                                    case 'save':
                                        $type_data = 'save';
                                        $name = 'Savings';
                                        $rate = 0.0095890410;
                                        break;
                                }
                                if ($duration == '6' || $duration == '7') {
                                    $time_dis = 1827;
                                } elseif ($duration == '5') {
                                    $time_dis = 365;
                                } elseif ($duration == '4') {
                                    $time_dis = 92;
                                } elseif ($duration == '3') {
                                    $time_dis = 30;
                                } elseif ($duration == '2') {
                                    $time_dis = 5;
                                } elseif ($duration == '1') {
                                    $time_dis = 1095;
                                }
                                $processData = [];
                                $data = 100;
                                $timeData = Carbon::now()->subDays($time_dis)->toDateString();
                                $processData[] = array(strtotime($timeData)*1000,0,100);
                                $val = $time_dis;
                                for ($i=1; $i < $time_dis; $i++) {
                                    $val -= 1;
                                    $timeData = Carbon::now()->subDays($val)->toDateString();
                                    // $data = $data*(((6.5/100)+1)^($i/365));
                                    // $per = 0.06*$data;
                                    $data = $data+$rate;
                                    $diff = round(($data-100), 2);
                                    // $diff = round((($data-1)/1)*100);
                                    $processData[] = array(strtotime($timeData)*1000,$diff,round($data, 2));
                                }
                                $scheme_details[] = ['scheme_code' => $value,
                                                    'scheme_name' => $name,
                                                    'current_value' => round($data, 2),
                                                    'current_per' => $diff,
                                                    'fund_type' => $type_data];
                                $finalData[$value] = $processData;
                            }
                        }
                    }
                }
                $countD += 1;
            }
            usort($scheme_details, function ($a, $b) {
                return $a['scheme_code'] <=> $b['scheme_code'];
            });
            return response()->json(['msg'=>1,'data'=>$finalData, 'scheme_details'=>$scheme_details]);
        }
    }

    public function topFunds(Request $request)
    {
        if ($request['cat'] != 'lp') {
            $funds = $this->getFundsData($request['cat'], $request['dur']);
            $scheme_details = [];
            $finalData = [];
            $topCount = 1;
            foreach ($funds as $value) {
                if ($topCount < 9) {
                    $performance_data = array();
                    $data = DB::table($value['amc_name'])->where('scheme_code', $value['scheme_code'])->where('date', '>=', $value['range'])->orderBy('date', 'asc')->get()->toArray();

                    $count = count($data)-1;
                    $todayNav = $data[$count]->nav;
                    $daySubOne = $data[0]->nav;

                    $scheme_value = $todayNav - $daySubOne;
                    $scheme_per = ($scheme_value/$daySubOne)*100;
                    $scheme_details[] = ['scheme_code' => $value['scheme_code'],
                                        'scheme_name' => $value['scheme_name'],
                                        'current_value' => $todayNav,
                                        'current_per' => round($scheme_per, 2),
                                        'fund_type' => $value['fund_type']];
                    $performance_data = [];
                    $cummilative_return = 0;
                    $currentIndex = '';
                    $preIndex = '';
                    foreach ($data as $index_detail) {
                        if (empty($preIndex)) {
                            $preIndex = $index_detail;
                        } else {
                            $currentIndex = $index_detail;
                            $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
                            $cummilative_return += $day_ratio;
                            $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
                        }
                        $preIndex = $index_detail;
                    }
                    $finalData[$value['scheme_code']] = $performance_data;
                    $topCount += 1;
                }
            }
            usort($scheme_details, function ($a, $b) {
                return $a['scheme_code'] <=> $b['scheme_code'];
            });
            return response()->json(['msg'=>1,'data'=>$finalData, 'scheme_details'=>$scheme_details]);
        } else {
            $arbitrage = ['105968','113345','108845'];
            $lp = ['101048','101993','100898'];
            $equity = [];
            $debt = [];
            $finalData = [];
            $scheme_details = [];
            foreach ($arbitrage as $value) {
                $equity[] = equity::where('scheme_code', $value)->get()->toArray();
            }
            foreach ($lp as $value) {
                $debt[] = debt::where('scheme_code', $value)->get()->toArray();
            }
            $schemes = array_merge($equity, $debt);
            foreach ($schemes as $value1) {
                $performance_data = array();
                $data = DB::table($value1[0]['amc_name'])->where('scheme_code', $value1[0]['scheme_code'])->orderBy('date', 'asc')->get()->toArray();

                $count = count($data)-1;
                $todayNav = $data[$count]->nav;
                $daySubOne = $data[0]->nav;

                $scheme_value = $todayNav - $daySubOne;
                $scheme_per = ($scheme_value/$daySubOne)*100;
                $scheme_details[] = ['scheme_code' => $value1[0]['scheme_code'],
                                    'scheme_name' => $value1[0]['scheme_name'],
                                    'current_value' => $todayNav,
                                    'current_per' => round($scheme_per, 2),
                                    'fund_type' => $value1[0]['fund_type']];
                $performance_data = [];
                $cummilative_return = 0;
                $currentIndex = '';
                $preIndex = '';
                foreach ($data as $index_detail) {
                    if (empty($preIndex)) {
                        $preIndex = $index_detail;
                    } else {
                        $currentIndex = $index_detail;
                        $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
                        $cummilative_return += $day_ratio;
                        $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
                    }
                    $preIndex = $index_detail;
                }
                $finalData[$value1[0]['scheme_code']] = $performance_data;
            }
        }
        return response()->json(['msg'=>1,'data'=>$finalData, 'scheme_details'=>$scheme_details]);
    }

    public function getFundsData($classification, $duration)
    {
        $Today = Carbon::now()->toDateString();
        $Dr = Carbon::now()->subDays(2)->toDateString();
        $Wr = Carbon::now()->subDays(6)->toDateString();
        $Mr = Carbon::now()->subMonths(1)->toDateString();
        $Three_Mr = Carbon::now()->subMonths(3)->toDateString();
        $Yr = Carbon::now()->subMonths(12)->toDateString();
        $Three_Yr = Carbon::now()->subMonths(36)->toDateString();
        $five_Yr = Carbon::now()->subMonths(60)->toDateString();
        $selected_date = '';
        // $current_sensex = HistoricBse::orderBy('date','DESC')->get()->toArray();
        // $one_year_sensex = HistoricBse::where('date','<=',$Yr)->orderBy('date','DESC')->get()->toArray();

        // if ($classification == 'sm') {
        //     $data1 = equity::where('classification','small cap')->get()->toArray();
        //     $data2 = equity::where('classification','mid cap')->get()->toArray();
        //     $schemes = array_merge($data1,$data2);
        // }else{
        //     $schemes = equity::where('classification', $classification)->get()->toArray();
        // }
        switch ($duration) {
            case '2':
                $selected_date = $Wr;
                break;
            case '3':
                $selected_date = $Mr;
                break;
            case '4':
                $selected_date = $Three_Mr;
                break;
            case '5':
                $selected_date = $Yr;
                break;
            case '1':
                $selected_date = $Three_Yr;
                break;
            default:
                $selected_date = $five_Yr;
                break;
        }
        $equityData = equity::where('classification', $classification)->get()->toArray();
        $debtData = debt::where('classification', $classification)->get()->toArray();
        $balanced = balanced::where('classification', $classification)->get()->toArray();
        $schemes = array_merge($equityData, $debtData, $balanced);
        $scheme_info = [];
        foreach ($schemes as $scheme) {
            $DataArray = [];
            $day_val = 0;
            $week_val = 0;
            $month_val = 0;
            $three_month_val = 0;
            $year_val = 0;
            $three_year_val = 0;
            $five_year_val = 0;

            $DataArray['scheme_name'] = $scheme['scheme_name'];
            $DataArray['scheme_code'] = $scheme['scheme_code'];
            $DataArray['amc_name'] = $scheme['amc_name'];
            $DataArray['fund_type'] = $scheme['fund_type'];
            $DataArray['range'] = $selected_date;

            $amc_name = $scheme['amc_name'];
            $scheme_code = $scheme['scheme_code'];
            $historicData = DB::table($amc_name)->where('scheme_code', $scheme_code)
                                    ->orderBy('date', 'dese')
                                    ->get()->toArray();
            $DataArray['todays_nav'] = $historicData[0]->nav;
            $DataArray['five_year_return'] = 0;
            $DataArray['three_year_return'] = 0;
            $DataArray['year_return'] = 0;
            $DataArray['three_month_return'] = 0;
            $DataArray['month_return'] = 0;
            $DataArray['week_return'] = 0;
            foreach ($historicData as $pickedData) {
                if ($five_Yr >= $pickedData->date) {
                    if ($five_year_val == 0) {
                        $five_year_val = 1;
                        $five_year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['five_year_return'] = round($five_year_return, 2);
                    }
                } elseif ($Three_Yr >= $pickedData->date) {
                    if ($three_year_val == 0) {
                        $three_month_val = 1;
                        $three_year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['three_year_return'] = round($three_year_return, 2);
                    }
                } elseif ($Yr >= $pickedData->date) {
                    if ($year_val == 0) {
                        $year_val = 1;
                        $year_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['year_return'] = round($year_return, 2);
                    }
                } elseif ($Three_Mr >= $pickedData->date) {
                    if ($three_month_val == 0) {
                        $three_month_val = 1;
                        $three_month_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['three_month_return'] = round($three_month_return, 2);
                    }
                } elseif ($Mr >= $pickedData->date) {
                    if ($month_val == 0) {
                        $month_val = 1;
                        $month_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['month_return'] = round($month_return, 2);
                    }
                } elseif ($Wr >= $pickedData->date) {
                    if ($week_val == 0) {
                        $week_val = 1;
                        $week_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                        $DataArray['week_return'] = round($week_return, 2);
                    }
                }
                // elseif ($Dr >= $pickedData->date) {
                //     if ($historicData[0]->date > $pickedData->date) {
                //         if ($day_val == 0) {
                //             $day_val = 1;
                //             $day_return = (($historicData[0]->nav-$pickedData->nav)/$pickedData->nav)*100;
                //             $DataArray['day_return'] = round($day_return, 2);
                //         }
                //     }
                // }
            }
            array_push($scheme_info, $DataArray);
        }
        switch ($duration) {
            case '2':
                usort($scheme_info, function ($a, $b) {
                    return $b['week_return'] <=> $a['week_return'];
                });
                break;
            case '3':
                usort($scheme_info, function ($a, $b) {
                    return $b['month_return'] <=> $a['month_return'];
                });
                break;
            case '4':
                usort($scheme_info, function ($a, $b) {
                    return $b['three_month_return'] <=> $a['three_month_return'];
                });
                break;
            case '5':
                usort($scheme_info, function ($a, $b) {
                    return $b['year_return'] <=> $a['year_return'];
                });
                break;
            case '1':
                usort($scheme_info, function ($a, $b) {
                    return $b['three_year_return'] <=> $a['three_year_return'];
                });
                break;
            default:
                usort($scheme_info, function ($a, $b) {
                    return $b['five_year_return'] <=> $a['five_year_return'];
                });
                break;
        }
        return $scheme_info;
    }

    public function beatMe(Request $request)
    {
        $scheme_code = $request['scheme_code'];
        $fund_type = $request['fund_type'];
        $timespace = $request['timespace'];
        $index_range_date = '';
        $scheme_detail = [];
        $classifications = [];
        $avg_returns  = [];
        $finalData = [];
        switch ($timespace) {
            case '2':
            {
                //calculating the range date to prepare data
                $index_range_date = Carbon::now()->subDays(5)->toDateString();
                break;
            }
            case '3':
            {
                //calculating the range date to prepare data
                $index_range_date = Carbon::now()->subMonths(1)->toDateString();
                break;
            }
            case '4':
            {
                //calculating the range date to prepare data
                $index_range_date = Carbon::now()->subMonths(3)->toDateString();
                break;
            }
            case '5':
            {
                //calculating the range date to prepare data
                $index_range_date = Carbon::now()->subMonths(12)->toDateString();
                break;
            }
            case '6':
            {
                //calculating the range date to prepare data
                $index_range_date = Carbon::now()->subMonths(60)->toDateString();
                break;
            }
            case '7':
            {
                $index_range_date = '';
                break;
            }
        }
        if ($fund_type == 1) {
            $scheme_info = equity::where('scheme_code', $scheme_code)->get()->toArray();
            $classifications = equity::where('classification', $scheme_info[0]['classification'])->where('scheme_code', '!=', $scheme_code)->get()->toArray();
        } elseif ($fund_type == 2) {
            $scheme_info = debt::where('scheme_code', $scheme_code)->get()->toArray();
            $classifications = debt::where('classification', $scheme_info[0]['classification'])->where('scheme_code', '!=', $scheme_code)->get()->toArray();
        } elseif ($fund_type == 3) {
            $scheme_info = balanced::where('scheme_code', $scheme_code)->get()->toArray();
            $classifications = balanced::where('classification', $scheme_info[0]['classification'])->where('scheme_code', '!=', $scheme_code)->get()->toArray();
        }
        $amc_name = $scheme_info[0]['amc_name'];
        $scheme_name = $scheme_info[0]['scheme_name'];
        $data = DB::table($amc_name)->where('scheme_code', $scheme_code)->where('date', '>=', $index_range_date)->orderBy('date', 'asc')->get()->toArray();
        $count = count($data)-1;
        $todayNav = $data[$count]->nav;
        $daySubOne = $data[0]->nav;
        $scheme_value = $todayNav - $daySubOne;
        $scheme_per = ($scheme_value/$daySubOne)*100;
        $scheme_selected = ['scheme_code' => $scheme_code,
                            'scheme_name' => $scheme_name,
                            'current_value' => $todayNav,
                            'current_per' => round($scheme_per, 2),
                            'fund_type' => $fund_type];
        array_push($scheme_detail, $scheme_selected);
        $cummilative_returns = 0;
        $currentIndexs = '';
        $preIndexs = '';
        $performance_datas = [];
        foreach ($data as $index_detail) {
            if (empty($preIndexs)) {
                $preIndexs = $index_detail;
            // $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
            } else {
                $currentIndexs = $index_detail;
                $day_ratio = (($currentIndexs->nav - $preIndexs->nav)/$preIndexs->nav)*100;
                $cummilative_returns += $day_ratio;
                $performance_datas[] = array(strtotime($index_detail->date)*1000,round($cummilative_returns, 2),round($index_detail->nav, 2));
            }
            $preIndexs = $index_detail;
        }
        $finalData[$scheme_code] = $performance_datas;
        foreach ($classifications as $value) {
            $performance_data = array();
            $scheme_data = DB::table($value['amc_name'])->where('scheme_code', $value['scheme_code'])->where('date', '>=', $index_range_date)->orderBy('date', 'asc')->get()->toArray();
            $count = count($scheme_data)-1;
            $todayNav = $scheme_data[$count]->nav;
            $daySubOne = $scheme_data[0]->nav;
            $scheme_value = $todayNav - $daySubOne;
            $scheme_per = ($scheme_value/$daySubOne)*100;
            $scheme_detail[] = ['scheme_code' => $value['scheme_code'],
                            'scheme_name' => $value['scheme_name'],
                            'current_value' => $todayNav,
                            'current_per' => round($scheme_per, 2),
                            'fund_type' => $fund_type];
            $cummilative_return = 0;
            $currentIndex = '';
            $preIndex = '';
            foreach ($scheme_data as $index_detail) {
                if (empty($preIndex)) {
                    $preIndex = $index_detail;
                // $performance_data[] = array(strtotime($index_detail->date)*1000,0,round($index_detail->nav, 2));
                } else {
                    $currentIndex = $index_detail;
                    $day_ratio = (($currentIndex->nav - $preIndex->nav)/$preIndex->nav)*100;
                    $cummilative_return += $day_ratio;
                    $performance_data[] = array(strtotime($index_detail->date)*1000,round($cummilative_return, 2),round($index_detail->nav, 2));
                }
                $preIndex = $index_detail;
            }
            $finalData[$value['scheme_code']] = $performance_data;
        }
        usort($scheme_detail, function ($a, $b) {
            return $b['current_per'] <=> $a['current_per'];
        });
        $sendScheme = [];
        $sendGraph = [];
        $max = 0;
        foreach ($scheme_detail as $value) {
            array_push($sendScheme, $value);
            $myarray = [];
            $sendGraph[$value['scheme_code']] = $finalData[$value['scheme_code']];
            if ($value['scheme_code'] == $scheme_code) {
                break;
            } elseif ($max == 5) {
                array_push($sendScheme, $scheme_selected);
                $sendGraph[$scheme_code] = $finalData[$scheme_code];
                break;
            }
            $max += 1;
        }
        usort($sendScheme, function ($a, $b) {
            return $a['scheme_code'] <=> $b['scheme_code'];
        });
        return response()->json(['msg'=>1,'data'=>$sendGraph, 'scheme_details'=>$sendScheme]);
    }
}
