<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\equity;
use App\debt;
use App\balanced;

class NavUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:nav';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Daily Nav Via amfiindia...!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $equity = equity::select('scheme_code','amc_name')->get()->toArray();
        $debt = debt::select('scheme_code','amc_name')->get()->toArray();
        $balanced = balanced::select('scheme_code','amc_name')->get()->toArray();

        $finalArray = array_merge($equity,$debt,$balanced);

        $file = file("https://www.amfiindia.com/spages/NAVAll.txt");

         foreach ($finalArray as $searchthis) {
            foreach ($file as $line) {
                $line_exp = explode(";", $line);
                if(count($line_exp) > 5) {
                    if($line_exp[0] == $searchthis['scheme_code']) {
                        $date = date("Y-m-d",strtotime($line_exp[5]));
                        DB::table($searchthis['amc_name'])
                                    ->insert(['date' => $date,
                                            'scheme_code' => $line_exp[0],
                                            'scheme_name' => $line_exp[3],
                                            'nav' => $line_exp[4]]);
                    }
                }
            }       
        }
    }
}
