<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\HistoricBse;
use App\HistoricMidcap;
use App\HistoricNse;
use App\HistoricSmallcap;
use App\BseAuto;
use App\BseBanking;
use App\BseEnergy;
use App\BseHealthCare;
use App\BseInfra;
use App\BsePower;
use App\NseSmallCap;
use App\NseMidCap;
use DateTime;
use DB;

class IndexUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $indexs = array('BSE-SMLCAP.BO', '%5EBSESN', 'BSE-MIDCAP.BO', '%5ENSEI');
        foreach($indexs as $index) {
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$index.'?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $timestamps = $data->chart->result[0]->timestamp;
            $count = count($timestamps) - 1;
            $date = new DateTime();
            $date->setTimestamp($timestamps[$count]);

            $finalDate = $date->format('Y-m-d');
            $closeVal = round($values->close[$count], 2);
            $openVal = round($values->open[$count], 2);
            $lowVal = round($values->low[$count], 2);
            $highVal = round($values->high[$count], 2);
            // $dataToinsert = ['date'=>$finalDate,'close'=>$closeVal,'open'=>$openVal,'low'=>$lowVal,'high'=>$highVal];
            switch($index) {
                case '%5ENSEI': {
                    $indexInsert = new HistoricNse();
                    break;
                }
                case '%5EBSESN': {
                    $indexInsert = new HistoricBse();
                    break;
                }
                case 'BSE-MIDCAP.BO': {
                    $indexInsert = new HistoricMidcap();
                    break;
                }
                case 'BSE-SMLCAP.BO': {
                    $indexInsert = new HistoricSmallcap();
                    break;
                }
            }

            $indexInsert->close = $closeVal;
            $indexInsert->date = $finalDate;
            $indexInsert->open = $openVal;
            $indexInsert->low = $lowVal;
            $indexInsert->high = $highVal;
            $indexInsert->save();
        }
        $this->updates();

    }

    public function updates()
    {
        $indexs = array('BSE-AUTO.BO', 'BSE-BANK.BO', 'ENERGY.BO', 'BSE-HC.BO','INFRASTRUCTURE.BO','BSE-POWER.BO','%5ECNXSC','%5ECRSMID');
        foreach($indexs as $index) {
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$index.'?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $timestamps = $data->chart->result[0]->timestamp;
            $count = count($timestamps) - 1;
            $date = new DateTime();
            $date->setTimestamp($timestamps[$count]);

            $finalDate = $date->format('Y-m-d');
            $closeVal = round($values->close[$count], 2);
            // $dataToinsert = ['date'=>$finalDate,'close'=>$closeVal,'open'=>$openVal,'low'=>$lowVal,'high'=>$highVal];
            switch($index) {
                case 'BSE-AUTO.BO': {
                    $indexInsert = new BseAuto();
                    break;
                }
                case 'BSE-BANK.BO': {
                    $indexInsert = new BseBanking();
                    break;
                }
                case 'ENERGY.BO': {
                    $indexInsert = new BseEnergy();
                    break;
                }
                case 'BSE-HC.BO': {
                    $indexInsert = new BseHealthCare();
                    break;
                }
                case 'INFRASTRUCTURE.BO': {
                    $indexInsert = new BseInfra();
                    break;
                }
                case 'BSE-POWER.BO': {
                    $indexInsert = new BsePower();
                    break;
                }
                case '%5ECNXSC': {
                    $indexInsert = new NseSmallCap();
                    break;
                }
                case '%5ECRSMID': {
                    $indexInsert = new NseMidCap();
                    break;
                }
            }

            $indexInsert->close = $closeVal;
            $indexInsert->date = $finalDate;
            $indexInsert->save();
        }

        $tableArrya = [['nse_auto','%5ECNXAUTO'],
                       ['nse_bank','%5ENSEBANK'],
                       ['nse_commodities','%5ECNXCMDT'],
                       ['nse_consumption','%5ECNXCONSUM'],
                       ['nse_cpse','CPSE.NS'],
                       ['nse_energy','%5ECNXENERGY'],
                       ['nse_financ','%5ECNXFIN'],
                       ['nse_fmcg','%5ECNXFMCG'],
                       ['nse_it','%5ECNXIT'],
                       ['nse_infra','%5ECNXINFRA'],
                       ['nse_media','%5ECNXMEDIA'],
                       ['nse_metal','%5ECNXMETAL'],
                       ['nse_pharama','%5ECNXPHARMA'],
                       ['nse_prvbank','NIFTYPVTBANK.NS'],
                       ['nse_psu','%5ECNXPSUBANK'],
                       ['nse_realty','%5ECNXREALTY'] 
                   ];
        foreach ($tableArrya as $value) {
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$value[1].'?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $timestamps = $data->chart->result[0]->timestamp;
            $count = count($timestamps) - 1;
            $date = new DateTime();
            $date->setTimestamp($timestamps[$count]);

            $finalDate = $date->format('Y-m-d');
            $closeVal = round($values->close[$count], 2);
            DB::table($value[0])->insert(['date'=>$finalDate,'close'=>$closeVal]);
        }    
    }
}
