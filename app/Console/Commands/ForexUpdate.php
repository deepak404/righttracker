<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\HistoricChf;
use App\HistoricGbp;
use App\HistoricSgd;
use App\HistoricUsd;
use App\HistoricYen;
use Carbon\Carbon;
use App\Chf;
use App\Eur;

class ForexUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:forex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currencies = array('USD', 'SGD', 'CNY', 'GBP', 'CHF','EUR');
        $ch = curl_init();
        $currency_date = Carbon::now()->subDays(1)->toDateString();
        foreach ($currencies as $value) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://free.currencyconverterapi.com/api/v6/convert?q=".$value."_INR&compact=ultra&date=".$currency_date);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            $output = json_decode($output);
            $inrdetails;
            
            switch($value) {
                case 'USD': {
                    $inrdetails = new HistoricUsd();
                    break;
                }
                case 'SGD': {
                    $inrdetails = new HistoricSgd();
                    break;
                }
                case 'GBP': {
                    $inrdetails = new HistoricGbp();
                    break;
                }
                case 'CNY': {
                    $inrdetails = new HistoricYen();
                    break;
                }
                case 'CHF': {
                    $inrdetails = new Chf();
                    break;
                }
                case 'EUR':{
                    $inrdetails = new Eur();                    
                    break;
                }
                    
            }
            foreach ($output as $key2 => $value2) {
                foreach ($value2 as $key3 => $value3) {
                    $inrdetails->exchange_value = round($value3,3);
                    $inrdetails->date = $key3;
                    $inrdetails->save();
                }
             } 
        }
    }
}
