<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\DowJones;


class GetDow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:dow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/%5EDJI?interval=1d');
        $data = json_decode($file);
        $values = $data->chart->result[0]->indicators->quote[0];
        $timestamps = $data->chart->result[0]->timestamp;
        $count = count($timestamps) - 1;
        $date = new DateTime();
        $date->setTimestamp($timestamps[$count]);

        $finalDate = $date->format('Y-m-d');
        $closeVal = round($values->close[$count], 2);
        $openVal = round($values->open[$count], 2);
        $lowVal = round($values->low[$count], 2);
        $highVal = round($values->high[$count], 2);
        $dataToinsert = ['date'=>$finalDate,'close'=>$closeVal,'open'=>$openVal,'low'=>$lowVal,'high'=>$highVal];
        DowJones::insert($dataToinsert); 
    }
}
