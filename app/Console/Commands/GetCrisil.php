<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Crisil;
use DB;
use DateTime;

class GetCrisil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:crisil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = file('https://www.crisil.com/content/crisil/en/home/what-we-do/financial-products/indices/_jcr_content/wrapper_100_par/tabs_copy/1/indices.json');

        $path = json_decode($path[0])->path;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://www.crisil.com/content/crisil/en/home/what-we-do/financial-products/indices/_jcr_content/wrapper_100_par/tabs_copy/1/indices.json");
        curl_setopt($ch, CURLOPT_POST, 1);
        
        // -------------------------POSTFIELDS Option 1-----------------------------------
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
                    // "filepath=/etc/crisil_indices/indiangraph05072018/indiangraph05072018&basepath=/content/dam/crisil/indices/Indian");
        
        // -------------------------POSTFIELDS Option 2-----------------------------------
        // in real life you should use something like:
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
                 http_build_query(
                    array(
                        'filepath' => $path,
                        'basepath'=>'/content/dam/crisil/indices/Indian'
                        )
                    )
             );


        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $output = json_decode($server_output);
        // dd($output);
        $count = 0;
        foreach ($output as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $dataArray = $value2->graphData;
                $tablename = Crisil::where('indexname',$key2)->value('tablename');
                $dataValue = $dataArray[count($dataArray)-1];
                $dateprice = explode(',', $dataValue);
                $timestamp = (int)($dateprice[0]/1000);
                $date = new DateTime();
                $date->setTimestamp($timestamp);
                $dateFormated = $date->format('Y-m-d');
                $inserted_data=array('date'=>$dateFormated,
                                     'price'=>round($dateprice[1],2) 
                                );
                if ($dateprice[1] != 0) {
                    DB::table($tablename)->insert($inserted_data);
                }
            }
        }
    }
}
