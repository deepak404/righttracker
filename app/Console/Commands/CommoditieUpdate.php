<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Gold;
use App\Silver;
use App\NaturalGas;

class CommoditieUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:commodities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commodities = ['GM1','AG1','NG1'];

        foreach ($commodities as $commoditie) {
            $getData = file_get_contents('https://www.quandl.com/api/v3/datasets/CHRIS/MCX_'.$commoditie.'.json?api_key=F4khnuVJdZM7D7Dys3_5');
            $outputs = json_decode($getData);
            $output = $outputs->dataset;
            if ($commoditie == 'GM1') {
                Gold::insert(['date'=>date('Y-m-d',strtotime($output->data[0][0])),'price'=>$output->data[0][4]]);
            }elseif ($commoditie == 'AG1') {
                Silver::insert(['date'=>date('Y-m-d',strtotime($output->data[0][0])),'price'=>$output->data[0][4]]);
            }elseif ($commoditie == 'NG1') {
                NaturalGas::insert(['date'=>date('Y-m-d',strtotime($output->data[0][0])),'price'=>$output->data[0][4]]);             
            }
        }
    }
}
