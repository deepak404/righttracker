<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DateTime;

class GlobalIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Global Index updata.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timeing = Carbon::now()->timezone('Asia/Kolkata');
        $timenow = date('H:i',strtotime($timeing));
        $array = [['index'=>'%5EIXIC','modelName'=>'Nacdaq'],
                 ['index'=>'%5EFTSE','modelName'=>'Ftse'],
                 ['index'=>'000001.SS','modelName'=>'Sse'],
                 ['index'=>'%5EHSI','modelName'=>'Hansen'],
                 ['index'=>'%5EDJI','modelName'=>'DowJones'],
                 ['index'=>'%5EN225','modelName'=>'Nikkei']];

        if ($timenow == '04:15') {
            $index = $array[0]['index'];
            $modelName = $array[0]['modelName'];
        }elseif ($timenow == '03:00') {
            $index = $array[2]['index'];
            $modelName = $array[2]['modelName'];
        }elseif ($timenow == '12:15') {
            $index = $array[5]['index'];
            $modelName = $array[5]['modelName'];
        }elseif ($timenow == '03:30') {
            $index = $array[4]['index'];
            $modelName = $array[4]['modelName'];
        }elseif ($timenow == '14:00') {
            $index = $array[3]['index'];
            $modelName = $array[3]['modelName'];
        }elseif ($timenow == '14:10') {
            $index = $array[1]['index'];
            $modelName = $array[1]['modelName'];
        }

        if (!empty($index)) {        
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$index.'?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $timestamps = $data->chart->result[0]->timestamp;
            $count = count($timestamps) - 1;
            $date = new DateTime();
            $date->setTimestamp($timestamps[$count]);

            $finalDate = $date->format('Y-m-d');
            $closeVal = round($values->close[$count], 2);
            $openVal = round($values->open[$count], 2);
            $lowVal = round($values->low[$count], 2);
            $highVal = round($values->high[$count], 2);
            $dataToinsert = ['date'=>$finalDate,'close'=>$closeVal,'open'=>$openVal,'low'=>$lowVal,'high'=>$highVal];
            // dd($dataToinsert);
            $ModelName = 'App\\'.$modelName;
            // dd($ModelName);
            $eod = $ModelName::insert($dataToinsert); 
        }
        
        // $array = [['index'=>'%5EIXIC','modelName'=>'Nacdaq'],
        //          ['index'=>'%5EFTSE','modelName'=>'Ftse'],
        //          ['index'=>'000001.SS','modelName'=>'Sse'],
        //          ['index'=>'%5EHSI','modelName'=>'Hansen'],
        //          ['index'=>'%5EDJI','modelName'=>'DowJones'],
        //          ['index'=>'%5EN225','modelName'=>'Nikkei']];

        // foreach ($array as $value) {
        // $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$value['index'].'?interval=1d');
        // $data = json_decode($file);
        // $values = $data->chart->result[0]->indicators->quote[0];
        // $timestamps = $data->chart->result[0]->timestamp;
        // $count = count($timestamps) - 1;
        // $date = new DateTime();
        // $date->setTimestamp($timestamps[$count]);

        // $finalDate = $date->format('Y-m-d');
        // $closeVal = round($values->close[$count], 2);
        // $openVal = round($values->open[$count], 2);
        // $lowVal = round($values->low[$count], 2);
        // $highVal = round($values->high[$count], 2);
        // $dataToinsert = ['date'=>$finalDate,'close'=>$closeVal,'open'=>$openVal,'low'=>$lowVal,'high'=>$highVal];
        // // dd($dataToinsert);
        // $ModelName = $value['modelName'];
        // $ModelName = 'App\\'.$ModelName;
        // // dd($ModelName);
        // $eod = $ModelName::insert($dataToinsert); 
        // }

    }
}
