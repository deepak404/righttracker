<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\IndexUpdate::class,
        Commands\ForexUpdate::class,
        Commands\NavUpdate::class,
        Commands\CommoditieUpdate::class,
        Commands\GetDow::class,
        Commands\GetFtse::class,
        Commands\GetHansen::class,
        Commands\GetNacdaq::class,
        Commands\GetNikkei::class,
        Commands\GetSse::class,
        Commands\GetDax::class,
        Commands\GlobalIndex::class,
        Commands\GetCrisil::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('get:forex')->daily(00.10);
        $schedule->command('get:crisil')->daily(00.20);
        $schedule->command('get:nav')->daily(00.12);
        $schedule->command('get:commodities')->daily(00.15);
        $schedule->command('get:sse')
                    ->dailyAt(03.00)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:dow')
                    ->dailyAt(03.30)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:nikkei')
                    ->dailyAt(12.15)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:hansen')
                    ->dailyAt(14.00)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:ftse')
                    ->dailyAt(14.10)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:nacdaq')
                    ->dailyAt(14.15)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:index')
                    ->weekdays()
                    ->dailyAt(16.00)
                    ->timezone('Asia/Kolkata');
        $schedule->command('get:dax')
                    ->weekdays()
                    ->dailyAt(22.30)
                    ->timezone('Asia/Kolkata');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
