<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/
Auth::routes();

Route::get('/', function () {
    $user = Auth::user();
    if (!$user) {
        return redirect('/login');
    } elseif ($user->role == 1) {
        return redirect('/home');
    } elseif ($user->role == 2) {
        return redirect('/rmadmin');
    } elseif ($user->role == 3) {
        return redirect('/rm');
    }
});

// ********************************************************** //
//                   Normal user login                        //
// ********************************************************** //

Route::get('/home', 'EquityController@diversified');

Route::get('/diversified', 'EquityController@diversified')->name('diversified');

Route::get('/largecap', 'EquityController@largCap')->name('largecap');

Route::get('/smallmidcap', 'EquityController@smallmidCap')->name('smallmidcap');

Route::get('/elss', 'EquityController@elss')->name('elss');

Route::get('/sectoral', 'EquityController@sectoral')->name('sectoral');

Route::get('/arbitrage', 'EquityController@arbitrage')->name('arbitrage');

Route::get('/eq_hybrid', 'EquityController@hybrid');

Route::get('/liquid', 'DebtController@liquid')->name('liquid');

Route::get('/fmp', 'DebtController@fmp')->name('fmp');

Route::get('/longterm', 'DebtController@longterm')->name('longterm');

Route::get('/shortterm', 'DebtController@shortterm')->name('shortterm');

Route::get('/ultrashortterm', 'DebtController@ultrashortterm')->name('ultrashortterm');

Route::get('/floatingrate', 'DebtController@floatingrate')->name('floatingrate');

Route::get('/creditopps', 'DebtController@creditopps')->name('creditopps');

Route::get('/mip', 'DebtController@mip')->name('mip');

Route::get('/hybrid', 'DebtController@hybrid')->name('hybrid');

Route::get('/giltshortterm', 'DebtController@giltshortterm')->name('giltshortterm');

Route::get('/giltlongterm', 'DebtController@giltlongterm')->name('giltlongterm');

Route::get('/balanced', 'BalancedController@balanced')->name('balanced');

Route::get('/ess', 'BalancedController@ess')->name('ess');

Route::get('/benchmark', 'IndexController@benchmark');

Route::get('/crisil', 'CrisilController@index');

Route::get('/policy', 'PolicyController@index');

Route::get('/comparison', 'ComparisonController@comparison');

Route::get('/temp', 'ComparisonController@temp');

Route::get('/forex', 'IndexController@forex');

Route::get('/commodities', 'CommoditieController@index');

Route::post('/getIndex', 'IndexController@getIndex');

Route::post('/selectedCommoditie', 'CommoditieController@selectedCommoditie');

Route::post('/getForex', 'IndexController@getForex');

Route::get('/globalIndex', 'IndexController@globalIndex');

Route::post('/selectedIndex', 'IndexController@selectedIndex');

Route::get('/schemes', 'SchemeController@getSchemes');

Route::get('/schemesDebt', 'SchemeController@schemesDebt');

Route::get('/schemesBalanced', 'SchemeController@schemesBalanced');

Route::post('/add_scheme', 'SchemeController@addSchemes');

Route::post('/delete_scheme', 'SchemeController@deleteScheme');

Route::get('/scheme_search', 'ComparisonController@getFunds');

Route::post('/get_data', 'ComparisonController@getData');

Route::post('/add_policy', 'PolicyController@addPolicy');

Route::post('/selectedPolicy', 'PolicyController@selectedPolicy');

Route::post('/get_scheme_info', 'SchemeController@getSchemeInfo');

Route::post('/edit_scheme_info', 'SchemeController@editSchemeInfo');

Route::post('/insert_policy', 'PolicyController@insertPolicy');

Route::post('/topFunds', 'ComparisonController@topFunds');

Route::post('/exportPdf', 'ExportController@exportPdf');

Route::post('/beatme', 'ComparisonController@beatMe');

Route::get('/portfolio_track', 'PortfolioController@index');

Route::post('/add_investor', 'PortfolioController@addInvestor');

Route::get('/get_group', 'PortfolioController@getGroup');

Route::post('/add_invetments', 'PortfolioController@addInvestments');

Route::get('/user_search', 'PortfolioController@userSearch');

Route::post('/track_user', 'PortfolioController@trackUser');

Route::get('/get_inv', 'PortfolioController@getInv');

Route::post('/get_investments', 'PortfolioController@getInvestments');

Route::post('/remove_scheme', 'PortfolioController@removeScheme');

Route::post('/remove_user', 'PortfolioController@removeUser');

Route::get('/live_tv', function () {
    return view('livetv.live-tv');
});

Route::get('/tvmodule/{type?}', 'TvController@index');

Route::post('/getCrisil', 'CrisilController@getGraph');

Route::get('/liveindex', 'IndexController@liveIndex');

Route::post('/tvgraph', 'TvController@tvGraph');

Route::post('/addindexpe', 'TvController@addIndexPe');

Route::get('/tv/{tv?}', 'TvController@newTv');






// ********************************************************** //
//                    RM Admin Routes                         //
// ********************************************************** //
Route::get('/rmadmin', 'RmadminController@index');

Route::get('/get_rm_amc', 'RmadminController@getRmAmc');

Route::post('/add_rmuser', 'RmadminController@addRmuser');

Route::post('/delete_rmuser', 'RmadminController@deleteRmuser');

Route::post('/get_rm_info', 'RmadminController@getRmInfo');

Route::post('/update_user', 'RmadminController@updateUser');

Route::post('/change_rmuser_pass', 'RmadminController@changePass');










// ********************************************************** //
//                    RM User Routes                          //
// ********************************************************** //
Route::get('/rm', 'RmuserController@index');

Route::get('/rm_debt', 'RmuserController@debt');

Route::get('/rm_hybrid', 'RmuserController@hybrid');

Route::post('/add_equity', 'RmuserController@addEquity');

Route::post('/add_hybrid', 'RmuserController@addHybrid');

Route::post('/add_debt', 'RmuserController@addDebt');







// ********************************************************** //
//                        Logout                              //
// ********************************************************** //
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');








// ********************************************************** //
//                    For testing Only                        //
// ********************************************************** //

Route::get('/xyz', 'TestController@forexUpdate');

// Route::get('/csvUpload','TestController@csvUpload');

// Route::get('/createTable/{name}','TestController@createTable');

// Route::get('/getNav','TestController@getNav');

// Route::get('/getLive','TestController@getLiveSmallcap');

// Route::get('/goldData','TestController@goldData');

// Route::get('/oneday','TestController@oneDay');

// Route::get('/guzzleTest', 'TestController@guzzlePost');

// Route::get('/liveindex', 'TestController@liveIndex');

// Route::get('/tvmodule', function() {
//     return view('livetv.tvmodule');
// });
